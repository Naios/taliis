package starlight.taliis.helpers;

import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.*;
import starlight.taliis.core.chunks.adt.MWMO;
import starlight.taliis.core.files.*;

/**
 * This sweet thingy does nothing else as check ALL offsets
 * 
 * 
 * It does NOT heck the correct size of a Chunk itself
 * It does NOT check for correct flied's or flags (atm)
 * 
 */
public class adtChecker {
	adt file;
	public int errcount = 0;
	
	/**
	 * Init the offset/size checker
	 * @param pointer to an allready initialisized adt file
	 */
	public adtChecker(adt pointer) {
		file = pointer;
	}
	
	/**
	 * Start the check
	 */
	public int check() {
		int ro = 0;
		int base = 0;
		int ddoffs=0, wmoffs=0, infoffs=0;
				
		System.out.println("\nOffset checkup ...");
		ro = file.info.getSize();
	
	//-------------------------------------------------------------
		// 1. header
		System.out.print("Header: ");
		
		MHDR header = file.header;
			ro += chunk.data;		// exclude chunk header
			base = ro;				// new base for all header offsets
			
			if(checkChunk(base + header.getInfoOffs() , "MCIN")==true)
				infoffs = base + header.getInfoOffs();
			checkChunk(base + header.getTexOffs() , "MTEX");
			checkChunk(base + header.getModelOffs() , "MMDX");
			if(checkChunk(base + header.getModelIDOffs() , "MMID")==true)
				ddoffs = 1;
			checkChunk(base + header.getMapObjOffs() , "MWMO");
			if(checkChunk(base + header.getMapObjIDOffs() , "MWID")==true)
				wmoffs = 1;
			checkChunk(base + header.getDooDsDefOffs() , "MDDF");
			checkChunk(base + header.getObjDefOffs() , "MODF");
			
		System.out.println("done.");
		
	//-------------------------------------------------------------	
		// 2. Doodad filename offsets
		if(ddoffs!=0) {
			MMID ddinfo = file.mmid;
			MMDX ddfiles = file.mmdx;
			
			int len = ddinfo.getLenght();
			System.out.print(len + " DD-Objects: ");
			
			// check for NULL at offs-1
			for(int c=1; c<len; c++) {
				int off = ddinfo.getOffsetNo(c);
				
				if(ddfiles.buff.get(off + chunk.data - 1)==0) {
					
				}
				else {
					System.out.println("Borken at " + c + ". entry. " +
							"Expected leading NULL was not found at 0x" +
							Long.toHexString(off + chunk.data) + " - 1"
						);
					errcount++;
				}
			}
			
			System.out.println("done.");
		}
		else System.out.println("Skipp DD offsets cause chunk offset is broken.");
		
	//-------------------------------------------------------------
		// 3. wmo filename offsets
		if(wmoffs!=0){
			MWID objinfo = file.mwid;
			MWMO objfiles = file.mwmo;
			
			int len = objinfo.getLenght();
			System.out.print(len + " MWO-Objects: ");
			
			// check for NULL at offs-1
			for(int c=1; c<len; c++) {
				int off = objinfo.getOffsetNo(c);
				
				if(objfiles.buff.get(off + chunk.data - 1)==0) {
					
				}
				else {
					System.out.println("Borken at " + c + ". entry. " +
							"Expected leading NULL was not found at 0x" +
							Long.toHexString(off + chunk.data) + " - 1"
						);
					errcount++;
				}
			}
			
			System.out.println("done.");
		}
		else System.out.println("Skipp wmo offsets cause chunk offset is broken.");
		
		
	//-------------------------------------------------------------
		// 4. info chunk
		if(infoffs!=0) {
			System.out.print("Map Fields: ");
			
			MCIN info = file.fieldInfo;
			wmoffs+=chunk.data;
			
			// 5. each field header
			for(int c=0; c<256; c++) {
				int offs = info.entrys[c].getOffset();
				if(checkChunk(offs, "MCNK")==true) {
					MCNK field = file.mcnk[c];
					
					checkChunk(offs + field.getHeightOffs(), "MCVT");
					checkChunk(offs + field.getNormalsOffs(), "MCNR");
					checkChunk(offs + field.getTexLayerOffs(), "MCLY");
					checkChunk(offs + field.getRefsOffs(), "MCRF");
					
					checkChunk(offs + field.getAlphaOffs(),
							"MCAL", field.getAlphaSize()/*-chunk.data/**/);
					checkChunk(offs + field.getShadowOffs(),
							"MCSH", field.getShadowSize());
					checkChunk(offs + field.getLiquitOffs(), 
							"MCLQ", field.getLiquitSize()/*-chunk.data/**/);
					if(field.getnSoundE()!=0)
					checkChunk(offs + field.getSoundEOffs(), "MCSE");
				}
				else System.out.println("Field Chunk " + c + " is broken.");
				//System.out.println();
			}

			System.out.println("done.");
		}
		else System.out.println("Skipp field offsets cause chunk offset is broken.");
		
		
		System.out.println("Total found " + errcount + " errors.");
		
		return errcount;
	}
		
	
	
	
	private boolean checkChunk(int offset, String wanted) {
		return checkChunk(offset, wanted, false);
	}

	private boolean checkChunk(int offset, String wanted, int sizeWanted) {
		return checkChunk(offset, wanted, sizeWanted, false);
	}
	
	private boolean checkChunk(
			int offset,
			String wanted,
			int sizeWanted,
			boolean toggle
	) {
		
		// check magic
		String tmp = new StringBuffer(
				getMagic(offset)
			).reverse().toString();		
		// same?
		if(tmp.compareTo(wanted)!=0) {
			if(toggle==false) {
				System.out.println("Broken at 0x" + Long.toHexString(offset) + ": " +
						"Expected " + wanted + " but found " + tmp + "."
					);
			}
			errcount++;
			return false;
		}
		
		// check size
		int size = getSize(offset);
		if(size!=sizeWanted) {
			if(toggle==false) {
				System.out.println("Broken at 0x" + Long.toHexString(offset) + ": " +
						"Expected size of " + wanted + ": "+ sizeWanted + " but found " + size + " (chunk)."
					);
			}
			
			errcount++;
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check if offset hold the wanted chunk and returns true or false.
	 * Does own error output!
	 * 
	 * @param offset
	 * @param wanted
	 * @param toggle true = no error message, no error counting
	 * @return
	 */
	private boolean checkChunk(int offset, String wanted, boolean toggle) {
		String tmp = new StringBuffer(
						getMagic(offset)
					).reverse().toString();		
		// same?
		if(tmp.compareTo(wanted)==0) {
			//TODO: check size !
			//System.out.println("Found!");
			return true;
		}
		// or not?
		else {
			if(toggle==false) {
				System.out.println("Broken at 0x" + Long.toHexString(offset) + ": " +
						"Expected " + wanted + " but found " + tmp + "."
					);
			}
			errcount++;
			return false;
		}
	}
	
	/**
	 * catch _magic_ at given offset
	 */
	private String getMagic(int chunkOffset) {
		byte tmp[] = new byte[4];
		file.buff.position(chunkOffset);
		file.buff.get(tmp);
		file.buff.position(0);
		
		return new String(tmp);
	}
	
	/**
	 * catch size
	 */
	private int getSize(int chunkOffset) {
		file.buff.position(chunkOffset + chunk.chunkSize);
		int tmp = file.buff.getInt();
		file.buff.position(0);
		
		return tmp;
	}
}
