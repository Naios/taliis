package starlight.taliis.core;

import java.nio.ByteBuffer;

/**
 * Fucking weird string...
 * <ul>
 * <li>Has 4byte aligments in its beginning.
 * <li>Terminates with 0.
 * <li>Pads with 0 until the next aligment.
 * </ul>
 * 
 * @author ganku
 */
public class ZeroPaddedString extends memory {
	public int initialOffset = -1;

	/**
	 * Catch a string and push the pointer to the beginning of the next string.
	 * 
	 * @param pointer A pointer to the padded String.
	 */
	public ZeroPaddedString(ByteBuffer pointer) {
		super(pointer);

		// Find String end
		byte t = 0;
		do
			t = buff.get();
		while (t != 0);
		int strend = buff.position();

		// find padding end
		while (t == 0 && buff.hasRemaining())
			t = buff.get();

		// push pointer
		pointer.position(pointer.position() + buff.position() - 1);

		// remember our initial offset in the buffer
		initialOffset = pointer.position();
		// limit our data area and kill out last zero
		buff.position(0);
		buff.limit(strend - 1);
	}

	/**
	 * <b>Not yet implemented!</b><br>
	 * <br>
	 * Renders the given string and adds the zero paddings until the next 4 byte
	 * aligtment.
	 */
	// TODO Implement!
	@Override
	public void render() {
		buff.position(0);
	}

	/**
	 * Create a zero terminated <code>String</code> by registering new memory to
	 * apply newString.
	 * 
	 * @param newString The new <code>String</code>.
	 */
	public ZeroPaddedString(String newString) {
		super(newString.length());
				
		buff.position(0);
		buff.put(newString.getBytes());
		
	}

	/**
	 * Sets a new string.
	 * 
	 * @param newString The new <code>String</code>.
	 */
	public void setString(String newString) {
		buff = doRebirth(newString.length());
		buff.position(0);
		buff.put(newString.getBytes());
	}
	
	/**
	 * Get our String.
	 * 
	 * @return The padded string without any zeros.
	 */
	@Override
	public String toString() {
		byte str[] = new byte[buff.limit()];

		buff.position(0);
		buff.get(str, 0, buff.limit());

		return new String(str);
	}
}
