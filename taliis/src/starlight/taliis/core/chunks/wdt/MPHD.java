package starlight.taliis.core.chunks.wdt;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Header chunk. Contains 8 32-bit integers. <br>
 * The first integer is 1 for maps without any terrain, 0 otherwise. The other
 * ones are always 0.
 */
public final class MPHD extends chunk {
	/**
	 * Set to 1 if the map has no terrain, 0 if it has.
	 */
	public final static int hasTerr = 0x8;
	public final static int unk1 = 0xc;
	public final static int unk2 = 0x10;
	public final static int unk3 = 0x14;
	public final static int unk4 = 0x18;
	public final static int unk5 = 0x1c;
	public final static int unk6 = 0x20;
	public final static int unk7 = 0x24;
	public final static int end = 0x28;

	/**
	 * Read data from pointer.
	 * 
	 * @param pointer The buffer where this chunk should be.
	 * @throws ChunkNotFoundException Thrown if the chunk is not there.
	 */
	public MPHD(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "DHPM");

		int size = buff.getInt();

		buff.limit(size + data);
		pointer.position(pointer.position() + buff.limit());
	}

	/**
	 * Create an empty one.
	 */
	public MPHD() {
		super(end);
		byte magic[] = { 'D', 'H', 'P', 'M' };

		buff.put(magic); // magic
		buff.putInt(end - data); // size

		buff.position(0);
		if (DEBUG)
			System.out.println("MPHD created.");
	}

	/**
	 * @return Returns the flag.
	 */
	public int hasTerrain() {
		return buff.getInt(hasTerr);
	}

	/**
	 * @return True if the map has terrain, false if it not.
	 */
	public boolean hasTerrainB() {
		return !(buff.getInt(hasTerr) == 1);
	}

	/**
	 * Sets the flag.
	 * 
	 * @param val 1 if the map has no terrain, 0 if it has.
	 */
	public void setTerrain(int val) {
		buff.putInt(hasTerr, val);
	}
}
