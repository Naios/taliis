package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Doodad sets
 * http://madx.dk/wowdev/wiki/index.php?title=WMO#MODS_chunk
 * 
 * @author ganku
 *
 */

public class MODS extends chunk {
	public MODS_Entry entrys[] = null;
	int length;
	/**
	 * Reads in from given pointer set in a databuffer
	 * @param pointer
	 */
	public MODS(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "SDOM");

		int size = buff.getInt();
		buff.limit(buff.position() + size);
		
		// load Entrys
		length= size/MODS_Entry.end;
		entrys = new MODS_Entry[length];
		for(int i=0; i<length; i++) {
			entrys[i] = new MODS_Entry(buff);
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MODS(){
		super(data);
		byte magic[] = { 'S', 'D', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}

	
	/**
	 * Adds a new MODS_Entry
	 * ::Rase the nSets in MOHD!
	 * @param SetName byte[20]
	 * @param startDoodad
	 * @param nDoodads
	 * @return
	 */
	public int addDoodad(byte[] SetName, int startDoodad, int nDoodads) {
		// make new array of objects
		MODS_Entry tmp[] = new MODS_Entry[length +1];
		
		// copy all data
		for(int c=0; c<length; c++) {
			tmp[c]=entrys[c];
		}
		
		// remove old references
		entrys = tmp;
		
		// create newest entry
		entrys[length] = new MODS_Entry();
		entrys[length].setFirstInstanceIndex(startDoodad);
		entrys[length].setName(SetName);
		entrys[length].setNDoodads(nDoodads);
		// set new number
		length++;
		
		// we have a new size now
		Change();
		
		return length -1;
	}
	
	
	/**
	 * Allocate our memory .. render all sub-objects
	 */
	public void render() {
		// calculate out size
		int l = 0;
		
		// do we had any changes?
		//if(isChanged()) {
			// re - create all memory
			
			// check for existence
			l = getLenght();
			
			System.out.println(l + " mods appeareances.");
			
			// create new buffer
			
			ByteBuffer tmp = doRebirth( l*MODS_Entry.end + data );

			// add magic
			tmp.position(magic);
			byte magic[] = { 'S', 'D', 'O', 'M'};
			tmp.put(magic);
			
			// copy all data
			tmp.position(data);
			for(int c=0; c<length; c++)
				if(entrys[c]!=null) {
					entrys[c].buff.position(0);
					tmp.put( entrys[c].buff );
				}
					
			// kick out old memory
			buff = tmp;
			
			// write new size
			writeSize();
			
			// re init out data objects
			buff.position(0x8);
			
			entrys = new MODS_Entry[l];
			for(int c=0; c<l; c++)
				entrys[c] = new MODS_Entry(buff);
			
		//}
		
		buff.position(0);
	}
	
	/**
	 * Returns how many entrys we have
	 */
	public int getLenght() {
		if(entrys==null) return 0;
		else return entrys.length;
	}
}