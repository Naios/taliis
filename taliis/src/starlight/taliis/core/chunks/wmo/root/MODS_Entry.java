package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

public class MODS_Entry extends memory {
	/*000h*/  public final static int name 					= 0x0;	// char[20]
	/*014h*/  public final static int firstinstanceindex 	= 0x14;
	/*018h*/  public final static int numDoodads 			= 0x18;
	/*01Ch*/  public final static int unknown				= 0x1c;
		public final static int end = 0x20;
	
	/**
	 * Creates Entry from the given databuffer.
	 * position have to be set, pointer get pushed after
	 * this data
	 * @param pointer
	 */
	public MODS_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		pointer.position( pointer.position() + end);
	}
	public MODS_Entry() {
		super(end);
		buff.position(0);
	}
	
	public String getName() {
		byte str[] = new byte[20];
		
		buff.position(name);
		buff.get(str, 0, 20);
		
		return new String(str);
	}
	public int getFirstInstanceIndex() {
		return buff.getInt(firstinstanceindex);
	}
	public int getHaveNDoodads() {
		return buff.getInt(numDoodads);
	}
	/**
	 * 
	 * @param str byte[20]
	 */
	public void setName(byte[] str) {
		if(str.length>20){
			System.out.println("MaxLength is 20!");
		}
		else{
		buff.position(name);
		buff.put(str);
		}
	}
	public void setFirstInstanceIndex(int val) {
		buff.putInt(firstinstanceindex, val);
	}
	public void setNDoodads(int val) {
		buff.putInt(numDoodads, val);
	}
}