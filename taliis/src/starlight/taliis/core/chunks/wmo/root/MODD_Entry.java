package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

public class MODD_Entry extends memory {
	  /*000h*/  public final static int nameIndex	= 0x0;
	  /*004h*/  public final static int pos_x		= 0x4;  // 3 float
	  			public final static int pos_y		= 0x8;
	  			public final static int pos_z		= 0xc;
	  /*010h*/  public final static int rot_a		= 0x10; // 4 float
	  			public final static int rot_b		= 0x14;
	  			public final static int rot_c		= 0x18;
	  			public final static int rot_d		= 0x1c;
	  /*020h*/  public final static int scale		= 0x20; // float
	  /*024h*/  public final static int color_r 	= 0x24; // 4 byte
	  			public final static int color_g		= 0x25;
	  			public final static int color_b		= 0x26;
	  			public final static int color_a		= 0x27;
	  /*028h*/
	  		public final static int end = 0x28;
	
	/**
	 * Creates Entry from the given databuffer.
	 * position have to be set, pointer get pushed after
	 * this data
	 * @param pointer
	 */
	public MODD_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		pointer.position( pointer.position() + end);
	}
	public MODD_Entry() {
		super(end);
		buff.position(0);
	}
	
	public void render(){
		
		buff.position(0);
	}

	public int getNameIndex() {
		return buff.getInt(nameIndex);
	}
	public void setNameIndex(int val) {
		buff.putInt(nameIndex, val);
	}
	public float getScale() {
		return buff.getFloat(scale);
	}
	public void setScale(float val) {
		buff.putFloat(scale, val);
	}
	
	public float[] getPosition() {
		float ret[] = new float[3];
		
		buff.position(pos_x);
		for(int i=0; i<3; i++)
			ret[i] = buff.getFloat();
		
		return ret;
	}
	/**
	 * Sets the Position
	 * @param val float[3]
	 */
	public void setPositionX(float val) {
		
		buff.position(pos_x);
		buff.putFloat(val);
		
		
	}
	public void setPositionY(float val) {
		
		buff.position(pos_y);
		buff.putFloat(val);
		
		
	}
	public void setPositionZ(float val) {
	
	buff.position(pos_z);
	
		buff.putFloat(val);
	
	
	}
	public float[] getRotation() {
		float ret[] = new float[4];
		
		buff.position(rot_a);
		for(int i=0; i<4; i++)
			ret[i] = buff.getFloat();
		
		return ret;
	}
	/**
	 * Sets the rotation
	 * @param val float[4]
	 */
	public void setRotationA(float val) {
		
		
		buff.position(rot_a);
		buff.putFloat(val);
		
		
	}
	public void setRotationB(float val) {
			
		buff.position(rot_b);
		buff.putFloat(val);
		
		
	}
	public void setRotationC(float val) {
	
	
	buff.position(rot_c);
	
		buff.putFloat(val);
	
	
	}
	public void setRotationD(float val) {
	
	
	buff.position(rot_d);
	
		buff.putFloat(val);
	
	
	}
	public byte[] getColors() {
		byte ret[] = new byte[4];
		
		buff.position(color_r);
		for(int i=0; i<4; i++)
			ret[i] = buff.get();
		
		return ret;
	}
	/**
	 * Sets the Color
	 * @param val byte[4]
	 */
	public void setColors(byte[] val) {
		buff.position(color_r);
		for(int i=0; i<4; i++)
			buff.put(val[i]);
				
	}
}