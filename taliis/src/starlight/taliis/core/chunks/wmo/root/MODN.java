package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.ZeroPaddedString;
import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Zero padded String list that holds the wmo doodad 
 * file names.
 * 
 * http://madx.dk/wowdev/wiki/index.php?title=WMO#MODN_chunk
 * 
 * @author ganku
 *
 */

public class MODN extends chunk {
	public ZeroPaddedString entrys[] = null;
	public int index[];
	public int length;
	int size;
	/**
	 * loads cound strings from the given pointer and
	 * hold it into this chunk.
	 * 
	 * @param pointer
	 * @param count
	 */
	public MODN(ByteBuffer pointer)  throws ChunkNotFoundException  {
		super(pointer, "NDOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		
		// how many strings?
		int count = 0;
		
		byte chr=0, old_chr=1;
		while(buff.hasRemaining()) {
			chr = buff.get();
			if(chr!=old_chr) {
				if(chr==0) count++;
				old_chr = chr;
			}
		};
		
		// catch strings
		buff.position(data);
		index=new int[count];
		entrys = new ZeroPaddedString[count];
		for(int i=0; i<count; i++) {
			index[i]=buff.position()-8;
			entrys[i] = new ZeroPaddedString(buff);
		}
		length=entrys.length;

		// push
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MODN() { 
		super(data);
		byte magic[] = { 'N', 'D', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	public void render(){
		int s = 0;
		// calc needed size (+ zero)
		s=8;
		for(int i=0;i<length;i++){
			String ts=entrys[i].toString();
			int temp;
			temp=((s+ts.getBytes().length+1)%4);
			s+=ts.length();
			s++;
			for(int j=0;j<temp;j++){
				//System.out.println("Adding Nullbit\t"+j+"\tto entry\t"+i);
				s++;
			}
		}
		// allocate
		ByteBuffer tmp = doRebirth(s);

		// write down
		for(int c=0; c<length; c++) {
			String ts=entrys[c].toString();
			//System.out.println(ts);
			entrys[c].buff.position(0);
			tmp.put(entrys[c].buff);
			tmp.put((byte)0);
			int temp=((tmp.position())%4);
			
			for(int i=0;i<temp;i++){
				//System.out.println("Adding Nullbyte\t"+i+"\tto entry\t"+c);
				tmp.put((byte) 0);
			}
			//this is shitty :/
		}
		buff = tmp;
		
		// reset pointers
		writeSize();
		buff.position(0);
	}
	
	/**
	 * Returns the zeropaddedstring at index as normal string
	 * @param index
	 * @return
	 */
	public String getString(int index) {
		if(entrys==null) return null;
		if(index<0 || index>entrys.length) return null;
		return entrys[index].toString();
	}
	
	/**
	 * Number of strings we have
	 */
	public int getLenght() {
		if(entrys==null) return 0;
		else return entrys.length;
	}
	
	/**
	 * Adds new string to our filenames list
	 * ::Rase the nModels in MOHD!
	 * @param text
	 * @return file index of the new string
	 */
	public int addString(String text) {
		ZeroPaddedString tmp[] =  new ZeroPaddedString[length+1];
		int[] indices = new int[length+1];
		if(length!=0)
		for(int c=0; c<length; c++){
			tmp[c] = entrys[c];
			indices[c] = index[c];
		}
		indices[length]=buff.position()-8;
		/*int temp=0;
		if((text.length()%16)!=0)
			temp= 16-(text.length()%16);*/
			
			tmp[length] = new ZeroPaddedString(text);
			//}
		entrys = tmp;
		index=indices;
		length++;
		size+=text.length()+1;
		writeSize();
		Change();
		return length-1;
	}
}
