package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.wmo.root.MOMT_Material;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


/**
*	Materials used in this mapobject
*	Contains nMaterials
*	size=nMaterials*64bytes
*
*	@author Tigurius
*
*/


public class MOMT extends chunk{
	public int nmat = 0;
	public MOMT_Material mats[];

	
	public MOMT(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "TMOM");

		int size = buff.getInt();
		buff.limit(buff.position() + size);
		
		nmat= size/64;
		mats = new MOMT_Material[nmat];
		for(int i=0;i<nmat;i++)
			mats[i] = new MOMT_Material(buff);
		
		pointer.position( pointer.position() + buff.limit() );
	}
	
	public MOMT(){
		super(data);
		byte magic[] = { 'T', 'M', 'O', 'M'};
		nmat=0;
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	/**
	 * Allocate our memory .. render all sub-objects
	 */
	public void render() {
		// calculate out size
		
		
		// do we had any changes?
		//if(isChanged()) {
			// re - create all memory
			
			// check for existence
			int l = getLenght();
			
			System.out.println(l + " momt appeareances.");
			
			// create new buffer
			
			ByteBuffer tmp = doRebirth( l*MOMT_Material.end + data );

			// add magic
			tmp.position(magic);
			byte magic[] = { 'T', 'M', 'O', 'M'};
			tmp.put(magic);
			
			// copy all data
			tmp.position(data);
			for(int c=0; c<nmat; c++)
				if(mats[c]!=null) {
					mats[c].buff.position(0);
					tmp.put(mats[c].buff);
					
				}
					
			// kick out old memory
			buff = tmp;
			
			// write new size
			writeSize();
			
			// re init out data objects
			buff.position(0x8);
			
			mats = new MOMT_Material[l];
			for(int c=0; c<l; c++)
				mats[c] = new MOMT_Material(buff);
			
		//}
		
		buff.position(0);
	}
	
	/**
	 * Creates a new Doodad Entry
	 * ::Rase the nDoodads in MOHD!
	 * @return
	 */
	public int addMaterial() {
		// make new array of objects
		MOMT_Material tmp[] = new MOMT_Material[nmat +1];
		
		// copy all data
		for(int c=0; c<nmat; c++) {
			tmp[c]=mats[c];
		}
		
		// remove old references
		mats = tmp;
		
		// create newest entry
		mats[nmat] = new MOMT_Material();

		// set new number
		nmat++;
		
		// we have a new size now
		Change();
		
		return nmat -1;
	}
	
	public int getLenght() {
		return mats.length;
	}
}