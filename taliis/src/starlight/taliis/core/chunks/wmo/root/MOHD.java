package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * The WMO header chunk.
 * Info taken from:
 * 	http://madx.dk/wowdev/wiki/index.php?title=WMO#MOHD_chunk
 * 
 * @author ganku
 *
 */

public class MOHD extends chunk {
	/*000h*/  public final static int nMaterials=data	+	0x0;		
	/*004h*/  public final static int nGroups =	data	+	0x4;		
	/*008h*/  public final static int nPortals=	data	+	0x8;		
	/*00Ch*/  public final static int nLights =	data	+	0xc;		
	/*010h*/  public final static int nModels =	data	+	0x10;		
	/*014h*/  public final static int nDoodads=	data	+	0x14;
	/*018h*/  public final static int nSets =	data	+	0x18;		
	/*01Ch*/  public final static int colR =	data	+	0x1c;		
	/*01Dh*/  public final static int colG =	data	+	0x1d;
	/*01Eh*/  public final static int colB =	data	+	0x1e;
	/*01Fh*/  public final static int colX =	data	+	0x1f;			
	/*020h*/  public final static int wmoID =	data	+	0x20;
	/*024h*/  public final static int bb1x =	data	+	0x24;
			  public final static int bb1y =	data	+	0x28;
			  public final static int bb1z =	data	+	0x2C;
	/*030h*/  public final static int bb2x =	data	+	0x30;
			  public final static int bb2y =	data	+	0x34;
			  public final static int bb2z =	data	+	0x38;
	/*03Ch*/  public final static int nullish =	data	+	0x3c;
			public final static int end		 =	data	+	0x40;
	
	/**
	 * Reads in from given pointer set in a databuffer
	 * @param pointer
	 */
	public MOHD(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "DHOM");

		int size = buff.getInt();

		buff.limit(buff.position() + size);
		pointer.position( pointer.position() + buff.limit());
	}
	
	/**
	 * Create new Header -> unimplementated
	 */
	public MOHD() {
		super(end);
		byte magic[] = { 'D', 'H', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(end);		// size
		
		// reset position
		buff.position(0);
	}
	
	
	// get and set functions
	
	public int getNMaterias() {
		return buff.getInt(nMaterials);
	}	
	public void setNMaterias(int val) {
		buff.putInt(nMaterials, val);
	}	
	public int getNGroups() {
		return buff.getInt(nGroups);
	}	
	public int getNPortals() {
		return buff.getInt(nPortals);
	}	
	public int getNLights() {
		return buff.getInt(nLights);
	}	
	public int getNModels() {
		return buff.getInt(nModels);
	}
	public void setNModels(int val) {
		buff.putInt(nModels, val);
	}
	public int getNDoodads() {
		return buff.getInt(nDoodads);
	}	
	public void setNDoodads(int val) {
		buff.putInt(nDoodads, val);
	}	
	public int getNSets() {
		return buff.getInt(nSets);
	}	
	public void setNSets(int val) {
		buff.putInt(nSets, val);
	}
	public int getWMOID() {
		return buff.getInt(wmoID);
	}
	public void setWMOID(int val){
		buff.putInt(wmoID,val);
	}
	
	public byte[] getColors() {
		byte ret[] = new byte[4];
		
		buff.position(colR);
		for(int i=0; i<4; i++)
			ret[i] = buff.get();
		
		return ret;
	}	
	public float[] getBondaryPoint1() {
		float ret[] = new float[3];
		
		buff.position(bb1x);
		for(int i=0; i<3; i++)
			ret[i] = buff.getFloat();
		
		return ret;
	}
	public float[] getBondaryPoint2() {
		float ret[] = new float[3];
		
		buff.position(bb2x);
		for(int i=0; i<3; i++)
			ret[i] = buff.getFloat();
		
		return ret;
	}	
}
