package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


/**
 * Doodad's that get spawned in this wmo file
 * 
 * http://madx.dk/wowdev/wiki/index.php?title=WMO#MODD_chunk
 * 
 * @author ganku
 *
 */

public class MODD extends chunk {
	public MODD_Entry entrys[] = null;
	MODD_Entry newentrys[];
	int length;
	/**
	 * Reads in from given pointer set in a databuffer
	 * @param pointer
	 */
	public MODD(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "DDOM");

		int size = buff.getInt();
		buff.limit(buff.position() + size);
		
		// load Entrys
		length= size/MODD_Entry.end;
		entrys = new MODD_Entry[length];
		for(int i=0; i<length; i++) {
			entrys[i] = new MODD_Entry(buff);
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MODD(){
		super(data);
		byte magic[] = { 'D', 'D', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	/**
	 * Allocate our memory .. render all sub-objects
	 */
	public void render() {
		// calculate out size
		
		
		// do we had any changes?
		//if(isChanged()) {
			// re - create all memory
			
			// check for existence
			int l = getLenght();
			
			System.out.println(l + " modd appeareances.");
			
			// create new buffer
			
			ByteBuffer tmp = doRebirth( l*MODD_Entry.end + data );

			// add magic
			tmp.position(magic);
			byte magic[] = { 'D', 'D', 'O', 'M'};
			tmp.put(magic);
			
			// copy all data
			tmp.position(data);
			for(int c=0; c<length; c++)
				if(entrys[c]!=null) {
					entrys[c].buff.position(0);
					tmp.put(entrys[c].get());
					
				}
					
			// kick out old memory
			buff = tmp;
			
			// write new size
			writeSize();
			
			// re init out data objects
			buff.position(0x8);
			
			entrys = new MODD_Entry[l];
			for(int c=0; c<l; c++)
				entrys[c] = new MODD_Entry(buff);
			
		//}
		
		buff.position(0);
	}
	
	/**
	 * Creates a new Doodad Entry
	 * ::Rase the nDoodads in MOHD!
	 * @param NameIndex int
	 * @param pos float[3]
	 * @param rot float[4]
	 * @param scale float
	 * @param col byte[4]
	 * @return
	 */
	public int addDoodad(int NameIndex, float[] pos, float[]rot, float scale, byte[] col) {
		// make new array of objects
		MODD_Entry tmp[] = new MODD_Entry[length +1];
		
		// copy all data
		for(int c=0; c<length; c++) {
			tmp[c]=entrys[c];
		}
		
		// remove old references
		entrys = tmp;
		
		// create newest entry
		entrys[length] = new MODD_Entry();
		entrys[length].setNameIndex(NameIndex);
		entrys[length].setPositionX(pos[0]);
		entrys[length].setPositionY(pos[1]);
		entrys[length].setPositionZ(pos[2]);
		entrys[length].setRotationA(rot[0]);
		entrys[length].setRotationB(rot[1]);
		entrys[length].setRotationC(rot[2]);
		entrys[length].setRotationD(rot[3]);
		entrys[length].setScale(scale);
		entrys[length].setColors(col);
		// set new number
		length++;
		
		// we have a new size now
		Change();
		
		return length -1;
	}

	/**
	 * Returns how many entrys we have
	 */
	public int getLenght() {
		if(entrys==null) return 0;
		else return entrys.length;
	}
}