package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.ZeroPaddedString;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * List of group names in the map object
 * http://www.madx.dk/wowdev/wiki/index.php?title=WMO#MOGN_chunk
 * 
 * Note: The data structure in this chunk is a bit strange.
 * 		im sure there 2Byte of unknown data in front of our stuff.
 * 		But since i cannot tell for sure we will handle it as
 * 		padded string ..
 * 
 * @author ganku
 *
 */
public class MOGN extends chunk {
	ZeroPaddedString entrys[] = null;
	
	/*000h*/public final static int unkn		= data	+	0x0;
			public final static int strstart	= data	+	0x2;
	/**
	 * loads cound strings from the given pointer and
	 * hold it into this chunk.
	 * 
	 * @param pointer
	 * @param count
	 */
	public MOGN(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "NGOM");

		int size = buff.getInt();
		buff.limit(buff.position() + size);
		
		// how many strings?
		int count = 0;
		
		buff.position(strstart);
		
		byte chr=0, old_chr=1;
		while(buff.hasRemaining()) {
			chr = buff.get();
			if(chr!=old_chr) {
				if(chr==0) count++;
				old_chr = chr;
			}
		}
		
		// catch strings
		buff.position(strstart);
		entrys = new ZeroPaddedString[count];
		for(int i=0; i<count; i++) {
			entrys[i] = new ZeroPaddedString(buff);
		}

		// push
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOGN() {
		super(data);
		byte magic[] = { 'N', 'G', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	/**
	 * Returns the zeropaddedstring at index as normal string
	 * @param index
	 * @return
	 */
	public String getString(int index) {
		if(entrys==null) return null;
		if(index<0 || index>entrys.length) return null;
		return entrys[index].toString();
	}
	
	/**
	 * Number of strings we have
	 */
	public int getLenght() {
		if(entrys==null) return 0;
		else return entrys.length;
	}
}
