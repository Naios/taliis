package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Vertex colors, 4 bytes per vertex (BGRA),
 * for WMO groups using indoor lighting.
 *
 * 
 * @author Tigurius
 *
 */

public class MOCV extends chunk {
	int size;
	
	byte[][] color;
	byte[][] newcolor;
	boolean haschanged;
	public int count;

	public MOCV(ByteBuffer pointer) throws ChunkNotFoundException {

		super(pointer, "VCOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		count = size/4;//we have 2bytes per triangle
		color=new byte[count][4];
		for(int i=0;i<count;i++){
			for(int j=0;j<4;j++)
				color[i][j]=buff.get();
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	/**
	 * Returns the given Color
	 * 
	 * @param nr
	 * @return color byte[4]
	 */
	public byte[] getColor(int nr){
		return color[nr];
	}
	
	/**
	 * Sets the given color to given value
	 * 
	 * @param nr int
	 * @param col byte[4]
	 */
	public void setColor(int nr, byte[] col){
		color[nr]=col;
		
	}
	
	public void render(){
		if(haschanged==true){
		ByteBuffer tmp = doRebirth(data + size);
		
		for(int i=0;i<count;i++)
			for(int j=0;j<4;j++)
				tmp.put(newcolor[i][j]);
		
		buff=tmp;
		writeSize();
		
		buff.position(0);
		}
		else buff.position(0);
	}
	
	public void setnewsize(int val, boolean inject){
		newcolor=new byte[val][4];
		if(inject==true)
		for(int i=0;i<color.length;i++)
			newcolor[i]=color[i];
		count=val;
		size=count*4;
		writeSize();
		haschanged=true;
	}
	
}