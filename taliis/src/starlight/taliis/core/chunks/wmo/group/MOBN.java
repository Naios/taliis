package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;
import starlight.taliis.core.chunks.adt.MDDF_Entry;

/**
 * 
 *
 * @author Tigurius
 *
 */

public class MOBN extends chunk {
	public MOBN_Entry entrys[]=null;
	int size, length;
	public MOBN(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "NBOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		length = size/MOBN_Entry.end;
		entrys=new MOBN_Entry[length];
		for(int i=0;i<length;i++)
			entrys[i]=new MOBN_Entry(buff);
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOBN(){
		super(data);
	}
	
	
	/**
	 * Allocate our memory .. render all sub-objects
	 */
	public void render() {
		// calculate out size
		int l = 0;
		
		// do we had any changes?
		//if(isChanged()) {
			// re - create all memory
			
			// check for existence
			l = getLenght();
			
			System.out.println(l + " mobn appeareances.");
			
			// create new buffer
			
			ByteBuffer tmp = doRebirth( l*MOBN_Entry.end + data );

			// add magic
			tmp.position(magic);
			byte magic[] = { 'N', 'B', 'O', 'M'};
			tmp.put(magic);
			
			// copy all data
			tmp.position(data);
			for(int c=0; c<length; c++)
				if(entrys[c]!=null) {
					entrys[c].buff.position(0);
					tmp.put( entrys[c].buff );
				}
					
			// kick out old memory
			buff = tmp;
			
			// write new size
			writeSize();
			
			// re init out data objects
			buff.position(0x8);
			
			entrys = new MOBN_Entry[l];
			for(int c=0; c<l; c++)
				entrys[c] = new MOBN_Entry(buff);
			
		//}
		
		buff.position(0);
	}
	
	public void setnewSize(int val, boolean inject){
		MOBN_Entry[] temp= new MOBN_Entry[val];
		if(inject==true)
			for(int i=0;i<entrys.length;i++)
				temp[i]=entrys[i];
		entrys=temp;
	}
	
	public int getLenght() {
			if(entrys == null) return 0;
			int l = 0;
			
			for(MOBN_Entry e : entrys) l++;

			// well .. just to make sure 
			length = l;
			return l;
		}
	
}

