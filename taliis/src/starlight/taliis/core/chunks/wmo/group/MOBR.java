package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Triangle indices (in MOVI which define triangles)
 * to describe polygon planes defined by MOBN BSP nodes.
 * 
 * COLLISION!!!
 * 
 * shorts...
 * 
 * @author Tigurius
 *
 */

public class MOBR extends chunk {
	int size;
	short vindex[];
	public MOBR(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "RBOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		int nTriangles = size/2;//we have 2bytes per triangle
		vindex=new short[nTriangles];
		for(int i=0;i<nTriangles;i++){
			vindex[i]=buff.getShort();
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOBR(){
		super(data);
		byte magic[] = { 'R', 'B', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		vindex=new short[0];
		// reset position
		buff.position(0);	
	}
	
	public MOBR(int nr){
		super(data+(2*nr));
		byte magic[] = { 'R', 'B', 'O', 'M'};
		buff.put(magic);	// magic
		buff.putInt(2*nr);		// size
		size=2*nr;
		vindex=new short[nr];
		buff.position(0);
	}
	
	public void render(){
		size=vindex.length*2;
		ByteBuffer tmp = doRebirth(data + size);
		
		for(int i=0;i<vindex.length;i++)
				tmp.putShort(vindex[i]);
		
		buff=tmp;
		writeSize();
		buff.position(0);
	}
	
	/**
	 * Returns the given Triangle index
	 * 
	 * @param nr int
	 * @return triangleindex short
	 */
	public short getindex(int nr){
		return vindex[nr];
	}
	
	/**
	 * Sets the given Triangle
	 * 
	 * @param nr int
	 * @param val short
	 */
	public void setTriangle(int nr, short val){
		vindex[nr]=val;
	}
	
	public void setnewSize(int val){
		short[] index=new short[val];
		if(vindex.length<index.length)
		for(int i=0;i<vindex.length;i++){
			index[i]=vindex[i];
		}
		
		vindex=index;
		
	}
}