package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * 
 *
 * @author Tigurius
 *
 */

public class MOBA extends chunk {
	public MOBA_Entry entrys[]=null;
	int size, length;
	public MOBA(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "ABOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		length = size/MOBA_Entry.end;
		entrys=new MOBA_Entry[length];
		for(int i=0;i<length;i++)
			entrys[i]=new MOBA_Entry(buff);
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOBA(){
		super(data);
	}
	
	
	/**
	 * Allocate our memory .. render all sub-objects
	 */
	public void render() {
		// calculate out size
		int l = 0;
		
		// do we had any changes?
		//if(isChanged()) {
			// re - create all memory
			
			// check for existence
			l = entrys.length;
			
			System.out.println(l + " moba appeareances.");
			
			// create new buffer
			
			ByteBuffer tmp = doRebirth( l*MOBA_Entry.end + data );

			// add magic
			tmp.position(magic);
			byte magic[] = { 'A', 'B', 'O', 'M'};
			tmp.put(magic);
			
			// copy all data
			tmp.position(data);
			for(int c=0; c<l; c++)
				if(entrys[c]!=null) {
					entrys[c].render();
					tmp.put(entrys[c].buff);
					System.out.println("MOBA\t"+c);
				}
					
			// kick out old memory
			buff = tmp;
			
			// write new size
			writeSize();
			
			// re init out data objects
			buff.position(0x8);
			
			entrys = new MOBA_Entry[l];
			for(int c=0; c<l; c++)
				entrys[c] = new MOBA_Entry(buff);
			
		//}
		
		buff.position(0);
	}
	
	public void setnewSize(int val, boolean inject){
		MOBA_Entry[] temp= new MOBA_Entry[val];
		if(inject==true)
			for(int i=0;i<entrys.length;i++)
				temp[i]=entrys[i];
		entrys=temp;
		writeSize();
	}
	
	public int getLenght() {
			if(entrys == null) return 0;
			int l = 0;
			
			for(MOBA_Entry e : entrys) l++;

			// well .. just to make sure 
			length = l;
			return l;
		}
	
}

