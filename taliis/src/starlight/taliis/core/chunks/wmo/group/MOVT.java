package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


/**
 * Vertices chunk. 3 floats per vertex,
 * the coordinates are in (X,Z,-Y) order.
 * @author Tigurius
 *
 */

public class MOVT extends chunk {
	int size;
	
	public float[][] index;
	public float[][] newindex;
	public int nVertices;
	boolean haschanged=false;
	
	public MOVT(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "TVOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		nVertices = size/12;//we have 3*4bytes per triangle
		
		index= new float[nVertices][3];
		for(int i=0;i<nVertices;i++){
			for(int j=0;j<3;j++)
				index[i][j]=buff.getFloat();
			
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOVT(){
		super(data);
		byte magic[] = { 'T', 'V', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	/**
	 * Returns the Position of the given Vertex
	 * ::to use if size hasn't changed!
	 * 
	 * @param nr
	 * @return position float[3]
	 */
	public float[] getVertexPos(int nr){
		return index[nr];
	}
	
	/**
	 * Sets the Position of the given Vertex
	 * ::to use if size hasn't changed!
	 * 
	 * @param nr int
	 * @param val float[3]
	 */
	public void setVertexPos(int nr, float[] val){
		index[nr]=val;
		
	}
	
	/**
	 * Returns the Position of the given Vertex
	 * ::to use  if changed the size!
	 * 
	 * @param nr
	 * @return position float[3]
	 */
	public float[] getnewVertexPos(int nr){
		return newindex[nr];
	}
	
	/**
	 * Sets the Position of the given Vertex
	 * ::to use  if changed the size!
	 * 
	 * @param nr int
	 * @param val float[3]
	 */
	public void setnewVertexPos(int nr, float[] val){
		newindex[nr]=val;
		
	}
	
	/**
	 * Sets the capacity of the MOVT-chunk to the given value
	 * 
	 * @param val
	 * @param replace boolean
	 */
	public void setnewSize(int val, boolean inject){
		newindex=new float[val][3];
		if(inject==true)
		for(int i=0;i<index.length;i++)
			newindex[i]=index[i];
		nVertices=val;
		size=nVertices*12;
		haschanged=true;
	}
	
	public void render(){
		if(haschanged==true){
		ByteBuffer tmp = doRebirth(data + size);
		
		for(int i=0;i<nVertices;i++)
			for(int j=0;j<3;j++)
				tmp.putFloat(newindex[i][j]);
		
		buff=tmp;
		writeSize();
		
		buff.position(0);
		}
		else buff.position(0);
	}
	
}
	