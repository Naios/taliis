package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 *Texture coordinates, 2 floats per vertex in (X,Y) order.
 *The values range from 0.0 to 1.0.
 *Vertices, normals and texture coordinates
 *are in corresponding order, of course. 
 *
 * @author Tigurius
 *
 */

public class MOTV extends chunk {
	int size;
	float[][] index;
	float[][] newindex;
	public int nTexVertices;
	boolean haschanged=false;
	
	public MOTV(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "VTOM");

		size = buff.getInt();
		buff.limit(data + size);
		nTexVertices = size/8;//we have 2*4bytes per triangle
		
		index=new float[nTexVertices][2];
		for(int i=0;i<nTexVertices;i++){
			index[i][0]=buff.getFloat();
			index[i][1]=buff.getFloat();
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOTV(){
		super(data);
		byte magic[] = { 'V', 'T', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	
	
	/**
	 * Returns the x and y value for the given TextureVertex
	 * ::use if size hasn't changed!
	 * 
	 * @param nr int
	 * @return TextureVertex float[2]
	 */
	public float[] getTexVertex(int nr){
		return index[nr];
	}
	
	/**
	 * Sets the TexVertex of the given to the given val
	 * ::use if size hasn't changed!
	 * @param nr
	 * @param val float[2]
	 */
	public void setTexVertex(int nr, float[] val){
		index[nr]=val;
		
	}
	
	/**
	 * Sets the TexVertex of the given to the given val
	 * ::use if size has changed!
	 * @param nr
	 * @param val float[2]
	 */
	public void setnewTexVertex(int nr, float[] val){
		newindex[nr]=val;
		
	}
	
	/**
	 * Sets the capacity of the MOTV-chunk to the given value
	 * 
	 * @param val
	 */
	public void setnewSize(int val, boolean inject){
		newindex=new float[val][2];
		if(inject==true)
		for(int i=0;i<index.length;i++)
			newindex[i]=index[i];
		nTexVertices=val;
		size=nTexVertices*8;
		
		haschanged=true;
	}
	
	public void render(){
		if(haschanged==true){
		ByteBuffer tmp = doRebirth(data + size);
		
		for(int i=0;i<nTexVertices;i++)
			for(int j=0;j<2;j++)
				tmp.putFloat(newindex[i][j]);
		
		buff=tmp;
		writeSize();
		
		buff.position(0);
		}
		else buff.position(0);
	}
	
	
	
}
	