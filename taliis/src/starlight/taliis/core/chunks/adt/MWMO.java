package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;

import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * A list of WMO files used by the terrain in this map tile. A contiguous block
 * of zero-terminated strings which are complete filenames with paths. The
 * textures will later be identified by their position in this list.
 * 
 * @see starlight.taliis.core.chunks.wdt.MWMO
 */
public final class MWMO extends chunk {
	public ZeroTerminatedString entrys[];
	int length = 0;

	/**
	 * Create an empty MWMO chunk.
	 */
	public MWMO() {
		super(0x8);
		byte magic[] = { 'O', 'M', 'W', 'M' };

		buff.put(magic); // magic
		buff.putInt(0); // size

		// reset position
		buff.position(0);

		if (DEBUG)
			System.out.println("Empty MWMO created.");
	}

	/**
	 * Read a MWMO chunk from a buffer.
	 * 
	 * @param pointer A pointer on the buffer.
	 * @throws ChunkNotFoundException Thrown, if the chunk is not found.
	 */
	public MWMO(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "OMWM");

		// get size
		int size = buff.getInt();
		buff.limit(size + data);

		// count 0 (string endings)
		while (buff.hasRemaining()) {
			byte t = buff.get();
			if (t == 0)
				length++;
		}

		// init strings
		entrys = new ZeroTerminatedString[length];
		buff.position(data);
		for (int i = 0; i < length; i++)
			entrys[i] = new ZeroTerminatedString(buff);

		// push pointer
		pointer.position(pointer.position() + buff.limit());
	}

	/**
	 * Adds new string to our filenames list.
	 * 
	 * @param text The filename to add.
	 * @return The file-index of the new string.
	 */
	public int addString(String text) {
		ZeroTerminatedString tmp[] = new ZeroTerminatedString[length + 1];
		for (int c = 0; c < length; c++)
			tmp[c] = entrys[c];
		tmp[length] = new ZeroTerminatedString(text);

		entrys = tmp;
		length++;
		Change();
		return length - 1;
	}

	/**
	 * @deprecated Please fix spelling and use getLength().
	 * @return number of strings in this file
	 */
	@Deprecated
	@Override
	public int getLenght() {
		return length;
	}

	/**
	 * Get the number of filenames in the chunk.
	 * 
	 * @return number of strings
	 */
	@Override
	public int getLength() {
		return length;
	}

	/**
	 * Get the filename of a texture.
	 * 
	 * @param index The texture id.
	 * @return The texture filename.
	 */
	public String getValue(int index) {
		if (index > length)
			return null;
		else
			return entrys[index].toString();
	}

	/**
	 * Get the filename of a texture.
	 * 
	 * @param index The texture id.
	 * @return The texture filename.
	 */
	public String getValueNo(int index) {
		if (index > length)
			return null;
		else
			return entrys[index].toString();
	}

	/**
	 * Removes the String with the given index.
	 * <br><br>
	 * <b>!! This can cause RAPID display failures by re-ordered indices !!</b>
	 * 
	 * @param index Which String to remove.
	 */
	public void removeString(int index) {
		// make new array of objects
		ZeroTerminatedString tmp[] = new ZeroTerminatedString[length - 1];

		// copy data in front of killed item
		for (int c = 0; c < index; c++)
			tmp[c] = entrys[c];

		// copy data in front of killed item
		for (int c = index + 1; c < length; c++)
			tmp[c - 1] = entrys[c];

		// remove old references
		entrys = tmp;

		// set new number
		length--;

		// we have a new size now
		Change();
	}

	/**
	 * Render the data to write it back to a file.
	 */
	@Override
	public void render() {
		int s = 0;
		// calc needed size (+ zero)
		for (int c = 0; c < length; c++)
			s += entrys[c].getLenght() + 1;

		// allocate
		ByteBuffer tmp = doRebirth(s + data);

		// write down
		for (int c = 0; c < length; c++) {
			entrys[c].buff.position(0);
			tmp.put(entrys[c].buff);
			tmp.put((byte) 0);
		}
		buff = tmp;

		// reset pointers
		writeSize();
		buff.position(0);
	}
}
