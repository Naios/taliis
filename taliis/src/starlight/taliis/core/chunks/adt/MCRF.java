package starlight.taliis.core.chunks.adt;

/**
 * A list of indices into the parent file's MDDF chunk,
 * saying which MCNK subchunk those particular MDDF doodads
 * are drawn within. This MCRF list contains duplicates for
 * map doodads that overlap areas. 
 * But I guess that's what the MDDF's UniqueID field is for.
 * ... 
 * 
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MCRF extends chunk {
	int size;
	int lenght;
	
	/*
	 * 0x0	magic
	 * 0x4	size
	 * 0x8	data
	 */
	
	public MCRF(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "FRCM");

		size = buff.getInt();
		lenght = size / 4;
		
		
		buff.limit(buff.position() + size);
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MCRF() {
		super(data);
		byte magic[] = { 'F', 'R', 'C', 'M'};
			
		buff.put(magic);		// magic
		buff.putInt(0);			// size
		
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("  MCRF created.");
	}
	/**
	 * Return the number of given entrys
	 * @return number of entrys
	 */
	public int getLenght() {
		return lenght;
	}
	
	/**
	 * Get entry value
	 * @param index index of entry that vaue we want
	 * @return the value
	 */
	public int getEntry(int index) {
		return buff.getInt(data + index*4);
	}
	
	/**
	 * Set the value of given index
	 * @param index
	 * @param value
	 */
	public void setEntry(int index, int value) {
		buff.putInt(data + index*4, value);
	}

	/**
	 * This will insert a new value at the beginning of 
	 * all values. This is to cause problems with WMO entrys
	 * that may be located on its end.
	 * 
	 * @param value
	 */
	public void insertEntry(int value) {
		// re-birth our buffer
		ByteBuffer tmp = doRebirth(buff.limit() + 4);
		
		// write in the new entry
		tmp.putInt(value);
		
		// copy all old entrys
		buff.position(data);
		while (buff.hasRemaining())
			tmp.put(buff.get());
		buff = tmp;
		
		lenght++;
		writeSize();
	}
	
	/**
	 * This appends a new Entry at the end of the list.
	 * This is dangerous because it may be that allready
	 * WMO entrys got stored there.
	 * So this function should be only used for WMO references.
	 * @param value
	 */
	public void appendEntry(int value) {
		// expand our memory by one value;
		expand(4);
		lenght++;
		buff.putInt(value);
		writeSize();
	}
}