package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Lists the relative offsets of string beginnings in the above MMDX chunk.
 * (sort of redundant) <br>
 * One 32-bit integer per offset.
 * 
 * @author conny
 * 
 */
public final class MWID extends chunk {
	int size;

	/**
	 * Read a chunk from a buffer.
	 * 
	 * @param pointer The buffer.
	 * @throws ChunkNotFoundException If you don't have a MWID chunk there.
	 */
	public MWID(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "DIWM");

		size = buff.getInt();

		buff.limit(size + data);
		pointer.position(pointer.position() + buff.limit());
	}

	/**
	 * Create a new and empty one.
	 */
	public MWID() {
		super(0x8);
		byte magic[] = { 'D', 'I', 'W', 'M' };

		buff.put(magic); // magic
		buff.putInt(0); // size

		// reset position
		buff.position(0);

		if (DEBUG)
			System.out.println("Empty MWID created.");
	}

	/**
	 * Catch all offsets and write them down.
	 * 
	 * @param pointer The matching MWMO chunk.
	 */
	public void render(MWMO pointer) {
		super.render();
		if (pointer.getLength() == 0)
			return;
		int ns = pointer.getLength() * 4;

		// extend our object
		buff = doRebirth(ns + data);
		writeSize();

		// catch up all offsets
		buff.putInt(0);

		// look into all memory except header
		int max = pointer.getSize() - data;
		byte mem[] = new byte[max];
		pointer.buff.position(data);
		pointer.buff.get(mem);

		for (int c = 1; c < max; c++)
			// +1 coz we want offset _after_ NULL
			if (mem[c - 1] == 0)
				buff.putInt(c);

		buff.position(0);
		pointer.buff.position(0);
	}

	/**
	 * @param index Which item in the list?
	 * @return The offset.
	 */
	public int getOffsetNo(int index) {
		buff.position(data + index * 4);
		return buff.getInt();
	}

	@Override
	@Deprecated
	public int getLenght() {
		return (getSize() - data) / 4;
	}

	/**
	 * Get the size of the chunk.
	 * 
	 * @return The size.
	 */
	@Override
	public int getLength() {
		return (getSize() - data) / 4;
	}
}
