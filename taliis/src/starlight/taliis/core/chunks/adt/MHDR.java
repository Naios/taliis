package starlight.taliis.core.chunks.adt;

/**
 * Header of ADT file - also called MHDR Chunk
 * Includes all offsets of other chunks in the file
 * 
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MHDR extends chunk {

	/* Chunk Header (0x8) */
 	 /*000h*/  public final static int flags 				= data + 0x0;
	 /*004h*/  public final static int offsInfo 		= data + 0x4;		
	 /*008h*/  public final static int offsTex 			= data + 0x8;		
	 /*00Ch*/  public final static int offsModels 		= data + 0xC;		
	 /*010h*/  public final static int offsModelsIds 	= data + 0x10;		
	 /*014h*/  public final static int offsMapObjects 	= data + 0x14;		
	 /*018h*/  public final static int offsMapObjectsIds= data + 0x18;		
	 /*01Ch*/  public final static int offsDoodsDef 	= data + 0x1C;		
	 /*020h*/  public final static int offsObjectsDef 	= data + 0x20;	
	 /*024h*/  public final static int offsFightBoundary= data + 0x24;	
	 /*028h*/  public final static int offsMH2O 		= data + 0x28;		 
	 /*02Ch*/  public final static int offsMTFX			= data + 0x2C;
	 
	 /*030h*/  public final static int pad4 = 0x38;		
	 /*034h*/  public final static int pad5 = 0x3c;		
	 /*038h*/  public final static int pad6 = 0x40;		
	 /*03Ch*/  public final static int pad7 = 0x44;
	 
	 /*0x40*/  public final static int end 				= data + 0x40;

	 /** 
	  * value that need to be added to make this offsets
	  * be global file offsets. Only set if the file get loaded!
	  */
	 int global_offset = -1;
	 
	 /**
	  * Init the data buffer by get a buffer that is allready 
	  * set to the correct position. push pointer forward to 
	  * his end.
	  * @param pointer ByteBuffer with the opened File
	  */
	 public MHDR(ByteBuffer pointer) throws ChunkNotFoundException {
		 super(pointer, "RDHM");
		 buff.limit(end);
		
		 // global offset
		 global_offset = pointer.position() + data;
		 
		 // push pointers
		 pointer.position( pointer.position() + end);
	 }
	 
	 /**
	  * Create new header
	  */
	 public MHDR() {
		 super(end);
		 byte magic[] = { 'R', 'D', 'H', 'M'};
			
		buff.put(magic);	// magic
		buff.putInt(end - 8);	// size
		 
		// reset position
		buff.position(0);	
		
		if(DEBUG) System.out.println("Empty MHDR created.");
	 }
	 
	 /**
	  * Prepares for new Data.
	  * In this case it will kill out ALL offsets. This because
	  * its possible that our ADT class dont know all chunks.
	  * Unknown offsets need to be killed in order to keep
	  * the file valid.
	  */
	 public void render() {
		 buff = doRebirth(end);
		 buff.position(0);
	 }

	 /**
	  * The header offsets are relative from the beginning
	  * of this chunks data offset. This function returns
	  * the needed value to calculate global offsets.
	  */
	 public int getBaseOffs() {
		 return global_offset;
	 }
	 
	 public void setFlags(int flag){
		 buff.putInt(flags,flag);
	 }
	 public int getFlags(){
		 return buff.getInt(flags);
	 }
	 
	 /**
	  * Function to set infos offset 
	  * @param offset
	  */
	 public void setInfoOffs(int offset) {
		 buff.putInt(offsInfo, offset);
	 }
	 public int getInfoOffs() {
		 return buff.getInt(offsInfo);
	 }
	 
	 public void setTexOffs(int offset) {
		 buff.putInt(offsTex, offset);
	 }
	 public int getTexOffs() {
		 return buff.getInt(offsTex);
	 }
	 
	 public void setModelOffs(int offset) {
		 buff.putInt(offsModels, offset);
	 }
	 public int getModelOffs() {
		 return buff.getInt(offsModels);
	 }
	 
	 public void setModelIDOffs(int offset) {
		 buff.putInt(offsModelsIds, offset);
	 }
	 public int getModelIDOffs() {
		 return buff.getInt(offsModelsIds);
	 }
	 
	 public void setMapObjOffs(int offset) {
		 buff.putInt(offsMapObjects, offset);
	 }
	 public int getMapObjOffs() {
		 return buff.getInt(offsMapObjects);
	 }
	 public void setMapObjIDOffs(int offset) {
		 buff.putInt(offsMapObjectsIds, offset);
	 }
	 public int getMapObjIDOffs() {
		 return buff.getInt(offsMapObjectsIds);
	 }
	 public void setDooDsDefOffs(int offset) {
		 buff.putInt(offsDoodsDef, offset);
	 }
	 public int getDooDsDefOffs() {
		 return buff.getInt(offsDoodsDef);
	 }
	 public void setObjDefOffs(int offset) {
		 buff.putInt(offsObjectsDef, offset);
	 }
	 public int getObjDefOffs() {
		 return buff.getInt(offsObjectsDef);
	 }
	 public void setoffsFlightBoundary(int offset) {
		 buff.putInt(offsFightBoundary, offset);
	 }
	 public int getoffsFlightBoundary() {
		 return buff.getInt(offsFightBoundary);
	 }
	 public void setoffsMH2O(int offset) {
		 buff.putInt(offsMH2O, offset);
	 }
	 public int getoffsMH2O() {
		 return buff.getInt(offsMH2O);
	 }
	 public void setoffsMTFX(int offset) {
		 buff.putInt(offsMTFX, offset);
	 }
	 public int getoffsMTFX() {
		 return buff.getInt(offsMTFX);
	 }

}
