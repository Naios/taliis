package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * MCCV bblablablabla
 * 
 * The chunk contains 145 color values,
 * each one is designated to one of the heightmap vertices.
 * @author ganku
 * +
 * @author tigurius
 *
 */

public class MCCV extends chunk {
	int size;
	
	public MCCV(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "VCCM");

		size = buff.getInt();

		buff.limit(buff.position() + size);
		pointer.position( pointer.position() + buff.limit());
	}
	
	/**
	 * No creation routine done atm !!
	 */
	public MCCV() {
		super(145*4 + data);
		
		byte magic[] = { 'V', 'C', 'C', 'M'};	
		buff.put(magic);		// magic
		buff.putInt(145*4);			// size
		
		buff.position(0);
		if(DEBUG) System.out.println("  MCCV created.");
	}
	
	public byte getRed(int index){
		return buff.get(data + index*4);
	}
	public byte getGreen(int index){
		return buff.get(data + index*4+1);
	}
	public byte getBlue(int index){
		return buff.get(data + index*4+2);
	}
	public byte getAlpha(int index){
		return buff.get(data + index*4+3);
	}
	public void setRed(int index, byte val){
		buff.put(data + index*4, val);
	}
	public void setGreen(int index, byte val){
		buff.put(data + index*4+1, val);
	}
	public void setBlue(int index, byte val){
		buff.put(data + index*4+2, val);
	}
	public void setAlpha(int index, byte val){
		buff.put(data + index*4+3, val);
	}
	
	/**
	 * Sets a value on its given x/y coordinates.
	 * This ignores LOD so the dimension of your coordinates are 9x9!
	 * @param x
	 * @param y
	 * @param val
	 */
	public void setRedNoLOD(int x, int y, byte val) {
		int add = y*8;
		
		buff.put(data + (y*9 + x + add)*4, val);
	}
	public byte getRedNoLOD(int x, int y) {
		int add = y*8;
		
		return buff.get(data + (y*9 + x + add)*4);
	}
	public void setGreenNoLOD(int x, int y, byte val) {
		int add = y*8;
		
		buff.put(data + (y*9 + x + add)*4+1, val);
	}
	public byte getGreenNoLOD(int x, int y) {
		int add = y*8;
		
		return buff.get(data + (y*9 + x + add)*4+1);
	}
	public void setBlueNoLOD(int x, int y, byte val) {
		int add = y*8;
		
		buff.put(data + (y*9 + x + add)*4+2, val);
	}
	public byte getBlueNoLOD(int x, int y) {
		int add = y*8;
		
		return buff.get(data + (y*9 + x + add)*4+2);
	}
	public void setAlphaNoLOD(int x, int y, byte val) {
		int add = y*8;
		
		buff.put(data + (y*9 + x + add)*4+3, val);
	}
	public byte getAlphaNoLOD(int x, int y) {
		int add = y*8;
		
		return buff.get(data + (y*9 + x + add)*4+3);
	}
	/**
	 * Sets a LOD value.
	 * This ignores standard values so your dimension is 8x8!
	 * @param x
	 * @param y
	 * @param val
	 */
	public void setRedLOD(int x, int y, byte val) {
		int add = (y+1)*9;
		
		buff.put(data + (y*8 + x + add)*4, val);
	}
	public void setGreenLOD(int x, int y, byte val) {
		int add = (y+1)*9;
		
		buff.put(data + (y*8 + x + add)*4+1, val);
	}
	public void setBlueLOD(int x, int y, byte val) {
		int add = (y+1)*9;
		
		buff.put(data + (y*8 + x + add)*4+2, val);
	}
	public void setAlphaLOD(int x, int y, byte val) {
		int add = (y+1)*9;
		
		buff.put(data + (y*8 + x + add)*4+1, val);
	}
}
