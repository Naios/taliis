package starlight.taliis.core.chunks.adt;

/**
 * sound emiters
 * i know nothing about them .. and i do not realy care ..
 *
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MCSE extends chunk {
	int size;
	
	/*
	 * 0x0 magic
	 * 0x4 size
	 * 0x8 data ->
	 */
	
	 /*000h*/  public final static int soundPointID = 0x0;		
	 /*004h*/  public final static int soundNameID = 0x4;		
	 /*008h*/  public final static int pos1 = 0x8;	// float[3]
	 /*00Ch*/   		
	 /*010h*/  		
	 /*014h*/  public final static int minDistance = 0x14;	 	// float	
	 /*018h*/  public final static int maxDistance = 0x18;		// float
	 /*01Ch*/  public final static int cutoffDistance = 0x1c;	//float	
	 /*020h*/  public final static int startTime = 0x20;	
	 /*022h*/  public final static int endTime = 0x22;
	 /*024h*/  public final static int groupSilenceMin = 0x24;		
	 /*026h*/  public final static int groupSilenceMax = 0x26; 	
	 /*028h*/  public final static int playInstancesMin = 0x28;
	 /*02Ah*/  public final static int playInstancesMax = 0x2a;	
	 /*02Ch*/  public final static int loopCountMin = 0x2c;
	 /*02Eh*/  public final static int loopCountMax = 0x2e;
	 /*030h*/  public final static int interSoundGapMin = 0x30;
	 /*032h*/  public final static int interSoundGapMax = 0x32;
	 
	 /*0x34*/	public final static int end = 0x3c;
	
	MCSE(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "ESCM");

		size = buff.getInt();
		
		buff.limit(buff.position() + size);
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MCSE() {
		super(end);
		byte magic[] = { 'E', 'S', 'C', 'M'};
			
		buff.put(magic);		// magic
		buff.putInt(end-data);	// size
		
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("  MCSE created.");
	}
}