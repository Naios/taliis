package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;

public final class MODF_Entry extends memory {
	/*000h*/  public final static int nameId = 0x0;
	/*004h*/  public final static int uniqueId = 0x4;		
	/*008h*/  public final static int posY = 0x8; //float pos[3];		
	/*00Ch*/  public final static int posZ = 0xc;
	/*010h*/  public final static int posX = 0x10;
	/*014h*/  public final static int rotA = 0x14; //float rot[3];	
	/*018h*/  public final static int rotB = 0x18;
	/*01Ch*/  public final static int rotC = 0x1c;		
	
	/*020h*/  public final static int ext1 = 0x20;	// Formula for this is posX+ExtPosX in WMO's DHOM chunk
	/*024h*/  public final static int ext2 = 0x24;	// Formula for this is posY+ExtPosY in WMO's DHOM chunk
	/*028h*/  public final static int ext3 = 0x28;	// Formula for this is posZ+ExtPosZ in WMO's DHOM chunk
	
	/*02Ch*/  public final static int ext4 = 0x2c;	// Formula for this is posX-ExtPosX in WMO's DHOM chunk
	/*030h*/  public final static int ext5 = 0x30;	// Formula for this is posY-ExtPosY in WMO's DHOM chunk
	/*034h*/  public final static int ext6 = 0x34;	// Formula for this is posZ-ExtPosZ in WMO's DHOM chunk

	/*038h*/  public final static int flags = 0x38;	
	
	/*03Ah*/  public final static int doodadset = 0x3c;	//uint16
	/*03Ch*/  public final static int setname = 0x3e;	//uint16
	
	/*040h*/  public final static int end = 0x40;

	public MODF_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		
		pointer.position( pointer.position() + buff.limit() );
		buff.position(0);
	}
	
	
	public MODF_Entry(int strIndex, int uID, float x, float y, float z) {
		super(end);
		
		setNameID(strIndex);
		setUniqID(uID);
		setX(x);
		setY(y);
		setZ(z);
		
		buff.position(0);
	}
	
	/**
	 * Trys to pre calculate an set of valid ext values.
	 */
	public void calcExtValues() {
		float tmp;
		tmp = translate( getX() );
		setU3( translate(tmp+20F));
		setU6( translate(tmp-20F));
		tmp = translate( getY() );
		setU1( translate(tmp+20F));
		setU4( translate(tmp-20F));
		tmp = translate( getZ() );
		setU2( translate(tmp+10F));
		setU5( translate(tmp-10F));
		
		setB(90.0F);
	}
	
	/**
	 * Translates local to global coordinates and back 
	 * @param val coordinate u wish to translate
	 * @return counter value
	 */
	public static float translate(float val) {
		return 32*533.3333F - val;
	}
	
	public int getNameID() {
		return buff.getInt(nameId);
	}
	public void setNameID(int val) {
		buff.putInt(nameId, val);
	}
	
	public int getUniqID() {
		return buff.getInt(uniqueId);
	}
	public void setUniqID(int val) {
		buff.putInt(uniqueId, val);
	}
	
	public float getX() {
		return buff.getFloat(posX);
	}
	public void setX(float val) {
		buff.putFloat(posX, val);
	}
	
	public float getY() {
		return buff.getFloat(posY);
	}
	public void setY(float val) {
		buff.putFloat(posY, val);
	}
	
	public float getZ() {
		return buff.getFloat(posZ);
	}
	public void setZ(float val) {
		buff.putFloat(posZ, val);
	}
	
	public float getA() {
		return buff.getFloat(rotA);
	}
	public void setA(float val) {
		buff.putFloat(rotA, val);
	}
	public float getB() {
		return buff.getFloat(rotB);
	}
	public void setB(float val) {
		buff.putFloat(rotB, val);
	}
	public float getC() {
		return buff.getFloat(rotC);
	}
	public void setC(float val) {
		buff.putFloat(rotC, val);
	}
	
	public short getDoodadSet() {
		return buff.getShort(doodadset);
	}
	public void setDoodadSet(short val) {
		buff.putShort(doodadset, val);
	}
	public short getWMONameSet() {
		return buff.getShort(setname);
	}
	public void setWMONameSet(short val) {
		buff.putShort(setname, val);
	}
	public int getFlags() {
		return buff.getInt(flags);
	}
	public void setFlags(int val) {
		buff.putInt(flags, val);
	}
	
	public float getU1() {
		return buff.getFloat(ext1);
	}
	public float getU2() {
		return buff.getFloat(ext2);
	}
	public float getU3() {
		return buff.getFloat(ext3);
	}
	public float getU4() {
		return buff.getFloat(ext4);
	}
	public float getU5() {
		return buff.getFloat(ext5);
	}
	public float getU6() {
		return buff.getFloat(ext6);
	}
	public void setU1(float val) {
		buff.putFloat(ext1, val);
	}
	public void setU2(float val) {
		buff.putFloat(ext2, val);
	}
	public void setU3(float val) {
		buff.putFloat(ext3, val);
	}
	public void setU4(float val) {
		buff.putFloat(ext4, val);
	}
	public void setU5(float val) {
		buff.putFloat(ext5, val);
	}
	public void setU6(float val) {
		buff.putFloat(ext6, val);
	}
}