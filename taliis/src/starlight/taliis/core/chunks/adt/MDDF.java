package starlight.taliis.core.chunks.adt;

/**
 * Placement information for doodads (M2 models). 36 bytes per model instance.
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MDDF extends chunk {	
	private int lenght = 0;
	public MDDF_Entry entrys[] = null;
	
	/*
	 * 0x0	magic
	 * 0x4	lenght
	 * 0x8	data
	 */
	
	/**
	 * @param pointer Data Buffer
	 * @param ModelCount Number of models we expect 
	 */
	public MDDF(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "FDDM");
		
		// get number of entrys
		int size = buff.getInt();
		lenght = size / MDDF_Entry.end;
				
		// init them
		entrys = new MDDF_Entry[lenght];
		for(int c=0; c<lenght; c++)
			entrys[c] = new MDDF_Entry(buff);
	
		// setup the buffer stuff ...
		buff.limit( data + size );
		pointer.position( pointer.position() + buff.limit() );
	}
	
	public MDDF() {
		 super(data);
		 byte magic[] = { 'F', 'D', 'D', 'M'};
			
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		 
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("Empty MDDF created.");
	}
	
	/**
	 * Add an other Doodad info our object list :)
	 * @param strIndex
	 * @param uID
	 * @param x
	 * @param y
	 * @param z
	 * @return index of this Doodad in the MDDF list
	 */
	public int addDoodad(int strIndex, int uID, float x, float y, float z) {
		// make new array of objects
		MDDF_Entry tmp[] = new MDDF_Entry[lenght +1];
		
		// copy all data
		for(int c=0; c<lenght; c++) {
			tmp[c]=entrys[c];
		}
		
		// remove old references
		entrys = tmp;
		
		// create newest entry
		entrys[lenght] = new MDDF_Entry(strIndex, uID, x, y, z);
		
		// set new number
		lenght++;
		
		// we have a new size now
		Change();
		
		return lenght -1;
	}
	
	/**
	 * A basic function that removes entry index
	 * and rebuilds the list
	 * @param index
	 */
	public void remove(int index) {
		if(index<getLenght()) return;
		// make new array of objects
		MDDF_Entry tmp[] = new MDDF_Entry[lenght -1];
		
		// copy data in front of killed item
		for(int c=0; c<index; c++) {
			tmp[c]=entrys[c];
		}
		
		// copy data in front of killed item
		for(int c=index+1; c<lenght; c++) {
			tmp[c-1]=entrys[c];
		}
		
		// remove old references
		entrys = tmp;

		// set new number
		lenght--;
		
		// we have a new size now
		Change();		
	}
	
	/**
	 * Allocate our memory .. render all sub-objects
	 */
	public void render() {
		// calculate out size
		int l = 0;
		
		// do we had any changes?
		//if(isChanged()) {
			// re - create all memory
			
			// check for existence
			l = getLenght();
			
			System.out.println(l + " dd appeareances.");
			
			// create new buffer
			
			ByteBuffer tmp = doRebirth( l*MDDF_Entry.end + data );

			// add magic
			tmp.position(magic);
			byte magic[] = { 'F', 'D', 'D', 'M'};
			tmp.put(magic);
			
			// copy all data
			tmp.position(data);
			for(int c=0; c<lenght; c++)
				if(entrys[c]!=null) {
					entrys[c].buff.position(0);
					tmp.put( entrys[c].buff );
				}
					
			// kick out old memory
			buff = tmp;
			
			// write new size
			writeSize();
			
			// re init out data objects
			buff.position(0x8);
			
			entrys = new MDDF_Entry[l];
			for(int c=0; c<l; c++)
				entrys[c] = new MDDF_Entry(buff);
			
		//}
		
		buff.position(0);
	}
	
	
	/**
	 * @return number of DD objects stored inside
	 */
	public int getLenght() {
		if(entrys == null) return 0;
		int l = 0;
		
		for(MDDF_Entry e : entrys) l++;

		// well .. just to make sure 
		lenght = l;
		return l;
	}
}