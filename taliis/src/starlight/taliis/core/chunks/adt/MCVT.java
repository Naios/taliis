package starlight.taliis.core.chunks.adt;

/**
 * These are the actual height values for the 9x9+8x8 
 * vertices. 145 floats in the following order/arrangement:
 *
 * 145 is correct but that is NOT 9x9+8x8 <.< however ..
 * 
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MCVT extends chunk {
	int size;
	
	/*
	 * 0x0	magic
	 * 0x4	size
	 * 0x8	data
	 */
	
	public MCVT(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "TVCM");

		size = buff.getInt();
		
		buff.limit(buff.position() + size);
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MCVT() {
		super(145*4 + data);
		byte magic[] = { 'T', 'V', 'C', 'M'};
			
		buff.put(magic);		// magic
		buff.putInt(145*4);		// size
		
		// reset position
		buff.position(0);	
		
		if(DEBUG) System.out.println("  MCTV created.");
	}
	
	public float getVal(int index) {
		return buff.getFloat(data + index*4);
	}
	
	public void setVal(int index, float val) {
		buff.putFloat(data + index*4, val);
	}
	
	/**
	 * Sets a value on its given x/y coordinates.
	 * This ignores LOD so the dimension of your coordinates are 9x9!
	 * @param x
	 * @param y
	 * @param val
	 */
	public void setValNoLOD(int x, int y, float val) {
		int add = y*8;
		
		buff.putFloat(data + (y*9 + x + add)*4, val);
	}
	public float getValNoLOD(int x, int y) {
		int add = y*8;
		
		return buff.getFloat(data + (y*9 + x + add)*4);
	}
	public float getValNoLOD(int index) {	
		int add = 0;
		
		for(int c=index; c>8; c-=9) {
			add+=8;
		}
		
		return buff.getFloat(data + (index + add)*4);
	}
	
	/**
	 * Sets a LOD value.
	 * This ignores standard values so your dimension is 8x8!
	 * @param x
	 * @param y
	 * @param val
	 */
	public float getValLOD(int x, int y) {
		int add = (y+1)*9;
		
		return buff.getFloat(data + (y*8 + x + add)*4);
	}
	
	/**
	 * Sets a LOD value.
	 * This ignores standard values so your dimension is 8x8!
	 * @param x
	 * @param y
	 * @param val
	 */
	public void setValLOD(int x, int y, float val) {
		int add = (y+1)*9;
		
		buff.putFloat(data + (y*8 + x + add)*4, val);
	}
}
