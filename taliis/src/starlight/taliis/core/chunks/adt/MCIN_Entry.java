package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;


public final class MCIN_Entry extends memory {
	 /*000h*/	public final static int offsMCNK = 0x0;
	 /*004h*/	public final static int sizeMCNK = 0x4;		
	 /*008h*/	public final static int flags = 0x8;		
	 /*00Ch*/	public final static int asyncId = 0xc;
	 /*010h*/	public final static int end = 0x10;
	
	public MCIN_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		pointer.position( pointer.position() + buff.limit() );
	}
	
	public int getOffset() {
		return buff.getInt(offsMCNK);
	}
	public void setOffset(int value) {
		buff.putInt(offsMCNK, value);
	}
	
	public int getSize() {
		return buff.getInt(sizeMCNK);
	}
	public void setSize(int value) {
		buff.putInt(sizeMCNK, value);
	}
	
	
	public int getFlags() {
		return buff.getInt(flags);
	}
	
	public int getAsyncID() {
		return buff.getInt(asyncId);
	}
}