package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;

import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

public final class MTFX extends chunk {
	
	public int[] mode;
	int length=0;
	
	public MTFX(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer,"XFTM");
		// TODO Auto-generated constructor stub
		// get size
		int size = buff.getInt();
		buff.limit( size + data );
		
		// count 0 (string endings)
		while(buff.hasRemaining()) {
			byte t = buff.get();
			if(t==0) length++;
		}
		
		// init strings
		mode = new int[length];
		buff.position(data);
		for(int i=0; i<length; i++) {
			mode[i] =buff.getInt();
		}
		
		// push pointer
		pointer.position( pointer.position() + buff.limit() ); 
	}	
	
	public MTFX() {
		super(0x8);
		byte magic[] = { 'X', 'F', 'T', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);	
		
		if(DEBUG) System.out.println("Empty MTFX created.");
	}
	
	/**
	 * render data to write it back to a file
	 */
	public void render() {
		
		// allocate
		ByteBuffer tmp = doRebirth(Integer.SIZE*length + data);
		
		// write down
		for(int c=0; c<length; c++) {
			tmp.putInt( mode[c] );
		}
		buff = tmp;
		
		// reset pointers
		writeSize();
		buff.position(0);
	}
	
	/**
	 * get the texture value
	 * @param number index in the string array
	 * @return the texture filename
	 */
	public int getValueNo(int index) {
		//if not set WoW handles the Texture normale
		//so return 0!
		if(index>length) return 0;
		else return mode[index];
	}
	
	/**
	 * @return number of modes in this file
	 */
	public int getLenght() {
		return length;
	}
	
}