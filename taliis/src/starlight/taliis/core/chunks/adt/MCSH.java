package starlight.taliis.core.chunks.adt;

/**
 * Shadowmap
 * includes bitwise light/shadow value for each 64x64 subfields.
 * This makes 8 byte for each row so size is 64*8
 * 
 * @author conny
 *
 */

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MCSH extends chunk {
	int size;
	
	/*
	 * 0x0	magic
	 * 0x4	size
	 * 0x8	data
	 */
	
	MCSH(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "HSCM");

		size = buff.getInt();
		
		buff.limit(buff.position() + size);
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MCSH() {
		super(64*8 + data);
		byte magic[] = { 'H', 'S', 'C', 'M'};
			
		buff.put(magic);		// magic
		buff.putInt(64*8);		// size
		
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("  MCSH created.");
	}
	
	public int[] getData() {
		int ret[] = new int[64*64];
		
		int i = 0;
		for(int line=0; line<64; line++) {
			//System.out.print(line + ":\t");
			for(int c=0; c<8; c++) {
				byte tmp = buff.get(data + (line*8) + c);
								
				int mask = 1;
				for(int shift=0; shift<8; shift++) {
					ret[i++] = (tmp&mask)>>shift;
					mask*=2;
				}
			}
			//System.out.println();
		}
		
		return ret;
	}
	
	public void setData(int[] values) {
		
		int i = 0;
		for(int line=0; line<64; line++) {
			for(int c=0; c<8; c++) {
				int tmp =0;
				
				for(int shift=0; shift<8; shift++) {
					tmp = (tmp|(byte)(values[i++]<<shift));
				}
				byte t=(byte)tmp;
				buff.put(data + (line*8) + c,t);
								
			}
		}
		
		  
	}
}