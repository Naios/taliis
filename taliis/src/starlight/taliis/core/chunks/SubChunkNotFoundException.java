package starlight.taliis.core.chunks;

/**
 * Another version of the ChunkNotFoundException. Just implemented in case that
 * someone may need it.
 * 
 * @author ganku
 * @see ChunkNotFoundException
 */
public class SubChunkNotFoundException extends ChunkNotFoundException {

}