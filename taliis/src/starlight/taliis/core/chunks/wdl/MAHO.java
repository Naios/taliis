package starlight.taliis.core.chunks.wdl;

/**
 * After each MARE chunk there is a MAHO chunk but I have
 * never seen data in one o.O
 * size is always 0x20 (as far as I've seen)
 * 
 * @author tigurius
 */


import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

public final class MAHO extends chunk {	

	public MAHO(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "OHAM");
		
		int size = buff.getInt();
		
		buff.limit( size + data );
		pointer.position( pointer.position() + buff.limit() ); 
	}

	public MAHO() {
		super(0x20 + data);
		byte magic[] = { 'O', 'H', 'A', 'M'};
		
		buff.put(magic);		// magic
		buff.putInt(0x20);	// size
		
		buff.position(0);
		if(DEBUG) System.out.println("MAHO created.");
	}
}