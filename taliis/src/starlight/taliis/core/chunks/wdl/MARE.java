package starlight.taliis.core.chunks.wdl;

/**
 *Contains the low-res heightmap
 * 
 * @author tigurius
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MARE extends chunk {	
	public short outerheight[];
	public short innerheight[];
	int size;

public MARE(ByteBuffer pointer) throws ChunkNotFoundException {
	super(pointer, "ERAM");
	
	size = buff.getInt();
	buff.limit( size + data );
	outerheight=new short[17*17];
	for(int i=0;i<17*17;i++)
		outerheight[i]=buff.getShort();
	innerheight=new short[16*16];
	for(int i=0;i<16*16;i++)
		innerheight[i]=buff.getShort();
	
	
	pointer.position( pointer.position() + buff.limit() ); 
}

public MARE() {
	super(16*16*2+17*17*2 + data);
	byte magic[] = { 'E', 'R', 'A', 'M'};
	
	buff.put(magic);		// magic
	buff.putInt(16*16*2+17*17*2);	// size
	size=16*16*2+17*17*2;
	outerheight=new short[17*17];
	for(int i=0;i<17*17;i++)
		outerheight[i]=0;
	innerheight=new short[16*16];
	for(int i=0;i<16*16;i++)
		innerheight[i]=0;
	
	buff.position(0);
	if(DEBUG) System.out.println("MARE created.");
}

public void render(){
	ByteBuffer tmp = doRebirth(data + size);
	for(int j=0;j<17*17;j++)
		tmp.putShort(outerheight[j]);

	for(int i=0;i<16*16;i++)
		tmp.putShort(innerheight[i]);


	buff=tmp;
	writeSize();
	buff.position(0);
}

public short getData(int n){
	return buff.getShort(n);
}

public void setInnerHeight(int nr, short val){
	innerheight[nr]=val;
}
public void setOuterHeight(int nr, short val){
	outerheight[nr]=val;
}



}