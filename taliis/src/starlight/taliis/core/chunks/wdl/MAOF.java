package starlight.taliis.core.chunks.wdl;

/**
 * Map tile table. 
 * contains 64x64 = 4096 uint32s
 * which are the offsets to each maptiles low-res heightmap 
 * in a MARE chunk, for unused tiles this value is 0
 * 
 * @author tigurius
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MAOF extends chunk {	

public MAOF(ByteBuffer pointer) throws ChunkNotFoundException {
	super(pointer, "FOAM");
	
	int size = buff.getInt();
	
	buff.limit( size + data );
	pointer.position( pointer.position() + buff.limit() ); 
}

public MAOF() {
	super(64*64*4 + data);
	byte magic[] = { 'F', 'O', 'A', 'M'};
	
	buff.put(magic);		// magic
	buff.putInt(64*64*4);	// size
	
	buff.position(0);
	if(DEBUG) System.out.println("MAOF created.");
}



public int getOffset(int x, int y) {
	int index = (x*64) + y;
	return buff.getInt(index*4 + data);
}

public void setOffset(int x, int y, int val) {
	int index = (x*64) + y;
	buff.putInt(index*4 + data, val);
}


}