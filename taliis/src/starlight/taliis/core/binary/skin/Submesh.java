package starlight.taliis.core.binary.skin;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

public  class Submesh extends memory{
	public static int ID = 0x0;
	public static int StartVertex = 0x4;
	public static int nVertices =0x6;
	public static int StartTriangle=0x8;
	public static int nTriangles=0xA;
	public static int nBones=0xC;
	public static int StartBone=0xE;
	public static int Unknown=0x10;//this should be set to 1 and not to 0!!!
	public static int RootBone=0x12;//unsure
	public static int Position=0x14;//[3]*float ==vector
	public static int Floats=0x20;//[4] new since BC
	public static int end=0x30;
	
	public Submesh(ByteBuffer databuffer) {
		//TODO: push pointer by our own size
		super(databuffer);
		
		buff.limit( end );
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public Submesh() {
		// TODO Auto-generated constructor stub
		super(end);
		buff.putShort(nBones,(short) 1);
		buff.putShort(Unknown, (short) 1);
		buff.position(0);
		System.out.println("Created Submesh");
	}
	
	public void render(){
		buff.position(0);
	}
	
	public int getID(){
		return buff.getInt(ID);
	}
	public void setID(int val){
		buff.putInt(ID, val);
	}

	public short getStartVertex(){
		return buff.getShort(StartVertex);
	}
	
	public void setStartVertex(short val){
		buff.position(StartVertex);
		buff.putShort(val);
	}
	
	public short getnVertices(){
		return buff.getShort(nVertices);
	}
	
	public void setnVertices(short val){
		buff.position(nVertices);
		buff.putShort(val);
		System.out.println("Submesh nVertices:\t"+val);
	}
	
	public short getStartTriangle(){
		return buff.getShort(StartTriangle);
	}
	
	public void setStartTriangle(short val){
		buff.position(StartTriangle);
		buff.putShort(val);
	}
	
	public short getnTriangles(){
		return buff.getShort(nTriangles);
	}
	
	public void setnTriangles(short val){
		buff.position(nTriangles);
		buff.putShort(val);
	}
	
	public short getStartBone(){
		return buff.getShort(StartBone);
	}
	
	public void setStartBone(short val){
		buff.position(StartBone);
		buff.putShort(val);
	}
	
	public short getnBones(){
		return buff.getShort(nBones);
	}
	
	public void setnBones(short val){
		buff.position(nBones);
		buff.putShort(val);
	}
	
	public short getRootBone(){
		return buff.getShort(RootBone);
	}
	
	/**
	 * Returns the Position of the Submesh
	 * 
	 * @return float[3]
	 */
	public float[] getPosition(){
		float[] pos = new float[3];
		buff.position(Position);
		for(int i=0;i<3;i++)
			pos[i]=buff.getFloat();
		return pos;
	}
	
	/**
	 * 
	 * Sets the Position for a Submesh
	 *  
	 * @param float[3] pos
	 */
	public void setPosition(float[] pos){
		buff.position(Position);
		for(int i=0;i<3;i++)
			buff.putFloat(pos[i]);
	}
}