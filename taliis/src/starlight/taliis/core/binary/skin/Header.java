package starlight.taliis.core.binary.skin;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

public  class Header extends memory{
	public static int ID = 0x0;
	public static int nIndices =0x4;
	public static int ofsIndices=0x8;
	public static int nTriangles=0xC;		
	public static int ofsTriangles=0x10;
	public static int nProperties=0x14;
	public static int ofsProperties=0x18;
	public static int nSubmeshes=0x1C;
	public static int ofsSubmeshes=0x20;
	public static int nTextureUnits=0x24;
	public static int ofsTextureUnits=0x28;
	public static int LOD=0x2C;
	public static int end=0x30;
	
	public Header(ByteBuffer databuffer) {
		//TODO: push pointer by our own size
		super(databuffer);
		
		buff.limit( end );
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public Header() {
		// TODO Auto-generated constructor stub
		super(end);
		byte[] magic={'S','K','I','N'};
		buff.put(magic);
		buff.putInt(LOD, 0x15);
		buff.position(0);
	}
	
	public void render(){
		buff.position(0);
	}

	public int getnIndices(){
		return buff.getInt(nIndices);
	}
	
	public int getofsIndices(){
		return buff.getInt(ofsIndices);
	}
	public void setnIndices(int val){
		buff.putInt(nIndices,val);
	}
	
	public void setofsIndices(int val){
		buff.putInt(ofsIndices,val);
	}
	
	public int getnTriangles(){
		return buff.getInt(nTriangles);
	}
	
	public int getofsTriangles(){
		return buff.getInt(ofsTriangles);
	}
	public void setnTriangles(int val){
		buff.putInt(nTriangles, val);
	}
	public void setofsTriangles(int val){
		buff.putInt(ofsTriangles, val);
	}
	
	public int getnProperties(){
		return buff.getInt(nProperties);
	}
	
	public int getofsProperties(){
		return buff.getInt(ofsProperties);
	}
	public void setnProperties(int val){
		buff.putInt(nProperties, val);
	}
	
	public void setofsProperties(int val){
		buff.putInt(ofsProperties,val);
	}
	
	public int getnSubmeshes(){
		return buff.getInt(nSubmeshes);
	}
	
	public int getofsSubmeshes(){
		return buff.getInt(ofsSubmeshes);
	}
	public void setnSubmeshes(int val){
		buff.putInt(nSubmeshes,val);
	}
	
	public void setofsSubmeshes(int val){
		buff.putInt(ofsSubmeshes,val);
	}
	
	public int getnTextureUnits(){
		return buff.getInt(nTextureUnits);
	}
	
	public int getofsTextureUnits(){
		return buff.getInt(ofsTextureUnits);
	}
	
	public void setnTextureUnits(int val){
		buff.putInt(nTextureUnits,val);
	}
	
	public void setofsTextureUnits(int val){
		buff.putInt(ofsTextureUnits,val);
	}
	
	
	
}