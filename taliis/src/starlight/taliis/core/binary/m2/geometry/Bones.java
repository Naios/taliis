package starlight.taliis.core.binary.m2.geometry;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;
import starlight.taliis.core.binary.m2.AnimationBlock;

public class Bones extends memory{
	public static int AnimationSeq=0x0;
	public static int Flags=0x4;
	public static int ParentBone=0x8;
	public static int GeosetID=0xA;
	public static int unk=0xC;
	public static int Translation=0x10;
	public static int Rotation=0x024;
	public static int Scaling=0x38;
	public static int PivotPoint=0x4C;
	public static int end=0x58;
	
	public AnimationBlock Trans;
	public AnimationBlock Rot;
	public AnimationBlock Scale;
	
	public Bones(ByteBuffer databuffer){
		super(databuffer);
		
		buff.position(Translation);
		Trans=new AnimationBlock(buff);
		buff.position(Rotation);
		Rot=new AnimationBlock(buff);
		buff.position(Scaling);
		Scale=new AnimationBlock(buff);
		buff.limit( end );
		
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public Bones(){
		super(end);
		buff.putInt(AnimationSeq,-1);
		buff.putShort(ParentBone,(short) -1);
		Trans=new AnimationBlock();
		Rot=new AnimationBlock();
		Scale=new AnimationBlock();
		buff.position(0);
	}
	
	public void render(){
		Trans.render();
		buff.position(Translation);
		buff.put(Trans.buff);
		Rot.render();
		buff.position(Rotation);
		buff.put(Rot.buff);
		Scale.render();
		buff.position(Scaling);
		buff.put(Scale.buff);
		buff.position(0);
	}
	
	public short getParent(){
		return buff.getShort(ParentBone);
	}
	public void setParent(short val){
		buff.putShort(ParentBone, val);
	}
	public float[] getPivotPoint(){
		float retf[]=new float[3];
		buff.position(PivotPoint);
		for(int i=0;i<3;i++){
			retf[i]=buff.getFloat();
		}
		return retf;
	}
	public void setPivotPoint(float[] pos){
		buff.position(PivotPoint);
		for(int i=0;i<3;i++){
			buff.putFloat(pos[i]);
		}
	}
	
	
	
}