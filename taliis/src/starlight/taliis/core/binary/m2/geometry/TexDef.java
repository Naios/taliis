package starlight.taliis.core.binary.m2.geometry;

/**
 * First is a list of nTextures texture definition records,
 * 16 bytes per record, starting at ofsTextures.
 * After it comes a string block with the texture filenames. 
 * 
 * @author Tigurius
 * 
 */

import java.nio.*;

import starlight.taliis.core.memory;

public  class TexDef extends memory{
	public static int Type = 0x00; //0=hardcoded
	public static short Unknown = 0x04;
	public static short Flags = 0x06;
	public static int lenFilename = 0x8;//if Type = 0 here is the Length 
	public static int ofsFilename = 0xC;//and the offset to the filename
	public static int end = 0x10;
	
	
	public TexDef(ByteBuffer databuffer) {
		//TODO: push pointer by our own size
		super(databuffer);

		buff.limit( end );
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public TexDef() {
		super(end);
	}
	
	public void render(){
		buff.position(0);
	}

	public boolean ishardcoded(){
		if(buff.getInt(Type)==0)
			return true;
		else
			return false;
	}
	public void setType(int val){
		buff.putInt(Type,val);
	}
	public int getlenFilename(){
		return buff.getInt(lenFilename);
	}
	public void setlenFilename(int val){
		buff.putInt(lenFilename, val);
	}
	public int getofsFilename(){
		return buff.getInt(ofsFilename);
	}
	public void setofsFilename(int val){
		buff.putInt(ofsFilename, val);
	}
}