package starlight.taliis.core.binary.m2.geometry;

/**
 * This block defines a Vertex
 * 
 * @author tigurius
 */

import java.nio.*;

import starlight.taliis.core.memory;


public class Vertex extends memory{
	public static int Position=0x0;//vec3D
	public static int BoneWeight=0xC;//4*uint8
	public static int BoneIndices=0x10;//4*uint8
	public static int Normal=0x14;//vec3D
	public static int TextureCoords=0x20;//float[2]
	public static int Unknown=0x28;//float[2]
	public static int end=0x30;
	
	public Vertex(ByteBuffer databuffer){
		super(databuffer);

		buff.limit( end );
		
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public Vertex(){
		super(end);
		buff.position(0);
	}
	
	public void render(){
		buff.position(0);
	}
	
	/**
	 * Returns the Position of the given Vertex
	 * 
	 * @return position float[3]
	 */
	public float[] getVertexPos(){
		float temp[]= new float[3];
		temp[0]=buff.getFloat(Position);
		temp[1]=buff.getFloat(Position+4);
		temp[2]=buff.getFloat(Position+8);
		return temp;
	}
	
	
	/**
	 * Sets the Position of the given Vertex
	 * 
	 * @param val float[3]
	 */
	public void setVertexPos(float[] val){
		buff.putFloat(Position, val[0]);
		buff.putFloat(Position+4, val[1]);
		buff.putFloat(Position+8, val[2]);
	}
	
	
	/**
	 * Returns the Position of the given Vertex
	 * 
	 * @return position float[2]
	 */
	public float[] getTexCoord(){
		float temp[]= new float[2];
		temp[0]=buff.getFloat(TextureCoords);
		temp[1]=buff.getFloat(TextureCoords+4);
		return temp;
	}
	
	/**
	 * Sets the Position of the given Vertex
	 * 
	 * @param val float[2]
	 */
	public void setTexCoord(float[] val){
		buff.putFloat(TextureCoords, val[0]);
		buff.putFloat(TextureCoords+4, val[1]);
	}
	
	public int getBoneIndex(){
		return buff.get(BoneIndices);
	}
}