package starlight.taliis.core.binary.m2.geometry;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;



public class Renderflags extends memory{
	public static int flag=0x0;
	public static int blend=0x2;
	public static int end=0x4;
	
	public Renderflags(ByteBuffer databuffer){
		super(databuffer);
		buff.limit(end);
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public Renderflags(){
		super(end);
		buff.position(0);
	}
	public short getFlags(){
		return buff.getShort(flag);
	}
	public void setFlags(short val){
		buff.putShort(flag, val);
	}
	public short getBlendingMode(){
		return buff.getShort(blend);
	}
	public void setBlendingMode(short val){
		buff.putShort(blend, val);
	}
}