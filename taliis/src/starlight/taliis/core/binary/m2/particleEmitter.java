package starlight.taliis.core.binary.m2;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

/**
 * Structure of an m2 particle data area. 
 * 
 * @author tharo
 *
 */



public class particleEmitter extends memory {
	/*
		0x000 	int32 		id (always -1?)
		0x004 	int32 		flags
		0x008 	float[3] 	position
		0x014 	int16 		Bone ID*/
		public final static int textureID = 0x016; 	//		0x016 	int16 		Texture ID
		/*0x018 	uint32 		Number of ints referenced #1
		0x01C 	uint32 		Offset to list of ints #1
		0x020 	uint32 		Number of ints referenced #2
		0x024 	uint32 		Offset to list of ints #2
		0x028 	int16 		Blending mode
		0x02A 	int16 		Emitter type
		0x02C 	int16 		Particle type
		0x02E 	int16 		Texture tile rotation (-1,0,1)
		0x030 	int16 		Rows on texture
		0x032 	int16 		Columns on texture
		0x034 	AnimationBlock (float) [10] 	Parameters
		0x14C 	float[36] 	unknown float values - more parameters?
		0x14C 	float 		Midpoint in lifespan? (0 to 1)
		0x150 	uint32[3] 	ARGB colors (start, mid, end)
		0x15C 	float[3] 	Particle sizes (start, mid, end)
		0x168 	short[10] 	Indices into the tiles on the texture?
		0x17C 	float[3] 	Unknown
		0x188 	float[3] 	Something about particle scaling? Hm.
		0x194 	float 		Slowdown
		0x198 	float 		Particle rotation
		0x19C 	float[10] 	Unknown (usually all 0)
		0x1C4 	float[6] 	Unknown, usually (2.5, 0.7, 7, 0.9, 0, 0)
		0x1DC 	AnimationBlock (int) 	unknown
	 */
		public final static int end = 0x1F8;
	
	
	public particleEmitter(ByteBuffer databuffer) {
		//TODO: push pointer by our own size
		super(databuffer);
		
		buff.limit( end );
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public short getTextureID() {
		return buff.getShort(textureID);
	}
	public void setTextureID(short val) {
		buff.putShort(textureID, val);
	}
}
