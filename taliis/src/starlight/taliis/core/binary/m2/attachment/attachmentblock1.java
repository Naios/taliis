package starlight.taliis.core.binary.m2.attachment;


import java.nio.*;

import starlight.taliis.core.memory;
import starlight.taliis.core.binary.m2.AnimationBlock;

public class attachmentblock1 extends memory{
	public static int Id=0x0;
	public static int Bone=0x4;
	public static int Position=0x8;//float[3] Relative to that bone of course.
	public static int Data=0x14;//AnimBlock
	public static int end=0x28;
	
	public AnimationBlock Dat;
	
	public attachmentblock1(ByteBuffer databuffer){
		super(databuffer);

		buff.limit( end );
		buff.position(Data);
		Dat=new AnimationBlock(buff);
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public attachmentblock1() {
		super(end);
	}

	public void render(){
		buff.position(0);
	}
	
	public int getID(){
		return buff.getInt(Id);
	}
	public void setID(int val){
		buff.putInt(Id, val);
	}
	public int getBone(){
		return buff.getInt(Bone);
	}
	public void setBone(int val){
		System.out.println("Old bone:\t"+buff.getInt(Bone));
		buff.putInt(Bone, val);
		System.out.println("New bone:\t"+buff.getInt(Bone));
	}
	
	/**
	 * Returns the Position of the Attachment
	 * 
	 * @return position float[3]
	 */
	public float getX(){
		return buff.getFloat(Position);
	}
	public float getY(){
		return buff.getFloat(Position+4);
	}
	public float getZ(){
		return buff.getFloat(Position+8);
	}
	/**
	 * Sets the Position of the Attachment
	 * 
	 * @param val float[3]
	 */
	public void setX(float val){
		buff.putFloat(Position, val);
	}
	public void setY(float val){
		buff.putFloat(Position+4, val);
	}
	public void setZ(float val){
		buff.putFloat(Position+8, val);
	}
}