package starlight.taliis.core.binary.m2.camera;

/**
 * This block defines the Cameras :O
 * 
 * @author tigurius
 */

import java.nio.*;

import starlight.taliis.core.memory;
import starlight.taliis.core.binary.m2.AnimationBlock;

public class Camerablock extends memory{
	public static int BoneID = 0x0;
	public static int FOV = 0x4;//Multiply by 35 to get degrees.
	public static int FarClipping = 0x8;
	public static int NearClipping = 0xC;
	public static int TranslationPos = 0x10;//->AnimBlock
	public static int Position = 0x24;
	public static int TranslationTar= 0x30;//->AnimBlock
	public static int Target = 0x44;
	public static int Scaling = 0x50;//->AnimBlock
	public static int end = 0x64;
	
	public AnimationBlock TransPos;
	public AnimationBlock TransTar;
	public AnimationBlock Scale;
	
	public Camerablock(ByteBuffer databuffer){
		super(databuffer);

		buff.limit( end );
		buff.position(TranslationPos);
		TransPos=new AnimationBlock(buff);
		buff.position(TranslationTar);
		TransTar= new AnimationBlock(buff);
		buff.position(Scaling);
		Scale=new AnimationBlock(buff);
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public float getFOV(){
		return buff.getFloat(FOV)*35;
	}
	
	
	/**
	 * Returns the Position of the Camera
	 * 
	 * @return float[3] position
	 */
	public float[] getPos(){
		float[] pos=new float[3];
		buff.position(Position);
		for(int i=0;i<3;i++)
			pos[i]=buff.getFloat();
		return pos;
	}
	
	
	/**
	 * Returns the Position of the target of the camera
	 * 
	 * @return float[3] target
	 */
	public float[] getTarget(){
		float[] tar=new float[3];
		buff.position(Target);
		for(int i=0;i<3;i++)
			tar[i]=buff.getFloat();
		return tar;
	}
	
	public void setFOV(float val){
		buff.putFloat(FOV,val);
	}
	public void setPos1(float val){
		buff.putFloat(Position, val);
	}
	public void setPos2(float val){
		buff.putFloat(Position+4, val);
	}
	public void setPos3(float val){
		buff.putFloat(Position+8, val);
	}
	
	
	public void setTarget1(float val){
		buff.putFloat(Target, val);
	}
	public void setTarget2(float val){
		buff.putFloat(Target+4, val);
	}
	public void setTarget3(float val){
		buff.putFloat(Target+8, val);
	}
}