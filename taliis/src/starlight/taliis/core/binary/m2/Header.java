package starlight.taliis.core.binary.m2;

/**
 * Header for a m2 file
 * WotLk
 * 
 * @author Tigurius
 */

import java.nio.*;

import starlight.taliis.core.memory;

public  class Header extends memory{
	public final static int magic = 0x00;
	public final static int version = 0x004;
	public final static int	lengthName = 0x08;
	public final static int	ofsName = 0x0C;
	public final static int	globalModelFlags = 0x010; //(0,1,3 seen), tilting toons
	//0=no tilt, 1=x-tilt only, 2=y-tilt only, 3=x-y-tilt
	
	/*
	 * Skeleton and animation related stuff part1
	 */
	public final static int	nGlobalSequences = 0x014;
	public final static int ofsGlobalSequences = 0x18;
	public final static int	nAnimations = 0x1C;
	public final static int	ofsAnimations = 0x20;
	public final static int	nAnimationLookup = 0x24;
	public final static int ofsAnimationLookup = 0x28;
	public final static int nBones = 0x2C;
	public final static int ofsBones = 0x30;
	public final static int nKeyBoneLookup = 0x34;
	public final static int ofsKeyBoneLookup = 0x38;
	
	/*
	 * Geometry and rendering
	 */
	public final static int	nVertices = 0x3C;
	public final static int	ofsVertices =0x40;
	public final static int nViews = 0x44;
	public final static int	nColors = 0x48;
	public final static int	ofsColors= 0x4C;
	public final static int	nTextures = 0x50;
	public final static int	ofsTextures=0x54;
	public final static int	nTransparency=0x58;
	public final static int ofsTransparency=0x5C;
	public final static int nTextureanimations=0x60;
	public final static int ofsTextureanimations=0x64;
	public final static int nTexReplace=0x68;
	public final static int ofsTexReplace=0x6C; //Replaceable Textures.
	public final static int nRenderFlags=0x70;
	public final static int ofsRenderFlags=0x74;
	public final static int nBoneLookupTable=0x78;
	public final static int ofsBoneLookupTable=0x7C;
	public final static int nTexLookup=0x80;
	public final static int ofsTexLookup=0x84;
	public final static int nTexUnits=0x88;
	public final static int ofsTexUnits=0x8C;
	public final static int nTransLookup=0x90;
	public final static int ofsTransLookup=0x94;
	public final static int nTexAnimLookup=0x98;
	public final static int ofsTexAnimLookup=0x9C;
	
	//14 floats noone knows what they are for - yet!
	
	public final static int bb1x=0xA0;
	public final static int bb1y=0xA4;
	public final static int bb1z=0xA8;
	public final static int bb2x=0xAC;
	public final static int bb2y=0xB0;
	public final static int bb2z=0xB4;
	public final static int modifier1=0xB8;
	public final static int vp1x=0xBC;
	public final static int vp1y=0xC0;
	public final static int modifier2=0xC4;
	public final static int vp2x=0xC8;
	public final static int vp2y=0xCC;
	public final static int parameters1=0xD0;
	public final static int parameters2=0xD4;
	
	/*
	 * Bounding Stuff blahblah
	 */
	public final static int nBoundingTriangles=0xD8;
	public final static int ofsBoundingTriangles=0xDC;
	public final static int nBoundingVertices=0xE0;
	public final static int ofsBoundingVertices=0xE4;
	public final static int nBoundingNormals=0xE8;
	public final static int ofsBoundingNormals=0xEC;
	
	/*
	 * Attachment stuff
	 */
	public final static int nAttachments=0xF0;
	public final static int ofsAttachments=0xF4;
	public final static int nAttachLookup=0xF8;
	public final static int ofsAttachLookup=0xFC;
	public final static int nAttachments_2=0x100;
	public final static int ofsAttachments_2=0x104;
	
	/*
	 * Lights!
	 */
	public final static int nLights=0x108;
	public final static int ofsLights=0x10C;
	
	/*
	 * Cameras blob
	 */
	public final static int nCameras=0x110;
	public final static int ofsCameras=0x114;
	public final static int nCameraLookup=0x118;
	public final static int ofsCameraLookup=0x11C;
	
	/*
	 * Ribbons...
	 */
	public final static int nRibbonEmitters=0x120;
	public final static int ofsRibbonEmitters=0x124;
	
	/*
	 * AND finally...the particles!
	 */
	public final static int nParticleEmitters=0x128;
	public final static int ofsParticleEmitters=0x12C;
	
	/*
	 * So the header is all in all 0x130-Bytes long
	 */
	
	public final static int end = 0x130;
	
	public Header(ByteBuffer databuffer) {
		//TODO: push pointer by our own size
		super(databuffer);
		
		buff.limit( end );
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public Header(){
		super(end);
		byte magic[]={ 'M', 'D', '2', '0'};
		buff.put(magic);
		byte version[]={8,1,0,0};
		buff.put(version);
		// reset position
		buff.position(0);	
	}
	
	public void render(){
		buff.position(0);
	}
	
	public String getMagic() {
		 byte mag[] = new byte[4];
		 buff.position(magic);
		 buff.get(mag, 0, 4);
		 
		 return new String(mag);
	}
	
	public byte[] getVersion() {
		byte ver[] = new byte[4];
		buff.position(version);
		buff.get(ver, 0, 4);
		return ver;
	}
	public int getofsName(){
		return buff.getInt(ofsName);
	}
	public void setofsName(int val){
		buff.putInt(ofsName,val);
	}
	public int getlName(){
		return buff.getInt(lengthName);
	}
	public void setlName(int val){
		buff.putInt(lengthName, val);
	}
	
	public int getTilt(){
		return buff.getInt(globalModelFlags);
	}
	public void setTilt(int val){
		buff.putInt(globalModelFlags,val);
	}
	
	public int getnGlobalSequ(){
		return buff.getInt(nGlobalSequences);
	}
	public void setnGlobalSequ(int val){
		buff.putInt(nGlobalSequences, val);
	}
	public int getofsGlobalSequ(){
		return buff.getInt(ofsGlobalSequences);
	}
	public void setofsGlobalSequ(int val){
		buff.putInt(ofsGlobalSequences, val);
	}
	
	public int getAnimoffs(){
		return buff.getInt(ofsAnimations);
	}
	public void setAnimOffs(int val){
		buff.putInt(ofsAnimations,val);
	}
	public int getNAnim(){
		return buff.getInt(nAnimations);
	}
	public void setNAnim(int val){
		buff.putInt(nAnimations,val);
	}

	public void setBoneOffs(int val){
		buff.putInt(ofsBones,val);
	}

	public void setNBone(int val){
		buff.putInt(nBones,val);
	}
	
	public int getnRenderFlags(){
		return buff.getInt(nRenderFlags);
	}
	public void setnRenderFlags(int val){
		buff.putInt(nRenderFlags, val);
	}
	public int getofsRenderFlags(){
		return buff.getInt(ofsRenderFlags);
	}
	public void setofsRenderFlags(int val){
		buff.putInt(ofsRenderFlags,val);
	}
	
	public int getnTextures() {
		return buff.getInt(nTextures);
	}
	public void setnTextures(int val){
		buff.putInt(nTextures,val);
	}
	
	public int getofsTextures(){
		return buff.getInt(ofsTextures);
	}
	public void setofsTextures(int val){
		buff.putInt(ofsTextures,val);
	}
	
	public int getnTexLookup(){
		return buff.getInt(nTexLookup);
	}
	public void setnTexLookup(int val){
		buff.putInt(nTexLookup, val);
	}
	public int getofsTexLookup(){
		return buff.getInt(ofsTexLookup);
	}
	public void setofsTexLookup(int val){
		buff.putInt(ofsTexLookup, val);
	}
	
	public int getofsTexUnits(){
		return buff.getInt(ofsTexUnits);
	}
	public void setofsTexUnits(int val){
		buff.putInt(ofsTexUnits, val);
	}
	public int getnTexUnits(){
		return buff.getInt(nTexUnits);
	}
	public void setnTexUnits(int val){
		buff.putInt(nTexUnits, val);
	}
	
	public int getnTexReplace(){
		return buff.getInt(nTexReplace);
	}
	public void setnTexReplace(int val){
		buff.putInt(nTexReplace, val);
	}
	public int getofsTexReplace(){
		return buff.getInt(ofsTexReplace);
	}
	public void setofsTexReplace(int val){
		buff.putInt(ofsTexReplace, val);
	}
	
	public int getnTexAnimLookup(){
		return buff.getInt(nTexAnimLookup);
	}
	public void setnTexAnimLookup(int val){
		buff.putInt(nTexAnimLookup, val);
	}
	public int getofsTexAnimLookup(){
		return buff.getInt(ofsTexAnimLookup);
	}
	public void setofsTexAnimLookup(int val){
		buff.putInt(ofsTexAnimLookup, val);
	}
	
	public int getnTransparency(){
		return buff.getInt(nTransparency);
	}
	public void setnTransparency(int val){
		buff.putInt(nTransparency,val);
	}
	
	public int getofsTransparency(){
		return buff.getInt(ofsTransparency);
	}
	public void setofsTransparency(int val){
		buff.putInt(ofsTransparency,val);
	}
	
	public int getnTransLookup(){
		return buff.getInt(nTransLookup);
	}
	public void setnTransLookup(int val){
		buff.putInt(nTransLookup,val);
	}
	public int getofsTransLookup(){
		return buff.getInt(ofsTransLookup);
	}
	public void setofsTransLookup(int val){
		buff.putInt(ofsTransLookup,val);
	}
	
	public int getnBones(){
		return buff.getInt(nBones);
	}
	
	public int getofsBones(){
		return buff.getInt(ofsBones);
	}
	
	public int getnKeyBoneLookup(){
		return buff.getInt(nKeyBoneLookup);
	}
	public void setnKeyBoneLookup(int val){
		buff.putInt(nKeyBoneLookup, val);
	}
	public int getofsKeyBoneLookup(){
		return buff.getInt(ofsKeyBoneLookup);
	}
	public void setofsKeyBoneLookup(int val){
		buff.putInt(ofsKeyBoneLookup, val);
	}
	
	public int getnBonelookup(){
		return buff.getInt(nBoneLookupTable);
	}
	public void setnBonelookup(int val){
		buff.putInt(nBoneLookupTable,val);
	}
	
	public int getofsBonelookup(){
		return buff.getInt(ofsBoneLookupTable);
	}
	public void setofsBonelookup(int val){
		buff.putInt(ofsBoneLookupTable,val);
	}
	
	public int getnVertices(){
		return buff.getInt(nVertices);
	}
	public void setnVertices(int val){
		buff.putInt(nVertices, val);
	}
	public int getofsVertices(){
		return buff.getInt(ofsVertices);
	}
	public void setofsVertices(int val){
		buff.putInt(ofsVertices, val);
	}
	
	public int getnViews(){
		return buff.getInt(nViews);
	}
	public void setnViews(int val){
		buff.putInt(nViews,val);
	}
	/**
	 * Returns the Number of Cameras
	 * 
	 * @return int nCameras
	 */
	public int getnCameras(){
		return buff.getInt(nCameras);
	}
	/**
	 * Returns the offset to the Cams
	 * 
	 * @return ofsCameras
	 */
	public int getofsCameras(){
		return buff.getInt(ofsCameras);
	}
	
	public int getnBoundingTriangles(){
		return buff.getInt(nBoundingTriangles);
	}
	public void setnBoundingTriangles(int val){
		buff.putInt(nBoundingTriangles, val);
	}
	public int getofsBoundingTriangles(){
		return buff.getInt(ofsBoundingTriangles);
	}
	public void setofsBoundingTriangles(int val){
		buff.putInt(ofsBoundingTriangles, val);
	}
	
	public int getnBoundingVertices(){
		return buff.getInt(nBoundingVertices);
	}
	public void setnBoundingVertices(int val){
		buff.putInt(nBoundingVertices, val);
	}
	public int getofsBoundingVertices(){
		return buff.getInt(ofsBoundingVertices);
	}
	public void setofsBoundingVertices(int val){
		buff.putInt(ofsBoundingVertices, val);
	}
	
	public int getnBoundingNormals(){
		return buff.getInt(nBoundingNormals);
	}
	public void setnBoundingNormals(int val){
		buff.putInt(nBoundingNormals, val);
	}
	public int getofsBoundingNormals(){
		return buff.getInt(ofsBoundingNormals);
	}
	public void setofsBoundingNormals(int val){
		buff.putInt(ofsBoundingNormals, val);
	}
	
	public int getnAttachment(){
		return buff.getInt(nAttachments);
	}
	public int getofsAttachment(){
		return buff.getInt(ofsAttachments);
	}
	public void setnAttachment(int val){
		buff.putInt(nAttachments,val);
	}
	public void setofsAttachment(int val){
		buff.putInt(ofsAttachments,val);
	}
	
	public int getnAttachmentlookup(){
		return buff.getInt(nAttachLookup);
	}
	public int getofsAttachmentlookup(){
		return buff.getInt(ofsAttachLookup);
	}
	public void setnAttachmentlookup(int val){
		buff.putInt(nAttachLookup,val);
	}
	public void setofsAttachmentlookup(int val){
		buff.putInt(ofsAttachLookup,val);
	}

	public int getnRibbons(){
		return buff.getInt(nRibbonEmitters);
	}
	public void setnRibbons(int val){
		buff.position(nRibbonEmitters);
		buff.putInt(val);
	}
	public int getofsRibbons(){
		return buff.getInt(ofsRibbonEmitters);
	}
	public void setofsRibbons(int val){
		buff.position(ofsRibbonEmitters);
		buff.putInt(val);
	}
	/**
	 * Returns the Number of Particles
	 * 
	 * @return int nParticles
	 */
	public int getnParticles(){
		return buff.getInt(nParticleEmitters);
	}
	/**
	 * Sets the Number of Particles
	 * 
	 * @param int val
	 */
	public void setnParticles(int val){
		buff.position(nParticleEmitters);
		buff.putInt(val);
	}
	/**
	 * Returns the offset to the Particles
	 * 
	 * @return ofsParticleEmitters
	 */
	public int getofsParticles(){
		return buff.getInt(ofsParticleEmitters);
	}
	/**
	 * Sets the offset to the Particles
	 * 
	 * @param int val
	 */
	public void setofsParticles(int val){
		buff.position(ofsParticleEmitters);
		buff.putInt(val);
	}
	
	
	public float[][] getBoundingBox(){
		float[][] ret=new float[2][3];
		buff.position(bb1x);
		for(int i=0;i<2;i++){
			for(int k=0;k<3;k++){
				ret[i][k]=buff.getFloat();
			}
		}
		return ret;
	}
	
	public void setBoundingBox(float[] bb1, float[] bb2){
		float[][] tmp={bb1,bb2};
		buff.position(bb1x);
		for(int i=0;i<2;i++){
			for(int k=0;k<3;k++){
				buff.putFloat(tmp[i][k]);
			}
		}
	}
	
	public void setCamInfo(float[] caminfo1,float[] caminfo2){
		float[][] tmp={caminfo1,caminfo2};
		buff.position(modifier1);
		for(int i=0;i<2;i++){
			for(int k=0;k<3;k++){
				buff.putFloat(tmp[i][k]);
			}
		}
	}
}