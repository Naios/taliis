package starlight.taliis.core.binary.m2.effects;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;
import starlight.taliis.core.binary.m2.AnimationBlock;
import starlight.taliis.core.binary.m2.FakeAnimationBlock;

public class particleemitters extends memory{
	//particles also have 10000 of information and that shit
	//it sucks
	public static int negative=0x0;
	public static int Flags=0x4;
	public static int Position=0x8;
	public static int Bone=0x014;
	public static int Texture=0x16;
	public static int nReferences1=0x18;
	public static int ofsReferences1=0x1C;
	public static int nReferences2=0x20;
	public static int ofsReferences2=0x24;
	public static int BlendingType=0x28;
	public static int EmitterType=0x29;
	public static int ParticleType=0x2B;
	public static int Padding=0x2C;//wth why do I add this *roll-eyes*
	public static int TextureTileRotation=0x2D;
	public static int TextureRows=0x30;
	public static int TextureCols=0x32;
	//some shitty animblocks follow
	public static int EmissionSpeed=0x34;
	public static int SpeedVariation=0x48;
	public static int VerticalRange=0x5C;
	public static int HorizontalRange=0x70;
	public static int Gravity=0x84;
	public static int Lifespan=0x98;
	//padding an animblock padding o.O
	public static int unknownPadding=0xAC;
	public static int EmissionRate=0xB0;
	public static int unknownPadding2=0xC4;
	//hm animblocks and fakeanimblocks blocks blocks blocks <.<
	public static int EmissionAreaLength=0xC8;
	public static int EmissionAreaWidth=0xDC;
	public static int Gravity2=0xF0;
	//fakeanimblocks don't point to a substructur yay consistancy
	public static int ParticleColor=0x104;
	public static int ParticleOpacity=0x114;
	public static int ParticleSizes=0x124;
	//now follows a part with muuuch unknown shit
	public static int UnknownFields=0x134;
	public static int UnknownFloats1=0x15C;
	public static int Scale=0x168;
	public static int Slowdown=0x174;
	public static int UnknownFloats2=0x178;//rotation?
	public static int Rotation=0x180;//	 As a single value? Most likely only one axis then..
	public static int UnknownFloats3=0x184;//or are these the missing rot axis
	public static int Rot1=0x18C;
	public static int Rot2=0x198;
	public static int Trans=0x1A4;
	public static int UnknownFloats4=0x1B0;
	public static int EnabledIn=0x1C8;//at the end an animblock
	public static int end=0x1DC;
	
	public AnimationBlock emissionspeed;
	public AnimationBlock speedvariation;
	public AnimationBlock verticalrange;
	public AnimationBlock horizontalrange;
	public AnimationBlock gravity;
	public AnimationBlock lifespan;
	
	public AnimationBlock emissionrate;
	
	public AnimationBlock emissionarealength;
	public AnimationBlock emissionareawidth;
	public AnimationBlock gravity2;
	
	public FakeAnimationBlock particlecolor;
	public FakeAnimationBlock particleopacity;
	public FakeAnimationBlock particlesizes;
	
	
	
	public particleemitters(ByteBuffer databuffer){
		super(databuffer);
		buff.limit( end );
		
		buff.position(ParticleColor);
		particlecolor=new FakeAnimationBlock(buff);
		buff.position(ParticleOpacity);
		particleopacity=new FakeAnimationBlock(buff);
		buff.position(ParticleSizes);
		particlesizes=new FakeAnimationBlock(buff);
		buff.position(0);
		databuffer.position( databuffer.position() + buff.limit());
	}
}
