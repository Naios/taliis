package starlight.taliis.core.binary.m2.effects;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;
import starlight.taliis.core.binary.m2.AnimationBlock;


public class ribbons extends memory{
	public static int negative=0x0;
	public static int BoneID=0x4;
	public static int Position=0x8;
	public static int nTextures=0x14;
	public static int ofsTextures=0x18;
	public static int nUnkRef=0x1C;
	public static int ofsUnkRef=0x020;
	public static int AColor=0x24;
	public static int AOpacity=0x38;
	public static int AAbove=0x4C;
	public static int ABelow=0x60;
	public static int Resolution=0x74;
	public static int Length=0x78;
	public static int Gravity=0x7C;
	public static int unkShorts=0x80;
	public static int Aunk1=0x84;
	public static int Aunk2=0x98;
	public static int Padding=0xAC;
	public static int end=0xB0;
	
	public AnimationBlock Color;
	public AnimationBlock Opacity;
	public AnimationBlock Above;
	public AnimationBlock Below;
	
	public AnimationBlock Unk1;
	public AnimationBlock Unk2;
	
	public ribbons(ByteBuffer databuffer){
		super(databuffer);
		buff.limit( end );
		
		buff.position(AColor);
		Color=new AnimationBlock(buff);
		buff.position(AOpacity);
		Opacity=new AnimationBlock(buff);
		buff.position(AAbove);
		Above=new AnimationBlock(buff);
		buff.position(ABelow);
		Below=new AnimationBlock(buff);
		
		buff.position(Aunk1);
		Unk1=new AnimationBlock(buff);
		buff.position(Aunk2);
		Unk2=new AnimationBlock(buff);
		buff.position(0);
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public ribbons(){
		super(end);
		Color=new AnimationBlock();
		Opacity=new AnimationBlock();
		Above=new AnimationBlock();
		Below=new AnimationBlock();
		Unk1=new AnimationBlock();
		Unk2=new AnimationBlock();
	}
	
	public int getnTexture(){
		return buff.getInt(nTextures);
	}
	public int getofsTextures(){
		return buff.getInt(ofsTextures);
	}
	public void setnTexture(int val){
		buff.putInt(nTextures,val);
	}
	public void setofsTextures(int val){
		buff.putInt(ofsTextures,val);
	}
	public int getnInt(){
		return buff.getInt(nUnkRef);
	}
	public int getofsInt(){
		return buff.getInt(ofsUnkRef);
	}
	public void setnInt(int val){
		buff.putInt(nUnkRef,val);
	}
	public void setofsInt(int val){
		buff.putInt(ofsUnkRef,val);
	}
}