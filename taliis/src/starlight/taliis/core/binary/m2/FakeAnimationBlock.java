package starlight.taliis.core.binary.m2;

/**
 * A simple FakeAnimation-Block :O
 * 
 * doesn't point to substructure
 * but again to a up to 16byte block
 * 
 * 
 * @author Tigurius
 */

import java.nio.*;

import starlight.taliis.core.memory;

public  class FakeAnimationBlock extends memory{
	public static int nTimes=0x0;
	public static int ofsTimes=0x4;
	public static int nKeys=0x8;
	public static int ofsKeys=0xC;
	public static int end=0x10;
	
	public FakeAnimationBlock(ByteBuffer databuffer){
		super(databuffer);
		
		buff.limit( end );
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public int getnTimes(){
		return buff.getInt(nTimes);
	}
	public int getofsTimes(){
		return buff.getInt(ofsTimes);
	}
	public int getnKeys(){
		return buff.getInt(nKeys);
	}
	public int getofsKeys(){
		return buff.getInt(ofsKeys);
	}
}

