package starlight.taliis.core.files;

import java.io.InvalidClassException;
import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

/**
 * A new created superclass for all wow file types.
 * 
 * This new layer is created as counterpart for the
 * new plugin editor in order to allow changing
 * file engines.
 * 
 * It does basicaly noting else then hooking constructors
 * and implements render() that need to be run before a
 * file can stored back to binary memory 
 * 
 * @author ganku
 *
 */

public class wowfile extends memory {
	public wowfile() {
		super();
	}
	public wowfile(ByteBuffer dataBuffer) throws InvalidClassException {
		super(dataBuffer);
	}
	public void render() {};
}
