package starlight.taliis.core.files;

import java.io.InvalidClassException;
import java.nio.ByteBuffer;

import starlight.taliis.core.binary.skin.Header;
import starlight.taliis.core.binary.skin.Submesh;
import starlight.taliis.core.binary.skin.TexUnits;

/**
 * A Skinfile contains information about views for m2files
 * 
 * indices==triangle indices
 * properties==bone vertex attached to
 * properties=4 Bytes aka Bone Indices
 * 
 * @author Tigurius
 */

public class skin extends wowfile {	
	public Header header=null;
	public short vertices[]=null;
	public short triangles[][]=null;
	public short indices[]=null;
	public int properties[]=null;
	public Submesh submeshes[]=null;
	public TexUnits texunits[]=null;
	
	boolean iscreated=false;
	
	
	/**
	 * constructor for opening files
	 * @param databuffer ByteBuffer of the data stream
	 */
	public skin(ByteBuffer databuffer) throws InvalidClassException {		
		super(databuffer);
		
		load(databuffer);
		
		System.out.println("Stopped at: 0x" + 
				Long.toHexString(databuffer.position()));
	}
	
	public skin(){
		iscreated=true;
		header=new Header();
		submeshes=new Submesh[1];
		submeshes[0]=new Submesh();
		texunits=new TexUnits[1];
		texunits[0]=new TexUnits();
	}
	
	public skin(int nSubmesh, int nTexUnit){
		iscreated=true;
		header=new Header();
		submeshes=new Submesh[nSubmesh];
		for(int k=0;k<nSubmesh;k++)
		submeshes[k]=new Submesh();
		texunits=new TexUnits[nTexUnit];
		for(int k=0;k<nTexUnit;k++)
		texunits[k]=new TexUnits();
	}
	
	private void load(ByteBuffer databuffer) throws InvalidClassException {
				
		header = new Header(buff);
		
		vertices=new short[header.getnIndices()];
		buff.position(header.getofsIndices());
		for(int i=0;i<header.getnIndices();i++)
			vertices[i]=buff.getShort();
		
		triangles=new short[header.getnTriangles()/3][3];
		buff.position(header.getofsTriangles());
		for(int i=0;i<(header.getnTriangles()/3);i++){
			triangles[i][0]=buff.getShort();
			triangles[i][1]=buff.getShort();
			triangles[i][2]=buff.getShort();
		}
		
		indices=new short[header.getnTriangles()];
        buff.position(header.getofsIndices());
		for(int i=0;i<(header.getnIndices());i++){
			indices[i]=buff.getShort();
		}
		
			
		
		properties=new int[header.getnProperties()];
		buff.position(header.getofsProperties());
		for(int i=0;i<header.getnProperties();i++)
			properties[i]=buff.getInt();
		
		submeshes=new Submesh[header.getnSubmeshes()];
		buff.position(header.getofsSubmeshes());
		for(int i=0;i<header.getnSubmeshes();i++)
			submeshes[i]=new Submesh(buff);
		
		texunits=new TexUnits[header.getnTextureUnits()];
		buff.position(header.getofsTextureUnits());
		for(int i=0;i<header.getnTextureUnits();i++)
			texunits[i]=new TexUnits(buff);
		
	}
	
	int calcSize(){
		int t=0;
		if(header!=null)
		t+=Header.end;
		if(vertices!=null)//==indices @ wowdev!
		t+=vertices.length*2+0x10;
		if(indices!=null)//==triangles!
		t+=indices.length*2+0x10;
		if(properties!=null)
		t+=properties.length*4+0x10;
		if(submeshes!=null)
		t+=submeshes.length*Submesh.end+0x10;
		if(texunits!=null)
		t+=texunits.length*TexUnits.end+0x10;
		return t;
	}
	ByteBuffer fillLine(ByteBuffer tmp){
		int Pos, linePos;
		Pos=tmp.position();
		linePos=Pos&0xFFFFFFF0;
		for(int i=0;i<16-(Pos-linePos);i++){
			tmp.put((byte) 0);
		}
		return tmp;
	}
	
	public void render(){
		{
			/*properties=new int[vertices.length];
			for(int i=0;i<properties.length;i++){
				properties[i]=0x01020304;
			}*/
			
			ByteBuffer tmp = doRebirth(calcSize());
			int ro=0;
			if(header!=null){
			header.render();
			tmp.put(header.buff);
			ro+=Header.end;
			}
			if(vertices!=null){
			header.setnIndices(vertices.length);
			header.setofsIndices(ro);
			for(int i=0;i<vertices.length;i++){
				tmp.putShort(vertices[i]);
			}
			fillLine(tmp);
			ro=tmp.position();
			}
			if(indices!=null){
			header.setnTriangles(indices.length);
			header.setofsTriangles(ro);
			for(int i=0;i<indices.length;i++){
				tmp.putShort(indices[i]);
			}
			fillLine(tmp);
			ro=tmp.position();
			}
			if(properties!=null){
			header.setnProperties(properties.length);
			header.setofsProperties(ro);
			for(int i=0;i<properties.length;i++){
				tmp.putInt(properties[i]);
			}
			fillLine(tmp);
			ro=tmp.position();
			}
			if(submeshes!=null){
			header.setnSubmeshes(submeshes.length);
			header.setofsSubmeshes(ro);
			for(int i=0;i<submeshes.length;i++){
				submeshes[i].render();
				tmp.put(submeshes[i].buff);
			}
			fillLine(tmp);
			ro=tmp.position();
			}
			if(texunits!=null){
			header.setnTextureUnits(texunits.length);
			header.setofsTextureUnits(ro);
			for(int i=0;i<texunits.length;i++){
				texunits[i].render();
				tmp.put(texunits[i].buff);
			}
			fillLine(tmp);
			}			
			header.render();
			tmp.position(0);
			tmp.put(header.buff);
			buff=tmp;
		}
		buff.position(0);
	}
	
	/**
	 * Sets the Vertex Number nr to the one in the m2-list
	 * with Number val
	 * 
	 * @param nr int
	 * @param val short
	 */
	public void setVertex(int nr,short val){
		vertices[nr]=val;
	}
	public void setnVertex(int val){
		vertices = new short[val];
	}
	public void setnProperties(int val){
		properties=new int[val];
		for(int i=0;i<val;i++){
			properties[i]=0;
		}
	}
	public void setnTriangles(int val){
		triangles=new short[val][3];
		indices=new short[3*val];
	}
	
	/**
	 * Sets the vertices of the triangle nr
	 * 
	 * @param nr int 
	 * @param val short[3]
	 */
	public void setTriangle(int nr, short[] val){
		for(int i=0;i<3;i++){
			triangles[nr][i]=val[i];
			indices[3*nr+i]=val[i];
		}
	}
	
	public void setnSubmeshes(int val){
		submeshes=new Submesh[val];
		for(int i=0;i<val;i++){
			submeshes[i]=new Submesh();
		}
	}
	
	public void setnTexUnits(int val){
		texunits=new TexUnits[val];
		for(int i=0;i<val;i++){
			texunits[i]=new TexUnits();
		}
	}
	
}
