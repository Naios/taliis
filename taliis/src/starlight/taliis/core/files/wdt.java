package starlight.taliis.core.files;

import java.io.InvalidClassException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.MVER;
import starlight.taliis.core.chunks.chunk;
import starlight.taliis.core.chunks.wdt.MAIN;
import starlight.taliis.core.chunks.wdt.MODF;
import starlight.taliis.core.chunks.wdt.MPHD;
import starlight.taliis.core.chunks.wdt.MWMO;

/**
 * WDT files specify exactly which map tiles are present in a world, if any, and
 * can also reference a "global" WMO. They have a chunked file structure.
 * 
 * @see starlight.taliis.core.files.adt
 * @see starlight.taliis.core.files.wmo
 */
public class wdt extends wowfile {
	/** This chunk is holding the version of the WDT. */
	MVER mver;
	/** The header. */
	MPHD mphd;
	/** Defining which ADTs are used. */
	public MAIN main;
	/** Used if there is no terrain. Filenames here, entries below. */
	public MWMO mwmo;
	/** Entries for global WMOs. */
	public MODF modf;

	/**
	 * Read a complete wdt file from the given data buffer
	 * 
	 * @param databuffer A file or other source for the WDT.
	 * @exception ChunkNotFoundException If the main chunk is not found.
	 * @exception InvalidClassException If the WDT file is corrupt and missing
	 *                chunks.
	 */
	public wdt(ByteBuffer databuffer) throws InvalidClassException, ChunkNotFoundException {
		super(databuffer);

		try {
			mver = new MVER(buff);
			mphd = new MPHD(buff);
			main = new MAIN(buff);
			mwmo = new MWMO(buff);

			if(databuffer.hasRemaining()
					   && chunk.nextChunk(buff).compareTo("FDOM")==0){
				modf = new MODF(buff);
			}
			else
				modf=new MODF();
		} catch (ChunkNotFoundException e) {
			throw new InvalidClassException(e.getMessage());
		}
	}

	/**
	 * Creates a complete new and empty world.
	 */
	public wdt() {
		mver = new MVER();
		mphd = new MPHD();
		main = new MAIN();
		mwmo = new MWMO();
		mphd.setTerrain(0);
		modf = new MODF();

	}

	/**
	 * Calculates different values to prepare the file to be written.
	 */
	@Override
	public void render() {
		int ro = 0;

		mver.render();
		ro += mver.getSize();
		mphd.render();
		ro += mphd.getSize();
		main.render();
		ro += main.getSize();
		mwmo.render();
		ro += mwmo.getSize();
		modf.render();
		ro += modf.getSize();

		buff = ByteBuffer.allocate(ro);
		buff.order(ByteOrder.LITTLE_ENDIAN);
		buff.position(0);

		buff.put(mver.buff);
		buff.put(mphd.buff);
		buff.put(main.buff);
		buff.put(mwmo.buff);
		buff.put(modf.buff);

		buff.position(0);
	}
}
