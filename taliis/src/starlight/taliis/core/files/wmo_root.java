package starlight.taliis.core.files;

import java.io.InvalidClassException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.MVER;
import starlight.taliis.core.chunks.chunk;
import starlight.taliis.core.chunks.unkChunk;
import starlight.taliis.core.chunks.wmo.root.MODD;
import starlight.taliis.core.chunks.wmo.root.MODN;
import starlight.taliis.core.chunks.wmo.root.MODS;
import starlight.taliis.core.chunks.wmo.root.MOGI;
import starlight.taliis.core.chunks.wmo.root.MOGN;
import starlight.taliis.core.chunks.wmo.root.MOHD;
import starlight.taliis.core.chunks.wmo.root.MOMT;
//import starlight.taliis.core.chunks.wmo.root.MOPV;
import starlight.taliis.core.chunks.wmo.root.MOTX;

/**
 * wmo root files
 * 
 * @author ganku
 *
 */

public class wmo_root extends wmo {
	// chunks 
	public MVER mver = null;
	// root file
	public MOHD mohd;
	public MOTX motx;
	public MOMT momt;
	public MOGN mogn;
	public MOGI mogi;
	unkChunk mosb;
	unkChunk mopv;
	unkChunk mopt;
	unkChunk mopr;
	unkChunk movv;
	unkChunk movb;
	unkChunk molt;
	public MODS mods;
	public MODN modn;
	public MODD modd;
	 unkChunk mfog;
	 unkChunk mcvp;

// -------------------------------------------------------	

	/**
	 * Load and Init the WMO structure
	 * @throws ChunkNotFoundException 
	 */
	public wmo_root(ByteBuffer databuffer) throws InvalidClassException, ChunkNotFoundException {
		super(databuffer);
		parse(databuffer);
	}
	
	/**
	 * Creates a new WMO object
	 * -> not impementated!
	 */
	public wmo_root() {
		try {
			create();
		} catch (ChunkNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
// -------------------------------------------------------	
	
	/**
	 * Loads the data and init the chunk structures
	 * @throws ChunkNotFoundException 
	 */
	private void parse(ByteBuffer databuffer) throws InvalidClassException {
		// get version
		try {
		mver = new MVER(databuffer);
		
		if(mver.getVersion()==WMO_VERSION_NORMAL||mver.getVersion()==WMO_VERSION_TALIIS) {
			// root or group file?
			if(chunk.nextChunk(databuffer).compareTo("DHOM")==0) {
				// header
				mohd = new MOHD(databuffer);
				// textures
				motx = new MOTX(databuffer);
				// groups
				momt = new MOMT(databuffer);
				mogn = new MOGN(databuffer);
				mogi = new MOGI(databuffer);
				// skybox
				mosb = new unkChunk(databuffer, "BSOM");
				// portals
				mopv = new unkChunk(databuffer, "VPOM");
				mopt = new unkChunk(databuffer, "TPOM");
				mopr = new unkChunk(databuffer, "RPOM");
				// visibles
				movv = new unkChunk(databuffer, "VVOM");
				movb = new unkChunk(databuffer, "BVOM");
				// lightning
				molt = new unkChunk(databuffer, "TLOM");
				// doodad sets
				mods = new MODS(databuffer);
				// doodads
				modn = new MODN(databuffer);
				modd = new MODD(databuffer);
				// fog
				mfog = new unkChunk(databuffer, "GOFM");
				// volume panels (optional)
				if(databuffer.hasRemaining()
				   && chunk.nextChunk(databuffer).compareTo("PVCM")==0)
					mcvp = new unkChunk(databuffer, "PVCM");
			}
			else {
				System.err.println("Given file was not a valid wmo_root file");
			}
		}
		else {
			System.err.println("Version " + mver.getVersion() + " is not supported.");
			return;
		}
		} catch(ChunkNotFoundException e) {
			throw new InvalidClassException(e.getMessage());
		}
	}
	
	/**
	 * Creates a new wmo file
	 * NOT IMPLEMENTATED RIGHT NOW!
	 * @throws ChunkNotFoundException 
	 */
	private void create() throws ChunkNotFoundException{
		mver = new MVER();
		mohd = new MOHD();
		motx = new MOTX();
		momt = new MOMT();
		mogn = new MOGN();
		mogi = new MOGI();
		mosb = new unkChunk("BSOM");
		mopv = new unkChunk("VPOM");
		mopt = new unkChunk("TPOM");
		mopr = new unkChunk("RPOM");
		movv = new unkChunk("VVOM");
		movb = new unkChunk("BVOM");
		molt = new unkChunk("TLOM");
		 
		 mods = new MODS();
		  
		
		modn = new MODN();
		modd = new MODD();
		mfog = new unkChunk("GOFM");
		//mcvp = new MCVP();
	}
	
// -------------------------------------------------------	

	/**
	 * Renders the whole data and prepare everything
	 * for save routines (set offsets and so on)
	 */
	public void render() {
		int ro = 0; // running offset
		
		mver.render();
		ro+=mver.getSize();
		
		mohd.render();
		ro+=mohd.getSize();
		
		motx.render();
		ro+=motx.getSize();
		
		momt.render();
		ro+=momt.getSize();
		
		mogn.render();
		ro+=mogn.getSize();
		
		mogi.render();
		ro+=mogi.getSize();
		
		mosb.render();
		ro+=mosb.getSize();
		
		mopv.render();
		ro+=mopv.getSize();
		
		mopt.render();
		ro+=mopt.getSize();
		
		mopr.render();
		ro+=mopr.getSize();
		
		movv.render();
		ro+=movv.getSize();
		
		movb.render();
		ro+=movb.getSize();
		
		molt.render();
		ro+=molt.getSize();
		
		mods.render();
		ro+=mods.getSize();
		
		modn.render();
		ro+=modn.getSize();
		
		modd.render();
		ro+=modd.getSize();
		
		mfog.render();
		ro+=mfog.getSize();
		
		if(mcvp!=null){
		mcvp.render();
		ro+=mcvp.getSize();
		}
		
		// malloc new memory
		buff = ByteBuffer.allocate(ro);
		buff.order(ByteOrder.LITTLE_ENDIAN);
		buff.position(0);
		
		buff.put(mver.buff);
		buff.put(mohd.buff);
		buff.put(motx.buff);
		buff.put(momt.buff);
		buff.put(mogn.buff);
		buff.put(mogi.buff);
		buff.put(mosb.buff);
		buff.put(mopv.buff);
		buff.put(mopt.buff);
		buff.put(mopr.buff);
		buff.put(movv.buff);
		buff.put(movb.buff);
		buff.put(molt.buff);
		buff.put(mods.buff);
		buff.put(modn.buff);
		buff.put(modd.buff);
		buff.put(mfog.buff);
		
		if(mcvp!=null)
		buff.put(mcvp.buff);
		
		buff.position(0);
	}
	
// -------------------------------------------------------	
			
	
	
	/**
	 * Has not realy a use right now
	 * @return
	 */
	public int getVersion(){
		if(mver!=null)
			return mver.getVersion();
		else return -1;
	}

}
