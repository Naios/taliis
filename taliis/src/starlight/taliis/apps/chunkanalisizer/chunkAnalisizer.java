package starlight.taliis.apps.chunkanalisizer;

import starlight.taliis.core.memory;
import starlight.taliis.core.files.adt;
import starlight.taliis.helpers.fileLoader;

/**
 * This is a smal tool written to open a 
 * given File and except its in a WoW chunked
 * structure that is unknown.
 * 
 * Now it will print out any found Data about it.
 * 
 * @author tharo
 *
 */

public class chunkAnalisizer {
	memory obj;
	
	public chunkAnalisizer(String FileName) {
		// open memory interface for FileName
		 obj = new memory( fileLoader.openBuffer(FileName) );
	}
	
	public void analysize() {
		// scan whole file
		while(obj.buff.hasRemaining()) {
			int offs = obj.position();
			
		  // get magic
			byte mag[] = new byte[4];
			String magic;
			
			obj.buff.get(mag, 0, 4);
			magic = new String(mag);
				
		  // get size
			int s = obj.buff.getInt();
			
			foundEntry n = new foundEntry(magic, offs, s);
			n.printInfo(0);
			
		  // go next
			obj.position( obj.position() + s ); 
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length<1) System.exit(-1);

		chunkAnalisizer a = new chunkAnalisizer(args[0]);
		a.analysize();
	}

}

/**
 * smal storage class for chunks we found
 * @author tharo
 */
class foundEntry {
	public String magic;
	public long glbOffset = -1;
	public long locOffset = -1;
	public long size = -1;
	
	public foundEntry(String mgc, long global_offs, long s) {
		magic = mgc;
		glbOffset = global_offs;
		size = s;
	}
	public foundEntry(String mgc, long global_offs, long local_offs, long s) {
		magic = mgc;
		glbOffset = global_offs;
		locOffset = local_offs;
		size = s;
	}
	
	/**
	 * level = number of tabs in our front
	 * @param level
	 */
	public void printInfo(int level) {
		for(int c=0; c<level; c++) System.out.print("\t");
		System.out.print(magic + "\t");
		System.out.print("0x" + Long.toHexString(glbOffset) + "\t");
		System.out.print(size + "\t");
		if(locOffset!=-1) System.out.print("0x" + Long.toHexString(locOffset));
		System.out.println();
	}
	
	public String toString(){
		return magic;
	}
}
