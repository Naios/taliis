package starlight.taliis.apps.creator;

/**
 * Creator aims to be a programm that is able to create
 * a complete new ADT file and place things on it ..
 * 
 * lets hope the best :/
 * 
 * @author tharo
 *
 */

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.*;
import java.io.*;
//import javax.vecmath.Vector3d;

import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.core.chunks.adt.MCNR;
import starlight.taliis.core.files.adt;
import starlight.taliis.helpers.adtChecker;
import starlight.taliis.helpers.adtCoordHelper;
import starlight.taliis.helpers.adtObjHelper;

public class main {
	adt obj;
	adtChecker check;
	String filename = "./files/Azeroth_31_49.tharo.adt";
	
	/**
	 * creates a complete new map to test on it.
	 */
	main() {
		obj = new adt();
		
		// setup some helpers
		check = new adtChecker(obj);
		adtCoordHelper cordh = new adtCoordHelper(obj);
		adtObjHelper objh = new adtObjHelper(obj);
		
		// Add some Textures
		int texID = obj.mtex.addString("Tileset\\SilverPine\\SilverPineDirt.blp");
		obj.mtex.addString("Tileset\\Elwynn\\ElwynnFlowerBase.blp");
		
		// add a dd
		int ddID = obj.mmdx.addString("world\\Kalimdor\\Ashenvale\\ActiveDoodads\\MannarothSpear\\AshenvaleMannarothSpear.m2");
		
		// add a wmo
		int wmoID = obj.mwmo.addString("world\\wmo\\azeroth\\buildings\\guildhouses\\guildhouseb.wmo");
		obj.mwmo.addString("World\\wmo\\Dungeon\\LD_ShadowFang\\LD_ShadowFang.wmo");

		// set our coordinates
		cordh.calcCoordinates(49, 31, 60F, texID);
		cordh.loadHeightMap("./images/hmap.jpg", 0.5F);
		
		// place a DD
		objh.addDoodad(ddID, 96050, -9270F, 300F, 60.5F);
		objh.addDoodad(ddID, 96051, -9260F, 300F, 60.5F);
		objh.addDoodad(ddID, 96052, -9280F, 300F, 60.5F);
		objh.addDoodad(ddID, 96053, -9250F, 300F, 60.5F);/**/
		
		// place a wmo building
		objh.addWMO(wmoID, 96049, -9288F, 300F, 60.5F);
		
		
		// register appeareances
		objh.generateAppeareances();
		
		// render
		obj.render();
		if(check.check()==0)
			write();
	}
	
	/**
	 * Opens 'filename' to make some basic tests with it
	 * @param filename
	 */
	main(String FileName) {
		this.filename = FileName;
		
		try {
		// open the file an isolate the file channel
			File f = new File(FileName);
		//FileInputStream fis = new FileInputStream(f);
			RandomAccessFile fis = new RandomAccessFile(f, "rw");
			FileChannel fc = fis.getChannel();
			
		// create buffer (lill endian) with our files data
			int sz = (int)fc.size();
			ByteBuffer bb = fc.map(FileChannel.MapMode.READ_WRITE, 0, sz);
			bb.order(ByteOrder.LITTLE_ENDIAN);
			
		// re alocate data to RAM
			ByteBuffer tmp = ByteBuffer.allocate(bb.capacity());
			tmp.order(ByteOrder.LITTLE_ENDIAN);
			bb.position(0);
			tmp.put(bb);
			tmp.position(0);
			
		// init adt file
			obj = new adt(tmp);
			check = new adtChecker(obj);
			
		// clean up
			fc.close();
			fis.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// do our test stuff	
		// ------------------------------------------------------------
		// set our coordinates 43-46_ 51-55
		adtCoordHelper cordh = new adtCoordHelper(obj);
		for(int i=0; i<5; i++) {
			for(int j=0; j<4; j++) {
				cordh.moveCoordinates((51+i), (43+j));
				obj.render();
				
				if(check.check()==0) {
					System.out.println("Kalimdor_" +(43+j) + "_" + (51+i)+".adt");
					filename = "./files/Kalimdor_" +(43+j) + "_" + (51+i)+".adt";
					write();
				}
			}
		}
		
		//cordh.calcCoordinates(55, 43, 5F, texID);
		//filename = "./files/Kalimdor_43_55.adt";
		
		
		obj.render();
		
		// re-check and write
		System.out.println("\nRendered File: ");
		if(check.check()==0) {};
			//write();
	}
		
	
	/**
	 * Writes new adt file into test file
	 */
	public void write() {
		// write buffer
		File file = new File(filename);
		
		boolean append = false;
		try {
			// Create a writable file channel
			FileChannel wChannel = new FileOutputStream(file, append).getChannel();

			// write out
			wChannel.write(obj.buff);
		    
			// Close the file
			wChannel.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		// test 1:	create ADT file
		new main("./files/Kalimdor_37_54.adt"/**/); // "files/Azeroth_31_49.adt"
	}
}
