package starlight.taliis.apps.wmotest;

import java.io.File;
import java.io.InvalidClassException;

import starlight.taliis.core.files.wmo;
import starlight.taliis.core.files.wmo_root;
import starlight.taliis.helpers.fileLoader;

/**
 * Well, a smal app that test our new wmo classes
 * 
 * @author ganku
 *
 */
public class wmoTest {
	
	public static void main(String[] args) {
		for(String s : args){
			new wmoTest(s);
			System.out.println("___________________\n");
		}
	}

	
	wmoTest(String FileName) {
		File f = new File(FileName);
		
		System.out.print("Loading .. ");

		wmo obj;
		try {
			obj = wmo.load( fileLoader.openBuffer(FileName) );
		} catch (InvalidClassException e) {
			System.err.println("Unable to load wmo file!");
			System.err.println(e.getMessage());
			return;
		}
		
		System.out.println(f.getName()+"\n");
		
		if(obj instanceof wmo_root) {
			wmo_root ro = (wmo_root)obj;
			System.out.println("Type:\twmo root");
			
			System.out.println("Textures:\t" + ro.motx.getLenght());
			
			System.out.println("n Materials:\t" + ro.mohd.getNMaterias());
			System.out.println("Materials:\t" + ro.momt.getLenght());
			
			System.out.print("Group names:\t" + ro.mogn.getLenght() + "\t(");
			for(int i=0; i<ro.mogn.getLenght(); i++) {
				if(i!=0) System.out.print(",");
				System.out.print( ro.mogn.getString(i) );
			}
			System.out.println(")");
			System.out.println("n Groups:\t" + ro.mohd.getNGroups());
			System.out.println("Groups:  \t" + ro.mogi.getLenght());
			System.out.println("n Portals:\t" + ro.mohd.getNPortals());
			System.out.println("Portals:\t" + ro.mopv.getLenght());
			//System.out.println(":\t" + ro);
		}
	}
}
