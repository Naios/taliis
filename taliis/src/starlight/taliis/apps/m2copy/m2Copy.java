package starlight.taliis.apps.m2copy;

import starlight.taliis.core.binary.m2.particleEmitter;
import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.m2;
import starlight.taliis.helpers.fileLoader;

public class m2Copy {
	String inFile, outFile;
	m2 data1, data2;
	
	public m2Copy(String inF, String outF) {
		inFile = inF;
		outFile = outF;
		
		// load files
		if(inFile.endsWith(".m2") || inFile.endsWith(".M2")) {
			data1 = new m2( fileLoader.openBuffer(inFile) );
		}
		if(outFile.endsWith(".m2") || outFile.endsWith(".M2")) {
			data2 = new m2( fileLoader.openBuffer(outFile) );
		}
		
		// print out emitting infomations
		System.out.println("Partikel Emitters src: " + data1.getNParticleEmitters());
		System.out.println("Partikel Emitters dest: " + data2.getNParticleEmitters());
		
		// copy emitetrs to the destiny
			// gr��e der zu kopierenden werte?
		int adds = data1.getNParticleEmitters() * particleEmitter.end;
		int oldend = data2.limit();
		
			// ziel vergr��ern
		data2.expand(adds);
			// kopieren
		data2.position(oldend);
		data1.position( data1.getParticleEmittersOffs() );

		for(int i=0; i<adds; i++) {
			data2.buff.put(data1.buff.get());
		}
			// offset
		data2.setParicleEmittersOffs(oldend);
			// NParticleEmitters
		data2.setNParticleEmitters(data1.getNParticleEmitters());
		
		// save
		data2.buff.position(0);
		fileLoader.saveBuffer(data2.buff, outFile + ".taliis.m2");
	}
	
	
	
	public static void main(String[] args) {
		System.out.println("Taliis Shadow Tool (c)2008 by Mae, Ganku");
		System.out.println("                   Visit http://taliis.blogspot.com !!\n");
		
		if(args.length==2){
			m2Copy mc = new m2Copy(args[0], args[1]);
		}
		else {
			System.out.println("Usage: java -jar m2Copy <src file> <dest file>");
			System.exit(0);
		}
		
	}
}
