package starlight.taliis.apps.editors.adt3D;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Texture;
import javax.media.j3d.Texture2D;

import com.sun.j3d.utils.image.TextureLoader;

//import starlight.taliis.apps.adt3Dtest.myTextureLoader;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.core.files.adt;


public class Area extends BranchGroup {
	adt obj;
//	myTextureLoader myLoader;

	/**
	 * constructor
	 * @param map loaded adt file reference
	 */
	public Area(adt map) {
		obj = map;
//		myLoader = new myTextureLoader();
	
		// load textures
		int texn = obj.mtex.getLenght();
		String[] fileNames = new String[texn];
		
		for(int c=0; c<texn; c++) 
			fileNames[c] = obj.mtex.getValueNo(c);
		
//		myLoader.loadBaseTextures(fileNames);
		

		long start = System.currentTimeMillis();
		
		// load / create fields
		for(int x=0; x<16; x++)
    		for(int y=0; y<16; y++) {
    			MCNK t = obj.mcnk[16*x+y];
    			FieldBranch bla = new FieldBranch(
    					t, 
    					t.getIndexX(), 
    					t.getIndexY(), 
    					0.1);
    			//if(x>8 && y<8) bla.setLayerVisibility(0, false);
    			addChild(bla.getBranchGroup());
    		}
		long end = System.currentTimeMillis();
		System.out.println("Loading took: " + (end - start) + "ms");
	}
}
