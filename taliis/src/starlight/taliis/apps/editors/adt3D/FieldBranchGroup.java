package starlight.taliis.apps.editors.adt3D;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.media.j3d.Appearance;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Material;
import javax.media.j3d.Node;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Texture;
import javax.media.j3d.Texture2D;
import javax.media.j3d.TextureAttributes;
import javax.media.j3d.TextureUnitState;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TriangleStripArray;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Point3f;
import javax.vecmath.Quat4f;
import javax.vecmath.TexCoord2f;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.image.TextureLoader;


//import starlight.taliis.apps.adt3Dtest.myTextureLoader;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.MCAL_Entry;
import starlight.taliis.core.chunks.adt.MCLY_Entry;
import starlight.taliis.core.chunks.adt.MCNK;

/**
 * This is the second try ..
 * 
 * So this branch Group displays an MCYN chunk of a ADT Field
 * hate it !
 * 
 * @author tharo
 *
 */
public class FieldBranchGroup {
	// main j3d stuff
	BranchGroup root;	
	Appearance app;
	TextureUnitState textureUnitState[] = new TextureUnitState[10];
	PolygonAttributes polyAttr;
	
	// coordinates stuff
	double xOffset, yOffset, zOffset;
	double globalSize = 33.3333333333;
	double scaleFactor;
	
	// display options
	boolean layerVis[] = new boolean[5];
	
	// given data
	MCNK data;
	//myTextureLoader texLoader;
	
	// for debug files: a filename prestring
	String debFileStr = "";
	
	/* Constructors and stuff */
	public FieldBranchGroup(MCNK mapObject, /*chunk ref*/
			//myTextureLoader rTexLoader, /* text pool*/
			double xOffs, double yOffs,
			double scale,
			boolean lod)
	{		
	// save important infos
		data = mapObject;
		//texLoader = rTexLoader;
		
		globalSize *= scale;
		scaleFactor = scale;

		yOffset = data.getIndexX()*globalSize;
		xOffset = data.getIndexY()*globalSize;
		zOffset = data.getPosZ()*scale;
	
		debFileStr = data.getIndexX() + "_" + data.getIndexY() + "_";
		
	// layer visiblity 
		for(int c=0; c<5; c++)
			layerVis[c] = true;
		
	// some output that might be usefull later
		/*System.out.print(data.getIndexX() + "/" + data.getIndexY() + ": ");
		//System.out.print(xOffset + ", " + yOffset + ", " + zOffset + ": ");
		System.out.print(data.mcly.getLenght() + " Layers" );
		System.out.println();/**/
		
	// init root group
		root = new BranchGroup();
		
	// Geneate Heighmap
		//TODO: read heigmap from file !!
		//TODO: think about LOD
		double hf[][] = CreateRandomHeighfieldData(8, 8, -0.3, 0.3);
		
	// create out points row by row
		//TODO: think about LOD (twice, quantity + way of generating!)
		Point3f lines[][] = new Point3f[8][];	// point storage for 8 rows
		int striplens[] = new int[8];			// point count for 8 rows
		
		// number of the vertexes in all strips
		int vertexcount = 0;
		
		// einzelne streifen erstellen
		for(int y=0;y < 8;y++){
			// create a strip line along the x axis for each y segment
			lines[y] = CreateStripLineOfY(hf, y, globalSize, globalSize);
			
			vertexcount += lines[y].length;
			striplens[y] = lines[y].length;
		}

	// triangle array setup
		TriangleStripArray strip = new TriangleStripArray(vertexcount, 
				TriangleStripArray.COORDINATES | 
				TriangleStripArray.NORMALS | 
				TriangleStripArray.TEXTURE_COORDINATE_2, striplens); 
		
		// position of the data in in the strip data space
		int strippos = 0;
		
		// copy the vertex data into the strip
		for(int y=0;y < 8;y++){
			strip.setCoordinates(strippos, lines[y]);
			strippos += lines[y].length;
			
			// generate normals
			//TODO: READ normals?
			Vector3f normals[] = CreateNormalsStripLineOfY(hf,y);
			
			// and copy them into the strip
			strip.setNormals(y * normals.length, normals);
			
			// generate texture coordinates
			TexCoord2f texcoords[] = CreateTextureCoordinatesStripLineOfY(hf,y,1f);
			strip.setTextureCoordinates(0, y * texcoords.length, texcoords);
		}
		
		
	// polygon mode 
		polyAttr = new PolygonAttributes();
        polyAttr.setCullFace( PolygonAttributes.CULL_NONE );
        polyAttr.setPolygonMode(PolygonAttributes.POLYGON_LINE); //POLYGON_FILL);
		
	// textures
//        renderTextures();

	// appearance
		app = new Appearance();
		
		Material mat = new Material();
		mat.setLightingEnable(true);
		mat.setDiffuseColor(1.0f, 1.0f, 1.0f);
		
		app.setPolygonAttributes(polyAttr);
		app.setTextureUnitState(textureUnitState);
		app.setMaterial(mat);
		
		// add to root node
		root.addChild(new Shape3D(strip,app));
	}
	
	/* Set functions */
	//TODO:	all textures on/off (grid view)
	//TODO:	LOD on/off
	// texture layer 1-5 on/off
	public void setLayerVisibility(int layer, boolean value) {
		if(layer<0 || layer>4) return;
		else layerVis[layer] = value;
		
		renderTextures();
	}
	//TODO: water on/off
	
	/* private helpers */
	//TODO: DELETE !!
	private double[][] CreateRandomHeighfieldData(int w, int h, double minh,
			double maxh) {
		
		double hf[][] = new double[9][9];
		
		int c = 0;
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				hf[i][j] = data.mcvt.getVal(c++) *scaleFactor;
			}
			c+=8;
		}
		
		
		/*
		double dh = maxh - minh;
		
		for(int x=0;x <= w;++x)
			for(int y=0;y <= h;++y){
				hf[x][y] = minh + Math.random() * dh;
			}
		/**/
		return hf;/**/
	}
	
	/**
	 * creates the points for one strip line (the y-th) of hf 
	 * @param hf	height field data
	 * @param y		y index to get the data from
	 * @param xoffset	x position offset
	 * @param yoffset	y position offset
	 * @param zoffset	z position offset
	 * @param dx	size of one segment along x
	 * @param dy	size of one segment along y
	 * @return
	 */
	private Point3f[] CreateStripLineOfY(double[][] hf, int y, double dx, double dy) {
		
		// number of vertexes needed
		int count = 1 + 1 + (hf.length - 1) * 2;
		
		// distance 
		double distance = globalSize / 8;
		
		Point3f line[] = new Point3f[count];
		
		for(int i = 0;i < count; ++i){
			// position in hf data
			int hfx = (i - (i % 2)) / 2;
			int hfy = y + i % 2;
			
			// position in 3d space
			float px = (float)(xOffset + (hfx)*distance);
			float py = (float)(yOffset + (hfy)*distance);
			float pz = (float)(zOffset + (hf[hfx][hfy]));
			
			line[i] = new Point3f(px, py, pz);
		}
		
		return line;
	}
	
	/**
	 * Create normal Strip for our line of Data
	 * @param hf
	 * @param y
	 * @return
	 */
	private Vector3f[] CreateNormalsStripLineOfY(double[][] hf, int y) {
		// number of vertexes needed
		int count = 1 + 1 + (hf.length - 1) * 2;
		
		Vector3f normals[] = new Vector3f[count];
		
		for(int i = 0;i < count; ++i){
			// position in hf data
			int hfx = (i - (i % 2)) / 2;
			int hfy = y + i % 2;
			
			// calculate normal average of the 4 neighbor points along the x and y axis
			Vector3f l[] = new Vector3f[4];
			
			// generate normals
			l[0] = GenerateNormal(hf, hfx, hfy, hfx + 1, hfy + 0, hfx + 0, hfy + 1);
			l[1] = GenerateNormal(hf, hfx, hfy, hfx + 0, hfy + 1, hfx - 1, hfy + 0);
			l[2] = GenerateNormal(hf, hfx, hfy, hfx - 1, hfy + 0, hfx + 0, hfy - 1);
			l[3] = GenerateNormal(hf, hfx, hfy, hfx + 0, hfy - 1, hfx + 1, hfy + 0);
			
			// calculate average
			float nx = 0.0f;
			float ny = 0.0f;
			float nz = 0.0f;
			
			for(int j=0;j < l.length;++j){
				nx += l[j].x;
				ny += l[j].y;
				nz += l[j].z;
			}
			
			float lcount = l.length;
			normals[i] = new Vector3f(nx / lcount, ny / lcount, nz / lcount);
			normals[i].normalize();
			normals[i].negate();
		}
		
		return normals;
	}
	
	/**
	 * generates a normal calculated from the base-a and base-b vector
	 * @param hf height field data
	 * @param basex	position in data array
	 * @param basey position in data array
	 * @param ax position in data array
	 * @param ay position in data array
	 * @param bx position in data array
	 * @param by position in data array
	 * @return normal
	 */
	private Vector3f GenerateNormal(double[][] hf, int basex, int basey, int ax,
			int ay, int bx, int by) {
		
		Vector3f base = new Vector3f(basex,basey,GetHFHeight(hf,basex,basey));
		Vector3f a = new Vector3f(ax,ay,GetHFHeight(hf,ax,ay));
		Vector3f b = new Vector3f(bx,by,GetHFHeight(hf,bx,by));
		
		Vector3f s1 = new Vector3f(a);
		Vector3f s2 = new Vector3f(b);
		
		s1.sub(base);
		s2.sub(base);
		
		Vector3f n = new Vector3f();
		n.cross(s1, s2);
		
		n.normalize();

		return n;
	}
	private float GetHFHeight(double[][] hf, int basex, int basey) {
		int w = hf.length;
		int h = hf[0].length;
		
		if(basex < 0) basex = 0;
		else if(basex >= w) basex = w-1;
		if(basey < 0) basey = 0;
		else if(basey >= h) basey = h-1;
		
		return (float)hf[basex][basey];
	}
	
	/**
	 * Create texture coordinates for our strip line
	 * @param hf
	 * @param y
	 * @param scale
	 * @return
	 */
	private TexCoord2f[] CreateTextureCoordinatesStripLineOfY(double[][] hf,
			int y, float scale) {
		
		// number of vertexes needed
		int count = 1 + 1 + (hf.length - 1) * 2;
		
		// data size
		float w = hf.length;
		float h = hf[0].length;
		
		TexCoord2f texs[] = new TexCoord2f[count];
		
		for(int i = 0;i < count; ++i){
			// position in hf data
			int hfx = (i - (i % 2)) / 2;
			int hfy = y + i % 2;
			
			texs[i] = new TexCoord2f((float)hfx * scale / w, (float)hfy * scale / h);
			
		//	System.out.println("x="+hfx+" y="+hfy+" tex="+texs[i]);
		}
		
		return texs;
	}

	/**
	 * take care of render all texture layers including effects
	 * to the global textureunistate array
	 */
	private void renderTextures() {
        Texture tex;
        TextureAttributes texAttr;
        
    // absolute background
        if(layerVis[0]==false) {
        	polyAttr.setPolygonMode(PolygonAttributes.POLYGON_LINE);
        	return;
        }
        else {
        	polyAttr.setPolygonMode(PolygonAttributes.POLYGON_FILL);
    		// deactivate
        	if(1==1) return;
        	
        	// muss die textur neu erstellt werden?
        	//TODO: cache funktion ..
        	
        	// einmaliges pre-rendering der texturen
        	BufferedImage screen, alpha, layer, tmp;
        	screen = new BufferedImage(256, 256, BufferedImage.TYPE_4BYTE_ABGR_PRE);
        	tmp = new BufferedImage(256, 256, BufferedImage.TYPE_4BYTE_ABGR_PRE);
        	
        	// graphics objekt erstellen
        	Graphics2D g2 = (Graphics2D)screen.getGraphics();
        	RenderingHints rh = new RenderingHints(
        			RenderingHints.KEY_TEXT_ANTIALIASING,
        			RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        	rh.put(RenderingHints.KEY_ALPHA_INTERPOLATION,
        			RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
        	//g2.setRenderingHints(rh);
        	
        	
        	// bild holen
        	MCLY_Entry lData = data.mcly.getLayer(0);    	
        	//layer = texLoader.getBitmapNo( lData.getTextureID() );
    
        	// rendern
        	//g2.drawImage(layer, null, 0, 0);
        	
        	// das selbe f�r alle layer
        	for(int i=0; i<data.getNLayers()-1; i++) {        		
        		lData = data.mcly.getLayer(i+1);
        		
        		// bild holen
            	//layer = texLoader.getBitmapNo( lData.getTextureID() );
            	
        		// lightmap holen
            	//alpha = createLightMap(i);	
            	
            	// alpha setzen
//            	NewAlphaFilter filter = new NewAlphaFilter(data.mcal.getLayerNo(i));
                //tmp = filter.createCompatibleDestImage(layer, screen.getColorModel());

               // filter.filter(layer, tmp);
              
            	// rendern
            	g2.drawImage(tmp, null, 0, 0);
        	}
            
        	// chacen
        	//saveImage(screen, "screen");
        	
        	// screen in die textur umwandeln und anzeigen
        	ImageComponent2D texImage = new ImageComponent2D(
        	        ImageComponent.FORMAT_RGBA4, screen);

        	tex = new Texture2D(Texture.BASE_LEVEL, Texture.RGB, 256, 256);
        	tex.setImage(0, texImage);
        	
	        texAttr = new TextureAttributes();
			texAttr.setTextureMode(TextureAttributes.MODULATE);
			textureUnitState[0] = new TextureUnitState(tex, texAttr, null);
		    textureUnitState[0].setCapability(TextureUnitState.ALLOW_STATE_WRITE);
        }
    }
	
	/**
	 * Create, scale and blut light map for layer n
	 * @param n layer that data is puposed to get used
	 * @return
	 */
	public BufferedImage createLightMap(int n) {
		int dimension = 64;
	    
		// werte bereitstellen
		MCAL_Entry alpha = data.mcal.getLayerNo(n);
		
	    // neues bild als buffered Image erstellen
	    BufferedImage bimage = new BufferedImage(dimension, dimension,
	        BufferedImage.TYPE_INT_RGB );
	    
	    int[] rgbArray = new int[dimension * dimension];
	    int i, j;

	    // lightmap erstellen 
	    for(i=0; i<dimension; i++)
	    	for(j=0; j<dimension; j++) {
	    		rgbArray[dimension*i+j] = alpha.getValue(dimension*i+j);
	    	}
	    
	    // werte �bernehmen
	    bimage.setRGB(0, 0, dimension, dimension, rgbArray, 0, dimension);	    
	    
	    saveImage(bimage, "aMap-a-original");
	    
	    // scalieren / drehen
	    BufferedImage tmp = new BufferedImage(256, 256, BufferedImage.TYPE_INT_RGB);
	    Graphics2D gt = (Graphics2D) tmp.getGraphics();
	    gt.drawImage(bimage, 0, 0, 256, 256, 0, 0, 64, 64, null);
	    gt = null;
	    
	    saveImage(tmp, "aMap-b-resized");
	    
	    // weich zeichnen
	    /*BoxBlurFilter b = new BoxBlurFilter(4, 4, 2);
        b.filter(tmp, tmp);
	    /**/
	    saveImage(tmp, "aMap-c-blured");
	    
	    return tmp;
	}	

	 public void saveImage(BufferedImage img, String ref) {  
		 
		 try {  
		         ImageIO.write(img, "png", new File("./debug/" + debFileStr + ref + ".png"));  
		     } catch (IOException e) {  
		         e.printStackTrace();  
		     }/**/  
	 } 
	
	/* access stuff */
	public BranchGroup getBranchGroup() {
		return root;
	}
}
