package starlight.taliis.apps.editors.adt3D;

import java.awt.AWTEvent;
import java.awt.event.KeyEvent;
import java.util.Enumeration;

import javax.media.j3d.Behavior;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.WakeupOnAWTEvent;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

public class BevWASD extends Behavior {
    private double rotationX = 0;
    private double rotationY = 0;
    private double rotationZ = 0;
    private float addX = 0;
    private float addY = 0;
    
    private Transform3D transAdd = new Transform3D();
    private Transform3D transRot = new Transform3D();
    private TransformGroup tg;
    
    public void initialize() {
        setSchedulingBounds(new BoundingSphere(new Point3d(0,0,0),100));
        wakeupOn(new WakeupOnAWTEvent(AWTEvent.KEY_EVENT_MASK));
    }
    
    public void setTransformGroup(TransformGroup t) {
    	tg = t;
    }
    
    public void processStimulus(Enumeration enumeration) {
        // reschedule the wake up 
    	wakeupOn(new WakeupOnAWTEvent(AWTEvent.KEY_EVENT_MASK));

        KeyEvent event = (KeyEvent) ((WakeupOnAWTEvent) enumeration.nextElement()).getAWTEvent()[0];
        
        // add a little to the rotation
        if (event.getKeyCode() == KeyEvent.VK_A) addY++;
        if (event.getKeyCode() == KeyEvent.VK_D) addY--;
        if (event.getKeyCode() == KeyEvent.VK_W) addX++;
        if (event.getKeyCode() == KeyEvent.VK_S) addX--;
        
        if(event.getKeyCode() == KeyEvent.VK_LEFT) rotationZ--;
        else if(event.getKeyCode() == KeyEvent.VK_RIGHT) rotationZ++;
        
        // update the transform affecting this shape
        transAdd.setIdentity();
        transAdd.setTranslation(new Vector3f(addX, addY, 0.F ));
        
        // rotations
        transRot.rotZ(rotationZ / 10);
        transAdd.mul(transRot);
        
        tg.setTransform(transAdd);
    }
}
