package starlight.taliis.apps.editors;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import starlight.alien.*;
import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.MDDF;
import starlight.taliis.core.chunks.adt.MDDF_Entry;
import starlight.taliis.core.chunks.adt.MMDX;
import starlight.taliis.helpers.adtObjHelper;
import starlight.taliis.helpers.fileLoader;

/**
 * JTable interface for edit MTEX chunks in ADT files
 * @author tharo
 *
 */

public class adtDoodadAppTable extends tallisStdEditor 
						implements ActionListener {
	adt obj;
	JTable table;
	JToolBar toolBar;
	
	public adtDoodadAppTable(adt reference) {
		obj = reference;
		this.setLayout(new BorderLayout());
			
		toolBar = new JToolBar();
		JButton  button = makeNavigationButton("delete", ICON_DEL,
                "Delete given texture File",
                "Remove Texture");
		toolBar.add(button);
		button = makeNavigationButton("add", ICON_ADD,
                "Add a new texture File",
                "Add Texture");
		toolBar.add(button);
		toolBar.addSeparator();
		add(toolBar, BorderLayout.PAGE_START);
			
        table = new JTable(new mddfTableModel(obj.mddf, obj.mmdx));
        //table.setPreferredScrollableViewportSize(new Dimension(70, 70));

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 
        
        // set some table look and feel
        initTableFace();
        
        //Add the scroll pane to this panel.
        add(scrollPane);
	}
	
	void initTableFace() {
		TableColumn column;
		
		// file id
		column = table.getColumnModel().getColumn(0);
        column.setPreferredWidth(50);
        column.setCellRenderer(new TableColorRenderer(Color.WHITE));
        
        table.getColumnModel().getColumn(1).setMaxWidth(90);

		// first floats
		for(int c=2; c<5; c++) {
			column = table.getColumnModel().getColumn(c);
			column.setMaxWidth(150);
			column.setCellRenderer(new TableColorRenderer(new Color(224,255,224)));
		}
		
		// orientation floats
		for(int c=7; c<10; c++) {
			column = table.getColumnModel().getColumn(c);
			column.setMaxWidth(150);
			column.setCellRenderer(new TableColorRenderer(new Color(224,255,224)));
		}
		
		

	}
	
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (ICON_ADD.equals(cmd)) {
        	if(obj.mmdx.getLenght()==0) {
        		JOptionPane.showMessageDialog(this.getParent(),
        				"No Doodads registered!",
        				"Error",
        				JOptionPane.ERROR_MESSAGE);
        		return;
        	}
        	// add dirty empty new doodad
        	adtObjHelper objh = new adtObjHelper(obj);
        	Random r = new Random();
        	objh.addDoodad(0, r.nextInt(100000)+1);
        	
        	table.updateUI();
        }
        else if (ICON_DEL.equals(cmd)) {
        	if(table.getSelectedRow()==-1) return;
        	obj.mddf.remove( table.getSelectedRow()  );
        	table.updateUI();
        }
    }

}

class mddfTableModel extends AbstractTableModel {
	MDDF archive;
	MMDX files;
	
	// Bezeichnungen
    Vector columnNames = new Vector();
    
    mddfTableModel(MDDF reference, MMDX files_ref) {
    	archive = reference;
    	files = files_ref;
    }
    
    public int getColumnCount() {
        return 10;
    }

    public int getRowCount() {
    	return (int)archive.getLenght();
    }

    public String getColumnName(int col) {
        switch(col) {
        	case 0: return "File ID";
        	case 1: return "Unique ID";
        	case 2: return "x";
        	case 3: return "y";
        	case 4: return "z";
        	case 5: return "Scale";
        	case 6: return "Flags";
        	case 7: return "rot a";
        	case 8: return "rot b";
        	case 9: return "rot c";
        }
        return "";
    }

    public Object getValueAt(int row, int col) {
    	switch(col) {
	    	case 0: {
	    		int id = archive.entrys[row].getNameID();
	    		String filename = files.getValueNo(id);
	    		return new AdvJTableCell(id, filename);
	    	}
	    	case 1: return archive.entrys[row].getUniqID();
	    	case 2: return MDDF_Entry.translate(archive.entrys[row].getX());
	    	case 3: return MDDF_Entry.translate(archive.entrys[row].getY());
	    	case 4: return archive.entrys[row].getZ();
	    	case 5: return archive.entrys[row].getScale();
	    	case 6: return archive.entrys[row].getFlags();
	    	case 7: return archive.entrys[row].getA();
	    	case 8: return archive.entrys[row].getB();
	    	case 9: return archive.entrys[row].getC();
    	}
	    return null;
    }

/*    public Class getColumnClass(int c) {
        if(c==0) return Icon.class;
        else return String.class;
    }/**/

    public boolean isCellEditable(int row, int col) {
    	//if(col==1) return true;
    	return true;
    }

    public void setValueAt(Object value, int row, int col) {
    	// translate value
    	int vInt = 0;
    	float vFloat = 0;
    	
	    try {
	    	switch(col) {
	    		case 0: case 1: case 5: case 6:
	    			// integer value
	    			vInt = Integer.valueOf(value.toString());
	    			break;
	    		case 2: case 3: case 4: case 7: case 8: case 9:
	    			// float value
	    			vFloat = Float.valueOf(value.toString());
	    			break;
	    		default:
	    			return;
	    	}
	    } catch(Exception exp) {
	    	return;
	    }
	// unique id	TODO: check for aviability
	    switch(col) {
	    	case 0:
	    		if(vInt>=0 || vInt<archive.getLenght())
	    	    		archive.entrys[row].setNameID(vInt);
	    		break;
	    	case 1: archive.entrys[row].setUniqID(vInt); break;
	    	case 2: archive.entrys[row].setX( MDDF_Entry.translate( vFloat ) ); break;
	    	case 3: archive.entrys[row].setY( MDDF_Entry.translate( vFloat ) ); break;
	    	case 4: archive.entrys[row].setZ( vFloat ); break;
	    	case 5: archive.entrys[row].setScale((short)vInt); break;
		    //case 6: archive.entrys[row]
	    	case 7: archive.entrys[row].setA(vFloat); break;
	    	case 8: archive.entrys[row].setB(vFloat); break;
	    	case 9: archive.entrys[row].setC(vFloat); break;
	    }
    }
    
    public void clear() {
    }
}