package starlight.taliis.apps.editors;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;


import starlight.alien.*;
import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.files.dbc;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Vector;
import java.util.ArrayList;



public class dbcStringTable extends tallisStdEditor {
    private boolean DEBUG = false;
    private dbc cUplink;
    JToolBar toolBar;
    JTextField newString;
    JTable table;
    
    public dbcStringTable (dbc daddy) {
    	cUplink = daddy;

    	this.setLayout(new BorderLayout());
		
    	newString = new JTextField();
    	newString.setText("String lenght CANNOT changed at the moment!");
    	
		toolBar = new JToolBar();
		JButton button = makeNavigationButton("add", ICON_ADD,
                "Add a new String Entry",
                "Add String");
		toolBar.add(button);
		toolBar.addSeparator();
		toolBar.add(newString);
		add(toolBar, BorderLayout.PAGE_START);
		
    	
        table = new JTable(new dbcStringTableModel(cUplink));
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 
        
        //Add the scroll pane to this panel.
        add(scrollPane);
        
        // set a nice width
        TableColumn column = table.getColumnModel().getColumn(0);
		column.setMaxWidth(200);
		column.setWidth(75);
    }
    
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (ICON_ADD.equals(cmd)) {
        	cUplink.addString(newString.getText());
        	table.updateUI();
        }
    }
}



class dbcStringTableModel extends AbstractTableModel {
	dbc archive;
	
	// Bezeichnungen
	Vector labels;
    String[] columnNames = {
    		"#offset",
    		"Text",
    };


    dbcStringTableModel(dbc reference) {
    	archive = reference;
    }
    
    public int getColumnCount() {
        return 2;
    }

    public int getRowCount() {
       return archive.getStrLenght();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
    	ZeroTerminatedString tmp = archive.getString(row);
    	
    	if(col==0) return tmp.getInitOffset();
    	if(col==1) return tmp.toString();
    	
    	else return "error";
    }

/*    public Class getColumnClass(int c) {
        return dbc;
    }*/

    public boolean isCellEditable(int row, int col) {
    	return false;
    }

    public void setValueAt(Object value, int row, int col) {
    }
    
    public void clear() {
    }
}