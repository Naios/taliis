package starlight.taliis.apps.adtedit;

import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.dbc;
import starlight.taliis.helpers.fileLoader;

/**
 * A smal class that stores and provides everything needed
 * for an "opened" file.
 * 
 * @author tharo
 *
 */
class openFile {
	// ADT Icons
	static ImageIcon brick = fileLoader.createImageIcon("images/icons/brick.png");
	static ImageIcon bricks = fileLoader.createImageIcon("images/icons/bricks.png");
	static ImageIcon house = fileLoader.createImageIcon("images/icons/house.png");
	static ImageIcon folder = fileLoader.createImageIcon("images/icons/folder_page.png");
	static ImageIcon page = fileLoader.createImageIcon("images/icons/page_copy.png");
	static ImageIcon field = fileLoader.createImageIcon("images/icons/shape_move_forwards.png");
	static ImageIcon pack = fileLoader.createImageIcon("images/icons/package.png");
	static ImageIcon layers = fileLoader.createImageIcon("images/icons/layers.png");
	static ImageIcon table = fileLoader.createImageIcon("images/icons/table.png");
	static ImageIcon shade = fileLoader.createImageIcon("images/icons/contrast.png");
	static ImageIcon color = fileLoader.createImageIcon("images/icons/color_wheel.png");
	static ImageIcon liq = fileLoader.createImageIcon("images/icons/drink.png");
	
	//DBC icons
	static ImageIcon multi_table = fileLoader.createImageIcon("images/icons/table_multiple.png");
	static ImageIcon tag_blue = fileLoader.createImageIcon("images/icons/tag_blue.png");
	static ImageIcon tag_red = fileLoader.createImageIcon("images/icons/tag_red.png");
	
	//WDT icons
	static ImageIcon reppic = fileLoader.createImageIcon("images/icons/report_picture.png");
	static ImageIcon fields = fileLoader.createImageIcon("images/icons/shading.png");
	
	
	
	public String FileName;
	public Object obj;
	
	/**
	 * 
	 * @param path
	 */
	public openFile(String path) {
		String tmp[] = path.replace("\\", "/").split("/");
		FileName = tmp[tmp.length-1];
		
		if(FileName.endsWith("adt")) {
			System.out.println("Open adt File: " + FileName);
			obj = new adt( fileLoader.openBuffer(path) );
		}
		else if(FileName.endsWith("dbc")) {
			System.out.println("Open dbc File: " + FileName);
			obj = new dbc( fileLoader.openBuffer(path) );
		}
		else {
			System.err.println("File Extension is not supported.");
			return;
		}
	}
	
	public openFile(Object newObj, String name) {
		FileName = name;
		obj = newObj;
	}
	
	public void createNodes(DefaultMutableTreeNode root) {
		DefaultMutableTreeNode top;
		DefaultMutableTreeNode category = null;
	    DefaultMutableTreeNode book = null;
	    
	    if(obj.getClass()==adt.class) {
	    	adt o = (adt)obj;
	    	top = new DefaultMutableTreeNode(new MenueEntry(FileName, 0, this, pack));
	    	
		// files 
		    category = new DefaultMutableTreeNode(new MenueEntry("Files", 1, this, folder));
		    top.add(category);
		    book = new DefaultMutableTreeNode(new MenueEntry("Textures", 11, this, page, o.mtex));
		    category.add(book);
		    book = new DefaultMutableTreeNode(new MenueEntry("Models", 12, this, page, o.mmdx));
		    category.add(book);
		    book = new DefaultMutableTreeNode(new MenueEntry("Objects", 13, this, page, o.mwmo));
		    category.add(book);
		// Appeareances
		    category = new DefaultMutableTreeNode(new MenueEntry("Appeareances", 2, this, house));
		    top.add(category);
		    book = new DefaultMutableTreeNode(new MenueEntry("Models", 21, this, brick, o.mddf));
		    category.add(book);
		    book = new DefaultMutableTreeNode(new MenueEntry("Objects", 22, this, bricks, o.modf));
		    category.add(book);
		// fields
		    category = new DefaultMutableTreeNode(new MenueEntry("(256) Fields", 3, this, field));
		    top.add(category);
		    book = new DefaultMutableTreeNode(new MenueEntry("General", 31, this, table));
		    category.add(book);
		    book = new DefaultMutableTreeNode(new MenueEntry("Layers", 32, this, layers));
		    category.add(book);
		    book = new DefaultMutableTreeNode(new MenueEntry("Alpha", 33, this, color));
		    category.add(book);
		    book = new DefaultMutableTreeNode(new MenueEntry("Shadow", 34, this, shade));
		    category.add(book);
		    book = new DefaultMutableTreeNode(new MenueEntry("Liquid", 35, this, liq));
		    category.add(book);
	    
		    root.add(top);
	    }
	    else if(obj.getClass()==dbc.class) {
	    	top = new DefaultMutableTreeNode(new MenueEntry(FileName, 0, this, multi_table));
	    	
		// views
		    category = new DefaultMutableTreeNode(new MenueEntry("Table", 1, this, table));
		    top.add(category);
		    category = new DefaultMutableTreeNode(new MenueEntry("Raw Data", 2, this, table));
		    top.add(category);
		    category = new DefaultMutableTreeNode(new MenueEntry("Strings", 3, this, table));
		    top.add(category);
		    
		    root.add(top);
	    }
	}/**/
}