package starlight.taliis.apps.adtedit;


import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import starlight.taliis.helpers.fileLoader;

public class newADTDialog implements ActionListener{
	public boolean ok = false;
	public String name, xStr, yStr;
	public int x, y;
	
	JDialog frame;
	JTextField in1, in2, in3;
	JButton okButton = new JButton("OK");
	
	public void addComponentsToPane(Container pane){
    	pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		JLabel txt = new JLabel("Specify the Filename below.                 ");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 7;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(0,8,0,0);
		pane.add(txt, c);
	
		in1 = new JTextField();
		in1.setText("InstanceName");
		c.insets = new Insets(10,10,0,0);
		c.gridwidth = 2;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 1;
		pane.add(in1, c);
		
		c.gridwidth = 1;
		c.gridx = 2;
		c.weightx = 0.01;
		c.insets = new Insets(10,0,0,0);
		pane.add(new JLabel(" _ "), c);

		in2 = new JTextField();
		in2.setText("01");
		c.weightx = 0.1;
		c.gridx = 3;
		c.gridy = 1;
		pane.add(in2, c);
		
		c.gridx = 4;
		c.weightx = 0.01;
		pane.add(new JLabel(" _ "), c);
	
		in3 = new JTextField();
		in3.setText("01");
		c.weightx = 0.1;
		c.gridx = 5;
		c.gridy = 1;
		pane.add(in3, c);
		
		c.gridx = 6;
		c.weightx = 0.01;
		pane.add(new JLabel(" .adt"), c);
		
		c.gridx = 6;
		c.gridy = 2;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.NONE;
		okButton.addActionListener(this);
		pane.add(okButton, c);
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public newADTDialog(Component rel) {
    	//Create and set up the window.
        frame = new JDialog();
        frame.setLocationRelativeTo(rel);
        frame.setTitle("Create new ADT File");
        frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        // using this line make the Java VM crash at java 5 03 MAC .. o.O
        // static or nonstatic icon doesnt madder
        //frame.setIconImage(IconNew.getImage());
        frame.setModal(true);
        TitledBorder border = BorderFactory.createTitledBorder("New:");
        JPanel bla = new JPanel();
        bla.setBorder(border);
        
        //Set up the content pane.
        addComponentsToPane(bla/*frame.getContentPane()/**/);
        frame.setContentPane(bla);
        //Display the window.
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
    }

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==okButton) {
			// checkup
			if(in2.getText().length()!=2 || in3.getText().length()!=2) {
				System.err.println("Number have not 2 digits!");
				return;
			}
			try {
				x = Integer.valueOf( in2.getText() );
				y = Integer.valueOf( in3.getText() );
			} catch(Exception ex) {
				return;
			}
			
			
			xStr = in2.getText();
			yStr = in3.getText();
			name = in1.getText();
			
			frame.dispose();
			ok = true;
		}
	}
}
