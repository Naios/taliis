#	Taliis (c) 2007-2008 
#		by Melanie de Neygevisage	(Mae)
#			Tharo Herberg 		  	(ganku)
#		used libs are (c) by the contributors.

# Plugin Pack config File

pack_name = ADT 3D tools 
pack_info = Our 3D 'toolchain' .. .. :/
pack_autor = Tharo Herberg
pack_version = 1.0

plugin_count = 1

# the full classpath of the plugin
plugin_1 = de.taliis.plugins.adt.adt3DView

# lib files that need to get loaded from the
# taliis lib directory
libs_count = 2
lib_1 = libs/jogl.jar
lib_2 = libs/gluegen-rt.jar