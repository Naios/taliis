package de.taliis.dialogs;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;

import de.taliis.editor.configMananger;

/**
 * 'fancy' dialog that allows to setup the
 * location and order of mpq files in
 * the current system
 * 
 * @author ganku
 *
 */

public class mpqSources implements ActionListener {
	configMananger cm;
	DefaultListModel list;
	String lastDir = "";
	int initNumberOfFiles = 0;
	
	JDialog frame;
	JButton bUp, bDown, bAdd, bRem, bOK, bChancel;
	JList lFiles;
	JScrollPane spList;
	JCheckBox cbDisable;
	
	
	public static void main(String argv[]) {
		new mpqSources(null);
	}
	
	
	public mpqSources(configMananger cm) {
		this.cm = cm;
		TitledBorder border;
		
		// build frame
		frame = new JDialog();
		frame.setTitle("MPQ source files");
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		frame.setModal(true);
		
		// init components
		bUp = new JButton("/\\");
		 bUp.addActionListener(this);
		bDown = new JButton("\\/");
		 bDown.addActionListener(this);
		bAdd = new JButton("Add");
		 bAdd.addActionListener(this);
		bRem = new JButton("Remove");
		 bRem.addActionListener(this);
		bOK = new JButton("OK");
		 bOK.addActionListener(this);
		bChancel = new JButton("Chancel");
		 bChancel.addActionListener(this);
		
		border = BorderFactory.createTitledBorder("");
		// catch data
		list = new DefaultListModel();
		if(cm!=null) {
			int i = 0;
			String res;
			do {
				res = cm.getGlobalConfig()
							.getProperty("taliis_mpqLoader_srcFile_"+ (i++));
				if(res!=null) {
					list.add(i-1, res);
					initNumberOfFiles++;
				}
			} while(res!=null);
		}
		lFiles = new JList(list);
		spList = new JScrollPane(lFiles);
		spList.setBorder(border);
		
		// checkbox
		cbDisable = new JCheckBox("Disable file Loading");
		 cbDisable.addActionListener(this);
		if(cm!=null && cm.getGlobalConfig().getProperty("taliis_mpqLoader_dontload")!=null)
			cbDisable.setSelected(true);
		 
		 
		// build container
		JPanel stuff = new JPanel();
		border = BorderFactory.createTitledBorder("MPQ Locations:");
		addComponentsToPane(stuff);
		stuff.setBorder(border);
		
		frame.setContentPane(stuff);
		frame.pack();
		frame.setResizable(false);
        frame.setVisible(true);
	}
	
	/**
	 * Save the new file List to the global config
	 * file. Yes .. also the disable flag if needed
	 */
	private void save() {
		if(cm==null) return;
		
		// kill old data
		for(int i=0; i<initNumberOfFiles; i++)
			cm.getGlobalConfig().remove("taliis_mpqLoader_srcFile_"+i);
		
		// add new data
		for(int i=0; i<list.getSize(); i++) {
			cm.getGlobalConfig().setProperty(
					"taliis_mpqLoader_srcFile_" + i, 
					list.getElementAt(i).toString()
				);
		}
		
		// dont load?
		if(cbDisable.isSelected())
			cm.getGlobalConfig().setProperty("taliis_mpqLoader_dontload", "1");
		else cm.getGlobalConfig().remove("taliis_mpqLoader_dontload");
		
		// no dialogs at init anymore
		cm.getGlobalConfig().setProperty("taliis_mpqLoader_init", "1");
	}
	
	/**
	 * Add/Build our shit
	 * @param pane
	 */
	private void addComponentsToPane(Container main){
		// main comp
		GridBagConstraints c;
		JPanel n = new JPanel(new GridBagLayout());
		JPanel m = new JPanel(new GridBagLayout());
		JPanel s = new JPanel(new GridBagLayout());
		main.setLayout(new BorderLayout());
		
		// north comp
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0,10,5,10);
							// u, l, d, r
		String text = "<html><body>" + 
					"In order to use the full features of Taliis it is<br/>" + 
					"needed to register the location of your mpq files here." +
					"</body></html>";
		n.add(new JLabel(text), c);
		
		c.insets = new Insets(1,0,0,0);
		c.gridy = 2;
		n.add(new JSeparator(SwingConstants.HORIZONTAL),c);
		
		// mid comp
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 3;
		c.gridheight = 2;
		c.weightx = 0.9;
		c.weighty = 0.66;
		c.insets = new Insets(5,5,5,5);
		m.add(spList, c);
		
		c.gridx = 4;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.1;
		c.weighty = 0.33;
		c.insets = new Insets(0,5,0,5);
		m.add(bUp, c);
		
		c.gridy = 2;
		m.add(bDown, c);
		
		c.gridx = 1;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 2;
		c.weightx = 0.5;
		c.weighty = 0.33;
		m.add(bAdd, c);
		
		c.gridx = 3;
		m.add(bRem, c);

		// south comp
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		c.gridwidth = 4;
		c.weightx = 1;
		c.weighty = 0.33;
		c.insets = new Insets(5,0,5,0);
		s.add(new JSeparator(SwingConstants.HORIZONTAL),c);
		
		c.gridy = 2;
		s.add(cbDisable, c);
		
		c.gridx = 3;
		c.gridy = 3;
		c.gridwidth = 1;
		c.weightx = 0.25;
		c.insets = new Insets(5,5,5,5);
		s.add(bOK, c);
		
		c.gridx = 4;
		s.add(bChancel, c);
		
		// pack stuff
		main.add(n, BorderLayout.NORTH);
		main.add(m, BorderLayout.CENTER);
		main.add(s, BorderLayout.SOUTH);
	}



	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==bAdd) {
			JFileChooser fc = new JFileChooser();
			fc.setCurrentDirectory(new File(lastDir));
			fc.setMultiSelectionEnabled(true);
			// yes its unclean .. but whatever
			fc.setFileFilter(
					new FileFilter() {

						public boolean accept(File arg0) {
							if(arg0.isDirectory())return true;
							if(arg0.toString().toLowerCase().endsWith(".mpq")) return true;
							else return false;
						}

						public String getDescription() {
							return "WOW MPQ Archives";
						}
					}
				);
			
			int returnVal = fc.showOpenDialog(frame);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				for(File f : fc.getSelectedFiles()) {
					list.addElement(f.getAbsoluteFile());
				}
				
				lastDir = fc.getCurrentDirectory().getAbsolutePath();
			}
		}
		else if(e.getSource()==bOK) {
			save();
			frame.dispose();
		}
		else if(e.getSource()==bChancel) {
			frame.dispose();
		}
		else if(e.getSource()==bRem) {
			int index = lFiles.getSelectedIndex();
			if(index!=-1) {
				list.remove(index);
			}
		}
		else if(e.getSource()==bDown) {
			int index = lFiles.getSelectedIndex();
			if(index==-1 || (index+1) == list.getSize()) return;
			
			Object old = list.get(index);
			list.set(index, list.get(index+1));
			list.set(index+1, old);
			lFiles.setSelectedIndex(index+1);
		}
		else if(e.getSource()==bUp) {
			int index = lFiles.getSelectedIndex();
			if(index==-1 || (index-1) < 0) return;
			
			Object old = list.get(index);
			list.set(index, list.get(index-1));
			list.set(index-1, old);
			lFiles.setSelectedIndex(index-1);
		}
	}
}
