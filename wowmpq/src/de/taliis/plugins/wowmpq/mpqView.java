package de.taliis.plugins.wowmpq;


import java.awt.BorderLayout;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.Collator;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;

public class mpqView implements Plugin, PluginView {
	fileMananger fm;
	ImageIcon icon = null;
	
	public boolean checkDependencies() {
		return true;
	}
	public ImageIcon getIcon() {
		return icon;
	}
	public int getPluginType() {
		// TODO Auto-generated method stub
		return this.PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		return new String[] { "mpq" };
	}

	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/mpq.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}	
	}

	public void setConfigManangerRef(configMananger ref) {
		// we dont have a std editor? Cool! Not its me ^^
		String key = "taliis_storage_defaultView_mpq";
		if(!ref.getGlobalConfig().containsKey(key)) {
			ref.getGlobalConfig().setProperty(
					key, "de.taliis.plugins.wowmpq.mpqView"
				);
		}
	}

	public void setEventServer(eventServer arg0) {
		// TODO Auto-generated method stub
	}

	public void setFileManangerRef(fileMananger arg0) {
		// TODO Auto-generated method stub
		fm = arg0;
	}

	public void setMenuRef(JMenuBar arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setPluginPool(Vector<Plugin> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public JPanel createView() {
		openedFile of = fm.getActiveFile();
		if(of==null) return null;
		
		if(of.obj instanceof mpq) {
			mpq file = (mpq)of.obj;

			if(file.parsed==false)
				try {
					file.parseFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			
			// get datas
			List<String> names = (List)file.getHanlde().listFileNames();
			Collections.sort(names, Collator.getInstance());
			
			JList list = new JList(names.toArray());
			
			JPanel f = new JPanel(new BorderLayout());
			f.add(list, BorderLayout.CENTER);

			return f;
		}
		return null;
	}
	
	public String toString() {
		return "Show Files";
	}
	public void unload() {
		openedFile of = fm.getActiveFile();
		if(of==null) return;
		
		if(of.obj instanceof mpq) {
			mpq file = (mpq)of.obj;
			try {
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

