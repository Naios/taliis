package de.taliis.plugins.wdt;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import starlight.taliis.core.files.wdt;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;

public class wdtBasicView implements Plugin, PluginView, ActionListener {
	ImageIcon icon = null;
	fileMananger fm = null;
	JMenuItem mRemAll;
	JTable table;
	
	public boolean checkDependencies() { return true; }
	public ImageIcon getIcon() { return icon; }
	
	public int getPluginType() {
		return this.PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		return new String[]{ "wdt" }; 
	}

	public String[] neededDependencies() {
		return null;
	}

	public void setClassLoaderRef(URLClassLoader ref) { 
		try {
			URL u = ref.getResource("icons/shading.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}		
	}

	public void setConfigManangerRef(configMananger ref) {	
		// we dont have a std editor? Cool! Not its me ^^
		String key = "taliis_storage_defaultView_wdt";
		if(!ref.getGlobalConfig().containsKey(key)) {
			ref.getGlobalConfig().setProperty(
					key, "de.taliis.plugins.wdt.wdtBasicView"
				);
		}		
		
	}

	public void setFileManangerRef(fileMananger arg0) { 
		fm = arg0;	
	}
	
	public void setMenuRef(JMenuBar ref) {	
		mRemAll = new JMenuItem("Uncheck all");
		mRemAll.addActionListener(this);
		
		// get our sub menue
		for(int c=0; c<ref.getMenuCount(); c++) {
			JMenu menu = ref.getMenu(c);
			if(menu.getName().compareTo("edit_wdt")==0) {
				menu.setEnabled(true);
				menu.add(mRemAll, 0);
				return;
			}
		}
	}
	
	
	public void setEventServer(eventServer arg0) { }
	public void setPluginPool(Vector<Plugin> arg0) { }
	public void unload() { }


	public JPanel createView() {
		JPanel main = new JPanel();
		openedFile of = fm.getActiveFile(); 
		
		if(of.obj instanceof wdt) {
			main.setLayout(new BorderLayout());
			table = new JTable(new wdtTableModel( (wdt)of.obj ));
		
			// look and feel
			for(int c=0; c<65; c++) {
				TableColumn column = table.getColumnModel().getColumn(c);
				
				if(c==0) {
					column.setMaxWidth(30);
					continue;
				}
				
	    		//column.setCellRenderer(new wdtTableCellRenderer());
	    		column.setMaxWidth(20);
	    	}
			
			JScrollPane scrollPane = new JScrollPane(table);
	        scrollPane.addComponentListener(new
	                CorrectStrangeBehaviourListener(table, scrollPane));

	        main.add(scrollPane);
		}
		return main;
	}

	public String toString() {
		return "ADT Array";
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==mRemAll) {
			openedFile of = fm.getActiveFile();
			if(of==null) return;
			
			if(of.obj instanceof wdt) {
				wdt o = (wdt)of.obj;
				if(o.main==null) return;
				
				for(int i=0; i<64; i++) {
					for(int j=0; j<64; j++) {
						o.main.setValue(i, j, 0);
					}
				}				
			}
			table.updateUI();
		}
		
	}
}

/**
 * Table model for our Data ..
 * @author ganku
 */
class wdtTableModel extends DefaultTableModel {
	wdt obj;
	
	wdtTableModel(wdt o) {
		obj = o;
	}
	
    public int getColumnCount() {
        return 65;
    }

    public int getRowCount() {
       return 64;
    }
    
    public String getColumnName(int col) {
    	if(col==0) return "";
    	return (col-1) + "";
    }
    
    public Object getValueAt(int row, int col) {
    	if(col==0) return row;
    	if(obj.main!=null) {
    		if(obj.main.getValue(col-1, row)==1) return true;
    		else return false;
    	}
    	return null;
    }
    
    public Class getColumnClass(int col) {
    	if(col==0) return String.class;
    	return Boolean.class;
    }
    
    public boolean isCellEditable(int row, int col) {
    	if(col==0) return false;
    	else return true;
    }
    
    public void setValueAt(Object value, int row, int col) {
    	if(obj.main==null) return;
    	
    	boolean val = Boolean.parseBoolean(value.toString());
    	
    	if(val==true) obj.main.setValue(col-1, row, 1);
    	else obj.main.setValue(col-1, row, 0);
    }
}

/**
 * Shitti tables hates scrolling ...
 * @author ganku
 */
class CorrectStrangeBehaviourListener
	extends ComponentAdapter {
	
	private JTable table;
	
	private JScrollPane scrollPane;
	
	public CorrectStrangeBehaviourListener(JTable table,
	    JScrollPane scrollPane) {
	this.table = table;
	this.scrollPane = scrollPane;
	}
	
	public void componentResized(ComponentEvent e) {
	if (table.getPreferredSize().width
	        <= scrollPane.getViewport()
	               .getExtentSize().width) {
	    table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	} else {
	    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	}
	}
} 