package de.taliis.plugins.adt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URLClassLoader;
import java.nio.ByteBuffer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import starlight.taliis.core.chunks.adt.MCSH;
import starlight.taliis.core.files.adt;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;

/**
 * Gives ability do remove ANY shadow Infomation.
 * @author tharo
 *
 */

public class shadowDel implements Plugin, ActionListener {
	fileMananger fm;
	JMenuItem mRemAll, mRemNum;
	
	String dep[] = {
			"starlight.taliis.core.files.wowfile",
			"starlight.taliis.core.files.adt",
		};
	
	public boolean checkDependencies() {
		String now = "";
		try {
			for(String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch(Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}


	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return null;
	}


	public int getPluginType() {
		return PLUGIN_TYPE_FUNCTION;
	}


	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] {"adt"};
	}


	public String[] neededDependencies() {
		return dep;
	}


	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		
	}


	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}


	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}


	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}


	public void setMenuRef(JMenuBar ref) {
		mRemAll = new JMenuItem("Shadow - Remove All");
		 mRemAll.addActionListener(this);
		
		mRemNum = new JMenuItem("Shadow - Remove Number ..");
		 mRemNum.setEnabled(false);
		 mRemNum.addActionListener(this);
		
		// get our sub menue
		for(int c=0; c<ref.getMenuCount(); c++) {
			JMenu menu = ref.getMenu(c);
			if(menu.getName().compareTo("edit_adt")==0) {
				menu.setEnabled(true);
				menu.add(mRemNum, 0);
				menu.add(mRemAll, 0);
				return;
			}
		}
		System.err.println("No ADT Menu found o.O");
	}


	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}


	public void unload() {
		// TODO Auto-generated method stub
		
	}


	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==mRemAll) {
			openedFile of = fm.getActiveFile();
			if(of==null) return;
			
			if(of.obj instanceof adt) {
				for(int c=0; c<256; c++) {
					// get all shadow layers
					MCSH layer = ((adt)of.obj).mcnk[c].mcsh;

					// delete
					ByteBuffer b = layer.buff;
					b.position(layer.data);
					while(b.hasRemaining()) b.putInt(0xffffffff);
				}
				System.out.println("All Shadows got removed.");
			}
		}
	}
	
}
