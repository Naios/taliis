package de.taliis.plugins.adt;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import starlight.taliis.core.chunks.adt.MCLQ;
import starlight.taliis.core.files.adt;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;
import de.taliis.plugins.dialogs.waterHeightDialog;

/**
 * Displays adt's alphamap infomation as picture
 * 
 * @author tharo
 * 
 */
public class adtWater implements Plugin, PluginView, ActionListener {
	fileMananger fm;
	configMananger cm;
	ImageIcon icon = null;
	JMenu mWater;
	JMenuItem mImport, mExport,mAllWater;

	String dep[] = { 
			"starlight.taliis.core.files.wowfile", 
			"starlight.taliis.core.files.adt",
		};

	public boolean checkDependencies() {
		String now = "";
		try {
			for (String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch (Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}

	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}

	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "adt" };
	}

	public String[] neededDependencies() {
		return dep;
	}

	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/drink.png");
			icon = new ImageIcon(u);
		} catch (NullPointerException e) {
		}
	}

	public void setConfigManangerRef(configMananger ref) {
		cm = ref;
	}

	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub

	}

	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}

	public void setMenuRef(JMenuBar ref) {
		mWater = new JMenu("Water");
		mWater.setIcon(icon);

		mImport = new JMenuItem("Import from png ..");
		mImport.addActionListener(this);

		mExport = new JMenuItem("Export to png ..");
		mExport.addActionListener(this);
		
		mAllWater = new JMenuItem("AllWater");
		mAllWater.addActionListener(this);

		mWater.add(mImport);
		mWater.add(mExport);
		mWater.add(mAllWater);

		// get our sub menue
		for (int c = 0; c < ref.getMenuCount(); c++) {
			JMenu menu = ref.getMenu(c);
			if (menu.getName().compareTo("edit_adt") == 0) {
				menu.setEnabled(true);
				menu.add(mWater);
				return;
			}
		}
		System.err.println("No ADT Menu found o.O");
	}

	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub

	}

	public void unload() {
		// TODO Auto-generated method stub
	}

	public JPanel createView() {
		// get file
		openedFile of = fm.getActiveFile();

		if (of == null || !(of.obj instanceof adt))
			return null;
		adt obj = (adt) of.obj;
		// create buffered image

		BufferedImage img = new BufferedImage(144, 144, BufferedImage.TYPE_BYTE_GRAY);
		for(int x=0; x<16; x++) {
			for(int y=0; y<16; y++) {
					
				// preload data
				int data[] = new int[9*9];
				
				if(obj.mcnk[x*16+y].mclq.isempty!=true)
					for(int c=0; c<9*9; c++) {
						
					
						data[c]  = -1*100000*(obj.mcnk[x*16+y].mclq.getHeight(c));
						
					
						
										
				}
					
					
					for(int c=0; c<9; c++)
						
						img.setRGB(y*9,				//int startX,
					                  x*9+c,		//int startY,
					                  9,			//int w,
					                  1,			//int h,
					                  data,			//int[] rgbArray,
					                  c*9,			//int offset,
					                  9);			//int scansize
					
					}
				}
		// create panel, display
		Icon icon = new ImageIcon(img);
		JLabel label = new JLabel(icon);
		label.setMinimumSize(new Dimension(144, 144));

		JPanel f = new JPanel();
		f.add(label);

		JScrollPane scr = new JScrollPane(f, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scr.setPreferredSize(new Dimension(144, 144));

		JPanel r = new JPanel();
		r.setLayout(new BorderLayout());
		r.add(scr, BorderLayout.CENTER);

		return r;

	}


	private void Import() {
		// get file
		openedFile of = fm.getActiveFile();

		if (of == null || !(of.obj instanceof adt))
			return;

		// get data
		adt obj = (adt) of.obj;
		BufferedImage img = new BufferedImage(144, 144, BufferedImage.TYPE_BYTE_GRAY);

    		String filename = of.f.getAbsolutePath();
    		File f = new File(filename + ".png");
    		try {
				img=ImageIO.read(f);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    		for(int x=0; x<16; x++) {
					for(int y=0; y<16; y++) {
							
						// preload data
						int data[] = new int[9*9];
						
						for(int c=0; c<9; c++)
						img.getRGB(y*9,				//int startX,
						                  x*9+c,		//int startY,
						                  9,			//int w,
						                  1,			//int h,
						                  data,			//int[] rgbArray,
						                  c*9,			//int offset,
						                  9);			//int scansize
						
								float divisor=(float) 100000;
								if(obj.mcnk[x*16+y].mclq.isempty!=true){
									obj.mcnk[x*16+y].mclq.altitude=data[0];
									obj.mcnk[x*16+y].mclq.baseheight=data[0];
									for(int c=0; c<9*9; c++) {
										if(data[c]<obj.mcnk[x*16+y].mclq.altitude)
											obj.mcnk[x*16+y].mclq.altitude=data[c];
										if(data[c]>obj.mcnk[x*16+y].mclq.baseheight)
											obj.mcnk[x*16+y].mclq.baseheight=data[c];
									}
									obj.mcnk[x*16+y].mclq.altitude=-1*((obj.mcnk[x*16+y].mclq.altitude/divisor));
									obj.mcnk[x*16+y].mclq.baseheight=-1*(((obj.mcnk[x*16+y].mclq.baseheight)/divisor));
								for(int c=0; c<9*9; c++) {
									obj.mcnk[x*16+y].mclq.setHeight(c, ((-1)*(data[c]/(divisor))));
									
								}}
								else
								{
									float check=0;
									for(int c=0; c<9*9; c++) {
										check+=data[c];
									}
									System.out.println((((check/81)/(divisor))));
									if(check!=0){
									obj.mcnk[x*16+y].mclq=new MCLQ();
									obj.mcnk[x*16+y].setFlags(5);
									obj.mcnk[x*16+y].mclq.altitude=data[0];
									obj.mcnk[x*16+y].mclq.baseheight=data[0];
									for(int c=0; c<9*9; c++) {
										if(data[c]<obj.mcnk[x*16+y].mclq.altitude)
											obj.mcnk[x*16+y].mclq.altitude=data[c];
										if(data[c]>obj.mcnk[x*16+y].mclq.baseheight)
											obj.mcnk[x*16+y].mclq.baseheight=data[c];
									}
									obj.mcnk[x*16+y].mclq.altitude=-1*((obj.mcnk[x*16+y].mclq.altitude/divisor));
									obj.mcnk[x*16+y].mclq.baseheight=-1*(((obj.mcnk[x*16+y].mclq.baseheight)/divisor));
								for(int c=0; c<9*9; c++) {
									obj.mcnk[x*16+y].mclq.setHeight(c, ((-1)*(data[c]/(divisor))));
									
								}
								
							}
								}}
							}
						
    		System.out.println("Importet Water");

		}	


	/**
	 * Exports our layers as RGB image. Same directory as the ADT file have itself.
	 */
	private void Export() {
		// get file
		openedFile of = fm.getActiveFile();
		
		if(of==null || !(of.obj instanceof adt)) return;
		
		// get data
		adt obj = (adt)of.obj;
		
		// create buffered image
		BufferedImage img = new BufferedImage(144, 144, BufferedImage.TYPE_BYTE_GRAY);


    		for(int x=0; x<16; x++) {
					for(int y=0; y<16; y++) {
						
						// preload data
						int data[] = new int[9*9];
						
						
						if(obj.mcnk[x*16+y].mclq.isempty!=true)
								for(int c=0; c<9*9; c++) {

								data[c] = -1*100000*(obj.mcnk[x*16+y].mclq.getHeight(c));
												
							}
							

							
							
							
							
							for(int c=0; c<9; c++)

								img.setRGB(y*9,				//int startX,
							                  x*9+c,		//int startY,
							                  9,			//int w,
							                  1,			//int h,
							                  data,			//int[] rgbArray,
							                  c*9,			//int offset,
							                  9);			//int scansize
							
							}
						}
    		

    			
    	
		String filename = of.f.getAbsolutePath();
		
		File file = new File(filename + ".png");

        try {
			ImageIO.write(img, "png", file);
		} catch (IOException e) {
			e.printStackTrace();
		}
					
		System.out.println("Alpha layer successfull exported to: " + ".png");
					
	}
	
	private void AllWater(){
		// get file
		openedFile of = fm.getActiveFile();
		
		if(of==null || !(of.obj instanceof adt)) return;
		
		// get data
		adt obj = (adt)of.obj;
		
		waterHeightDialog a=new waterHeightDialog(null);
		float height=a.heightval;
		for(int i=0;i<256;i++){
			if(!obj.mcnk[i].mclq.isempty){
				obj.mcnk[i].mclq.altitude=height;
				obj.mcnk[i].mclq.baseheight=height;
				for(int k=0;k<9*9;k++)
				obj.mcnk[i].mclq.setHeight(k, height);
			}
			else{
				obj.mcnk[i].mclq=new MCLQ();
				obj.mcnk[i].setFlags(5);
				obj.mcnk[i].mclq.altitude=height;
				obj.mcnk[i].mclq.baseheight=height;
				for(int k=0;k<9*9;k++)
				obj.mcnk[i].mclq.setHeight(k, height);
			}
		}
	}

	public String toString() {
		return "Water Heightmap";
	}

	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == mImport)
			Import();
		else
			if (arg0.getSource() == mExport)
				Export();
		else
			if (arg0.getSource() == mAllWater)
				AllWater();
	}
}
