package de.taliis.plugins.adt;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import starlight.taliis.core.chunks.adt.MCAL_Entry;
import starlight.taliis.core.chunks.adt.MCSH;
import starlight.taliis.core.files.adt;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;

/**
 * Displays adt's shadow infomation as picture
 * 
 * @author tharo
 * 
 */

public class shadowView implements Plugin, PluginView, ActionListener {
	fileMananger fm;
	ImageIcon icon = null;
	JMenu mShadow;
	JMenuItem mImport, mExport;
	String dep[] = { "starlight.taliis.core.files.wowfile", "starlight.taliis.core.files.adt", };

	public boolean checkDependencies() {
		String now = "";
		try {
			for (String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch (Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}

	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}

	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "adt" };
	}

	public String[] neededDependencies() {
		return dep;
	}

	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/contrast.png");
			icon = new ImageIcon(u);
		} catch (NullPointerException e) {
		}
	}

	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub

	}

	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub

	}

	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}

	public void setMenuRef(JMenuBar ref) {
		mShadow = new JMenu("Alpha Layer");
		mShadow.setIcon(icon);

		mImport = new JMenuItem("Import from png ..");
		mImport.addActionListener(this);

		mExport = new JMenuItem("Export to png ..");
		mExport.addActionListener(this);

		mShadow.add(mImport);
		mShadow.add(mExport);

		// get our sub menue
		for (int c = 0; c < ref.getMenuCount(); c++) {
			JMenu menu = ref.getMenu(c);
			if (menu.getName().compareTo("edit_adt") == 0) {
				menu.setEnabled(true);
				menu.add(mShadow);
				return;
			}
		}
		System.err.println("No ADT Menu found o.O");
	}

	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub

	}

	public void unload() {
		// TODO Auto-generated method stub
	}
	
	private void Import() {
		// get file
		openedFile of = fm.getActiveFile();

		if (of == null || !(of.obj instanceof adt))
			return;

		// get data
		adt obj = (adt) of.obj;
		BufferedImage img = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
    		String filename = of.f.getAbsolutePath();
    		File f = new File(filename +"_Shadow_"+ ".png");
    		try {
				img=ImageIO.read(f);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    		for(int x=0; x<16; x++) {
					for(int y=0; y<16; y++) {
						/*if (obj.mcnk[16 * y + x].mcsh == null) {
							System.out.println("No MCSH at " + x + "/" + y);
							System.out.println("Creating...");
						}*/
						int data[] = new int[4096];
						
						for(int c=0; c<64; c++)
						img.getRGB(x*64,				//int startX,
						                  y*64+c,		//int startY,
						                  64,			//int w,
						                  1,			//int h,
						                  data,			//int[] rgbArray,
						                  c*64,			//int offset,
						                  64);			//int scansize
						obj.mcnk[y*16+x].mcsh=new MCSH();
						obj.mcnk[y*16+x].mcsh.setData(data);
					}
			}
    		
			
	}

	/**
	 * Exports our layers as RGB image. Same directory as the ADT file have itself.
	 */
	private void Export() {
		// get file
		openedFile of = fm.getActiveFile();
		
		if(of==null || !(of.obj instanceof adt)) return;
		
		// get data
		adt obj = (adt)of.obj;
		
		// create buffered image
		BufferedImage img = new BufferedImage(16 * 64, 16 * 64, BufferedImage.TYPE_BYTE_BINARY);
		for (int x = 0; x < 16; x++)
			for (int y = 0; y < 16; y++) {
				if (obj.mcnk[16 * y + x].mcsh == null) {
					System.err.println("No MCSH at " + x + "/" + y);
					continue;
				}
				// preload data
				int data[] = obj.mcnk[16 * y + x].mcsh.getData();

				// confert to RGB BW picture ..
				for (int c = 0; c < data.length; c++)
					if (data[c] != 0)
						data[c] = 0xFFFFFF;

				// set pixels
				for (int c = 0; c < 64; c++)
					img.setRGB(x * 64, // int startX,
					y * 64 + c,// int startY,
					64,// int w,
					1,// int h,
					data,// int[] rgbArray,
					c * 64,// int offset,
					64);// int scansize)

			}
    	
		String filename = of.f.getAbsolutePath();
		
		File file = new File(filename +"_Shadow_"+ ".png");

        try {
			ImageIO.write(img, "png", file);
		} catch (IOException e) {
			e.printStackTrace();
		}
					
		System.out.println("Shadow layer successfull exported to: " + ".png");
					
	}


	public JPanel createView() {
		// get file
		openedFile of = fm.getActiveFile();

		if (of == null || !(of.obj instanceof adt))
			return null;

		// get data
		adt obj = (adt) of.obj;

		// create buffered image
		BufferedImage img = new BufferedImage(16 * 64, 16 * 64, BufferedImage.TYPE_BYTE_BINARY);
		for (int x = 0; x < 16; x++)
			for (int y = 0; y < 16; y++) {
				if (obj.mcnk[16 * y + x].mcsh == null) {
					System.err.println("No MCSH at " + x + "/" + y);
					continue;
				}
				// preload data
				int data[] = obj.mcnk[16 * y + x].mcsh.getData();

				// confert to RGB BW picture ..
				for (int c = 0; c < data.length; c++)
					if (data[c] != 0)
						data[c] = 0xFFFFFF;

				// set pixels
				for (int c = 0; c < 64; c++)
					img.setRGB(x * 64, // int startX,
					y * 64 + c,// int startY,
					64,// int w,
					1,// int h,
					data,// int[] rgbArray,
					c * 64,// int offset,
					64);// int scansize)

			}

		// create panel, display
		Icon icon = new ImageIcon(img);
		JLabel label = new JLabel(icon);
		label.setMinimumSize(new Dimension(16 * 64, 16 * 64));

		JPanel f = new JPanel();
		f.add(label);

		JScrollPane scr = new JScrollPane(f, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scr.setPreferredSize(new Dimension(16 * 64, 16 * 64));

		JPanel r = new JPanel();
		r.setLayout(new BorderLayout());
		r.add(scr, BorderLayout.CENTER);

		return r;
	}

	@Override
	public String toString() {
		return "Shadows";
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.getSource() == mImport)
			Import();
		else
			if (arg0.getSource() == mExport)
				Export();
	}
}
