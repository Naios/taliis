package de.taliis.plugins.adt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.MenuElement;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.SubChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;
import starlight.taliis.core.chunks.adt.MCLQ;
import starlight.taliis.core.chunks.adt.MCLY;
import starlight.taliis.core.chunks.adt.MCRF;
import starlight.taliis.core.chunks.adt.MCSH;
import starlight.taliis.core.chunks.adt.MDDF;
import starlight.taliis.core.chunks.adt.MMDX;
import starlight.taliis.core.chunks.adt.MMID;
import starlight.taliis.core.chunks.adt.MODF;
import starlight.taliis.core.chunks.adt.MWID;
import starlight.taliis.core.chunks.adt.MWMO;
import starlight.taliis.core.files.adt;
import starlight.taliis.helpers.adtChecker;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;
import de.taliis.plugins.dialogs.AreaIdDialog;
import de.taliis.plugins.dialogs.UniqueModifierDialog;

public class adtReplaceTools implements Plugin, ActionListener {
	fileMananger fm;
	ImageIcon iconRep, iconWater, iconShadow,
	iconGo, iconAlpha, iconDoodads,
	iconArea,iconUnique, iconFixLines;
	
	// our Menu
	JMenu mReplace;
	JMenuItem miKillWater, miKillShadows, miInject
	, miInjectAlpha, miInjectDoodads, miMassAreaID,
	miUniqueID,miFixLines;
	JMenu menu;
		
	public boolean checkDependencies() { return true; }
	public ImageIcon getIcon() { return iconRep; }
	public int getPluginType() { return Plugin.PLUGIN_TYPE_FUNCTION; }
	public String[] getSupportedDataTypes() { return new String[] { "adt" }; }
	public String[] neededDependencies() { return null; }

	
// load our icon
	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/photos.png");
			iconRep = new ImageIcon( u );
			
			u = ref.getResource("icons/contrast.png");
			iconShadow = new ImageIcon( u );
			
			u = ref.getResource("icons/drink.png");
			iconWater = new ImageIcon( u );
			
			u = ref.getResource("icons/package_go.png");
			iconGo = new ImageIcon( u );
			
			u = ref.getResource("icons/color_wheel.png");
			iconAlpha = new ImageIcon( u );
			
			u = ref.getResource("icons/table.png");
			iconDoodads = new ImageIcon( u );
			
			u = ref.getResource("icons/area.png");
			iconArea = new ImageIcon( u );
			
			u = ref.getResource("icons/table_relationship.png");
			iconUnique = new ImageIcon( u );
			
			u = ref.getResource("icons/world_fix.png");
			iconFixLines = new ImageIcon( u );
			

		} catch(NullPointerException e) {}
	}

	public void setConfigManangerRef(configMananger arg0) { }
	public void setEventServer(eventServer arg0) { }
	public void setFileManangerRef(fileMananger arg0) { fm = arg0; }
	public void setPluginPool(Vector<Plugin> arg0) { }
	public void unload() { }

// setup menue
	public void setMenuRef(JMenuBar arg0) {
		// cast out edit menue
		for(MenuElement men : arg0.getSubElements()) {
			if(men instanceof JMenu) {
				if(((JMenu)men).getName().contains("edit_adt")) {
					menu = (JMenu)men;
					menu.setEnabled(true);
					break;
				}
			}
		}
		
		// insert our entry
		mReplace = new JMenu("Replacement tools");
		 mReplace.setIcon(iconRep);
		
		 miKillWater = new JMenuItem("Kill all water");
		  miKillWater.setIcon(iconWater);
		  miKillWater.addActionListener(this);
		 miKillShadows = new JMenuItem("Kill all shadows");
		  miKillShadows.setIcon(iconShadow);
		  miKillShadows.addActionListener(this);
		 miInject = new JMenuItem("Inject Heightmap ..");
		  miInject.setIcon(iconGo);
		  miInject.addActionListener(this);
		 miInjectAlpha = new JMenuItem("Inject Alphamap ..");
		  miInjectAlpha.setIcon(iconAlpha);
		  miInjectAlpha.addActionListener(this);
		 miInjectDoodads = new JMenuItem("Inject Doodads ..");
		  miInjectDoodads.setIcon(iconDoodads);
		  miInjectDoodads.addActionListener(this);
		miMassAreaID = new JMenuItem("Set AreaIDs");
		miMassAreaID.setIcon(iconArea);
		miMassAreaID.addActionListener(this);
		miUniqueID = new JMenuItem("Set UniqueIDs");
		miUniqueID.setIcon(iconUnique);
		miUniqueID.addActionListener(this);
		miFixLines = new JMenuItem("Fix Lines");
		miFixLines.setIcon(iconFixLines);
		miFixLines.addActionListener(this);


		  
		menu.add(mReplace);
		mReplace.add(miKillWater);
		mReplace.add(miKillShadows);
		mReplace.add(miInject);
		mReplace.add(miInjectAlpha);
		mReplace.add(miInjectDoodads);
		mReplace.add(miMassAreaID);
		mReplace.add(miUniqueID);
		mReplace.add(miFixLines);
	}

// action listener
	public void actionPerformed(ActionEvent e) {
		adt obj;
		openedFile os = fm.getActiveFile();

		if(os.obj instanceof adt) {
			obj = (adt)os.obj;	
		}
		else return;
	
		// kill all waters
		if(e.getSource()==miKillWater) {
			if(obj.getVersion()==adt.ADT_VERSION_NORMAL
			   || obj.getVersion()==adt.ADT_VERSION_EXPANSION_TBC) {
				
				for(int c=0; c<256; c++) {
					obj.mcnk[c].mclq = new MCLQ();
					obj.mcnk[c].setFlags(obj.mcnk[c].getFlags() & 0x3);
				}
			}	
			else {
				System.err.println("ADT Version is not suported jet.");
			}
		}
		// kill all shadows
		else if(e.getSource()==miKillShadows) {
			if(obj.getVersion()==adt.ADT_VERSION_NORMAL
			   || obj.getVersion()==adt.ADT_VERSION_EXPANSION_TBC) {
					
				for(int c=0; c<256; c++) {
					obj.mcnk[c].mcsh= new MCSH();
				}
			}	
			else {
				System.err.println("ADT Version is not suported jet.");
			}
		}
		
		
		else if(e.getSource()==miInjectAlpha) {
			Vector<openedFile> choices = fm.getFileList();
			openedFile result = (openedFile) JOptionPane.showInputDialog(
				miInjectAlpha.getComponent(),
                "Chose a Source File:\n",
                "Inject",
                JOptionPane.PLAIN_MESSAGE,
                null,
                choices.toArray(),
                null);
			
			if(result==null) return;
			if(result.obj instanceof adt) {
				adt injObj = (adt) result.obj;
				obj.mtex=injObj.mtex;
				for(int c=0; c<256; c++) {
					obj.mcnk[c].setNLayers(injObj.mcnk[c].getNLayers());
					obj.mcnk[c].mcal=injObj.mcnk[c].mcal;
					obj.mcnk[c].mcly=injObj.mcnk[c].mcly;
				}		
			}
		}
		
		
		else if(e.getSource()==miInjectDoodads) {
			Vector<openedFile> choices = fm.getFileList();
			openedFile result = (openedFile) JOptionPane.showInputDialog(
				miInject.getComponent(),
                "Chose a Source File:\n",
                "Inject",
                JOptionPane.PLAIN_MESSAGE,
                null,
                choices.toArray(),
                null);
			
			if(result==null) return;
			float xpos,ypos,oldy,oldx;
			//get new position
			ypos=533.33f*Integer.parseInt(os.f.getAbsolutePath().substring(os.f.getAbsolutePath().length()-6, os.f.getAbsolutePath().length()-4));
			xpos=533.33f*Integer.parseInt(os.f.getAbsolutePath().substring(os.f.getAbsolutePath().length()-9, os.f.getAbsolutePath().length()-7));
			//get old position
			oldy=533.33f*Integer.parseInt(result.f.getAbsolutePath().substring(result.f.getAbsolutePath().length()-6, result.f.getAbsolutePath().length()-4));
			oldx=533.33f*Integer.parseInt(result.f.getAbsolutePath().substring(result.f.getAbsolutePath().length()-9, result.f.getAbsolutePath().length()-7));			
			
			
			if(result.obj instanceof adt) {
				adt injObj = (adt) result.obj;

				injObj.mddf.render();
				injObj.modf.render();
				injObj.mwmo.render();
				injObj.mmdx.render();
				injObj.mmid.render();
				injObj.mwid.render();
				try {
					MDDF mddf=new MDDF(injObj.mddf.buff);
					MODF modf=new MODF(injObj.modf.buff);
					MWMO mwmo=new MWMO(injObj.mwmo.buff);
					MMDX mmdx=new MMDX(injObj.mmdx.buff);
					MMID mmid=new MMID(injObj.mmid.buff);
					MWID mwid=new MWID(injObj.mwid.buff);
					{
					obj.mddf=mddf;
					obj.modf=modf;
					obj.mwmo=mwmo;
					obj.mmdx=mmdx;
					obj.mmid=mmid;
					obj.mwid=mwid;
					}
						
						for(int i=0;i<obj.mddf.entrys.length;i++)
						{
							float newval;
							newval=(obj.mddf.entrys[i].translate(obj.mddf.entrys[i].getY())-(xpos-oldx));
							obj.mddf.entrys[i].setY(obj.mddf.entrys[i].translate(newval));
							newval=(obj.mddf.entrys[i].translate(obj.mddf.entrys[i].getX())-(ypos-oldy));
							obj.mddf.entrys[i].setX(obj.mddf.entrys[i].translate(newval));
						}
						
						for(int i=0;i<obj.modf.entrys.length;i++)
						{
							float newval;
							newval=(obj.modf.entrys[i].translate(obj.modf.entrys[i].getY())-(xpos-oldx));
							obj.modf.entrys[i].setY(obj.modf.entrys[i].translate(newval));
							newval=(obj.modf.entrys[i].translate(obj.modf.entrys[i].getX())-(ypos-oldy));
							obj.modf.entrys[i].setX(obj.modf.entrys[i].translate(newval));
						}
						
					
				} catch (ChunkNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				for(int c=0; c<256; c++) {
					obj.mcnk[c].setNDoodad(injObj.mcnk[c].getNDoodad());
					injObj.mcnk[c].mcrf.render();
					try{
						MCRF mcrf=new MCRF(injObj.mcnk[c].mcrf.buff);
						obj.mcnk[c].mcrf=mcrf;
					}
					 catch (ChunkNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		}
		
		// inject heightmap
		else if(e.getSource()==miInject) {
			Vector<openedFile> choices = fm.getFileList();
			openedFile result = (openedFile) JOptionPane.showInputDialog(
				miInject.getComponent(),
                "Chose a Source File:\n",
                "Inject",
                JOptionPane.PLAIN_MESSAGE,
                null,
                choices.toArray(),
                null);
			
			if(result==null) return;
			if(result.obj instanceof adt) {
				adt injObj = (adt) result.obj;
				for(int c=0; c<256; c++) {
					obj.mcnk[c].mcvt = injObj.mcnk[c].mcvt;
					obj.mcnk[c].setHeightOffs( 
						injObj.mcnk[c].getHeightOffs()
					);
					//obj.mcnk[c].setIndexX( injObj.mcnk[c].getIndexX() );
					//obj.mcnk[c].setIndexY( injObj.mcnk[c].getIndexY() );
					//obj.mcnk[c].setPosX( injObj.mcnk[c].getPosX() );
					//obj.mcnk[c].setPosY( injObj.mcnk[c].getPosY() );
					obj.mcnk[c].setPosZ( injObj.mcnk[c].getPosZ() );
				}
			}
		}
		
		else if(e.getSource()==miMassAreaID){
			AreaIdDialog a=new AreaIdDialog(null);
			int startc=a.startc;
			int endc=a.endc+1;
			int id=a.areaid;
			for(int i=startc;i<endc;i++){
				obj.mcnk[i].setAreaID(id);
			}
		}
		else if(e.getSource()==miUniqueID){
			UniqueModifierDialog a=new UniqueModifierDialog(null);
			int id=a.uniqueid;
			if(a.all.isSelected()){
				for(int i=0;i<obj.mddf.getLenght();i++){
					obj.mddf.entrys[i].setUniqID(id);
					id++;
				}
				for(int i=0;i<obj.modf.entrys.length;i++){
					obj.modf.entrys[i].setUniqID(id);
					id++;
				}
			}
			else if(a.models.isSelected()){
				for(int i=0;i<obj.mddf.getLenght();i++){
					obj.mddf.entrys[i].setUniqID(id);
					id++;
				}
			}
			else if(a.wmos.isSelected()){
				for(int i=0;i<obj.modf.entrys.length;i++){
					obj.modf.entrys[i].setUniqID(id);
					id++;
				}
			}
		}
		
		else if(e.getSource()==miFixLines){


			for(int x=0;x<16;x++){
			      for(int y=0;y<16;y++){
			        if(x<15){
			          float val1=0f;
			          float val2=0f;
			          float newval=0f;
			          for(int n=0;n<9;n++){
			            val1=obj.mcnk[x*16+y].mcvt.getValNoLOD(n, 8)+obj.mcnk[x*16+y].getPosZ();
			            val2=obj.mcnk[(x+1)*16+y].mcvt.getValNoLOD(n, 0)+obj.mcnk[(x+1)*16+y].getPosZ();
			            newval=(val1+val2)/2;
			            System.out.println("Old vals: "+val1+" "+val2+" New Val: "+newval);
			            obj.mcnk[x*16+y].mcvt.setValNoLOD(n, 8,newval-obj.mcnk[x*16+y].getPosZ());
			            obj.mcnk[(x+1)*16+y].mcvt.setValNoLOD(n, 0,newval-obj.mcnk[(x+1)*16+y].getPosZ());
			          }
			          for(int n=0;n<8;n++){
			            val1=obj.mcnk[x*16+y].mcvt.getValLOD(n, 7)+obj.mcnk[x*16+y].getPosZ();
			            val2=obj.mcnk[(x+1)*16+y].mcvt.getValLOD(n, 0)+obj.mcnk[(x+1)*16+y].getPosZ();
			            newval=val1;
			            obj.mcnk[x*16+y].mcvt.setValLOD(n, 7,newval-obj.mcnk[x*16+y].getPosZ());
			            obj.mcnk[(x+1)*16+y].mcvt.setValLOD(n, 0,newval-obj.mcnk[(x+1)*16+y].getPosZ());
			          }
			          if(y<15)
			          for(int n=0;n<9;n++){
			            val1=obj.mcnk[x*16+y].mcvt.getValNoLOD(8, n)+obj.mcnk[x*16+y].getPosZ();
			            val2=obj.mcnk[x*16+(y+1)].mcvt.getValNoLOD(0, n)+obj.mcnk[x*16+y+1].getPosZ();
			            newval=val1;
			            obj.mcnk[x*16+y].mcvt.setValNoLOD(8, n,newval-obj.mcnk[x*16+y].getPosZ());
			            obj.mcnk[x*16+(y+1)].mcvt.setValNoLOD(0, n,newval-obj.mcnk[x*16+y+1].getPosZ());
			          }
			          for(int n=0;n<8;n++){
			            val1=obj.mcnk[x*16+y].mcvt.getValLOD(7, n)+obj.mcnk[x*16+y].getPosZ();
			            val2=obj.mcnk[x*16+(y+1)].mcvt.getValLOD(0, n)+obj.mcnk[x*16+y+1].getPosZ();
			            newval=val1;
			            obj.mcnk[x*16+y].mcvt.setValLOD(7, n,newval-obj.mcnk[x*16+y].getPosZ());
			            obj.mcnk[x*16+(y+1)].mcvt.setValLOD(0, n,newval-obj.mcnk[x*16+y+1].getPosZ());
			          }
			        }
			        else if(y<15){
			          float val1=0f;
			          float val2=0f;
			          float newval=0f;
			          for(int n=0;n<9;n++){
			            val1=obj.mcnk[x*16+y].mcvt.getValNoLOD(8, n)+obj.mcnk[x*16+y].getPosZ();
			            val2=obj.mcnk[x*16+(y+1)].mcvt.getValNoLOD(0, n)+obj.mcnk[x*16+y+1].getPosZ();
			            newval=(val1+val2)/2;
			            obj.mcnk[x*16+y].mcvt.setValNoLOD(8, n,newval-obj.mcnk[x*16+y].getPosZ());
			            obj.mcnk[x*16+(y+1)].mcvt.setValNoLOD(0, n,newval-obj.mcnk[x*16+y+1].getPosZ());
			          }
			          for(int n=0;n<8;n++){
			            val1=obj.mcnk[x*16+y].mcvt.getValLOD(7, n)+obj.mcnk[x*16+y].getPosZ();
			            val2=obj.mcnk[x*16+(y+1)].mcvt.getValLOD(0, n)+obj.mcnk[x*16+y+1].getPosZ();
			            newval=(val1+val2)/2;
			            obj.mcnk[x*16+y].mcvt.setValLOD(7, n,newval-obj.mcnk[x*16+y].getPosZ());
			            obj.mcnk[x*16+(y+1)].mcvt.setValLOD(0, n,newval-obj.mcnk[x*16+y+1].getPosZ());
			          }
			        }
			      }
			      }
		}
		
		}
	}

