package de.taliis.plugins.adt;

//so, also ich bin da :P
import java.io.File;
import java.io.InvalidClassException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.adtChecker;
import starlight.taliis.helpers.adtCoordHelper;
import starlight.taliis.helpers.adtObjHelper;
import starlight.taliis.helpers.fileLoader;
import de.taliis.plugins.dialogs.newADTDialog;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.eventServer;

public class adtStorage implements Plugin, PluginStorage {
	fileMananger fm;
	ImageIcon icon;
	eventServer es;
	
	public void setEventServer(eventServer ref) { 
		es = ref;
	}
	
		
	public boolean checkDependencies() {
		return true;
	}

	public int getPluginType() {
		return PLUGIN_TYPE_STORAGE;
	}
	
	
	public String[] getSupportedDataTypes() {
		return new String[]{"adt"};
	}

	
	public String[] neededDependencies() {
		return null;
	}

	
	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/package.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}
	}

	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}
	
	public wowfile load(File f) throws ChunkNotFoundException {
		adt tmp = null;


			try {
				tmp=new adt( fileLoader.openBuffer(f.getAbsolutePath()));
			} catch (InvalidClassException e) {

			}

			return tmp;

	}

	public wowfile create() {
		newADTDialog n = new newADTDialog( null );
		if(n.ok==true) {
			wowfile tmp;
			
			switch(n.version) {
				case adt.ADT_VERSION_NORMAL: 
				case adt.ADT_VERSION_EXPANSION_TBC: 
				case adt.ADT_VERSION_EXPANSION_WOTLK: 
					tmp = new adt(n.version);
				   break;
				   
				default: 
					return null;
			}
	
			adtCoordHelper chelp = new adtCoordHelper( (adt)tmp );
			chelp.calcCoordinates(n.y, n.x, 0, 0);
			
			// register by ourselfes
			fm.registerObject(
				tmp, 
				new File(n.name + "_" + n.xStr + "_" + n.yStr + ".adt")
			);
			es.updateTable();
		}
		
		return null;
	}

	public int save(openedFile f) {
		//TODO: catch failures and so on
		if(f.obj instanceof wowfile) {
			wowfile o = (wowfile)f.obj;
			
			// re-render object appears
			adtObjHelper objh = new adtObjHelper((adt)o);
			objh.generateAppeareances();
			
			o.render();
			
			// check
			adtChecker check = new adtChecker((adt)o);
		/*	if(*/check.check();/*==0) {*/
				fileLoader.saveBuffer(o.buff , f.f.getAbsolutePath());
				return 1;
		//	} else return -2;
		}
		return -1;
	}
	
	public ImageIcon getIcon() {
		return icon;
	}

	
	public FileFilter getFiter() {
		return new FileFilter() {
			public boolean accept(File f) {
				if (f.isDirectory()) {
		            return true;
		        }
				
				String name = f.getName().toLowerCase();
				
				if(name.endsWith(".adt")) return true;
				else return false;
          }

			
			public String getDescription() {
				return "WoW ADT Map Files";
			}
      };
	}
	
	public void setConfigManangerRef(configMananger ref) { }
	public void setMenuRef(JMenuBar ref) {	}
	public void setPluginPool(Vector<Plugin> ref) { }

	public void unload() {}
}
