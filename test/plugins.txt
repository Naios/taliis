#	Taliis (c) 2007-2008 
#		by Melanie de Neygevisage	(Mae)
#			Tharo Herberg 		  	(ganku)
#		used libs are (c) by the contributors.

# Plugin Pack config File
	###########################################################
	# This config file is needed for each plugin package and 
	# descripes the plugin entry classes.
	# Also some Infomations about the plugins can be provided
	###########################################################

pack_name = Sample Plugin Package
pack_info = This plugin Pack was made to show how to use the \
			plugin interface and create a valid config file.
pack_autor = Tharo Herberg
pack_version = 0.1

plugin_count = 3

# the full classpath of the plugin
plugin_1 = de.taliis.test.plugin.TestPlugin
plugin_2 = de.taliis.test.plugin.TestStorage
plugin_3 = de.taliis.test.plugin.TestView

plugin_1_autor = Tharo Herberg						# optional
plugin_1_version = 0.1								# optional
plugin_1_info = Stupid sample Plugin				# optional