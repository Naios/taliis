package de.taliis.editor.plugin;

import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;

/**
 * A plugin is something that extends our main programms
 * Functionality. This plugin superclass provides the 
 * definition of all interface functions.
 * 
 * NOTE
 * A 'plugin' is a middle class.
 * Plugin's get instanced as soon they got loaded.
 * Therefore its NOT recomed to implement the functionality
 * of the Plugin outside of the Plugin itself. 
 * 
 * The plugin can get alot of references from the main application.
 * Such as menue, config and file loader references.
 * This references are OPTIONAL given by the main application.
 * 
 * In this version the following types of plugin's are allowed:
 * 
 * - view		:	Representation (and editor) for data classes.
 * - function	:	Functionality Plugin. Import, Export for exsample 
 * - wizard		:	Subtype of function that provides a set of graphical
 * 					wizards for whatever.
 * - storage	:	complete binary parsing classes for a given file type
 * 
 * All thoose types can be combined by logical AND
 * 
 * @author Tharo Herberg
 *
 */
public interface Plugin {
	// constances
	public final static int PLUGIN_TYPE_DEACTIVATED = 0;
	public final static int PLUGIN_TYPE_VIEW = 1;
    public final static int PLUGIN_TYPE_FUNCTION = 2;
	public final static int PLUGIN_TYPE_WIZARD = 4;
	public final static int PLUGIN_TYPE_STORAGE = 8;
	
	/**
	 * Let the Plugin itsef the order to check all
	 * needed dependencies of loaded classes to work
	 * property.
	 * 
	 * @return true for succes, false for failure
	 */
	public boolean checkDependencies();
	
	/**
	 * Gives a String array of needed classnames.
	 * It's okay to just return a list of missed classes. 
	 * @return List of needed Java Classes
	 */
	public String[] neededDependencies();
	
	/**
	 * Returns a list of supported data class to
	 * say the editor if this is a DBC or ADT functionality
	 * f.e.
	 * @return supported data types. f.e. {"dbc", "adt"} and so on.
	 */
	public String[] getSupportedDataTypes();

	/**
	 * @return plugin type of this programm
	 */
	public int getPluginType();

	/**
	 * Gives reference to main Applications main
	 * menue that allready go init. by the main app.
	 * @param ref
	 */
	public void setMenuRef(JMenuBar ref);
	
	/**
	 * Gives reference to main Applications file
	 * loader that should be used to access any 
	 * opened files.
	 * @param ref
	 */
	public void setFileManangerRef(fileMananger ref);
	
	/**
	 * Gives refence to the main Applications config
	 * loader that should be used to load and access
	 * config files.
	 * @param ref
	 */
	public void setConfigManangerRef(configMananger ref);
	
	/**
	 * Gives reference to the classloader that was
	 * loading this plugin. Could be used to load icons
	 * and Stuff.
	 * @param ref
	 */
	public void setClassLoaderRef(URLClassLoader ref);
	
	/**
	 * Gives reference to an event Server that allos the
	 * Pugin to tell important stuff to the main application.
	 * @param ref
	 */
	public void setEventServer(eventServer ref);
	
	/**
	 * Gives reference to a list of any loaded plugin without
	 * take care of its source.
	 * @param ref
	 */
	public void setPluginPool(Vector<Plugin> ref);
	
	/**
	 * Our Icon. Whatever it is
	 * @return icon or null
	 */
	public ImageIcon getIcon();
}