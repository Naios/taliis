package de.taliis.editor.plugin;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import de.taliis.editor.openedFile;
import starlight.taliis.core.files.wowfile;

/**
 * spez interface for the main data storage classes
 * of the starlight project.
 * Since starlight.taliis.core.memory is the superclass
 * of any storage class we return this.
 * 
 * @author Ganku
 *
 */

public interface PluginStorage {
	/**
	 * Parse the given bytebuffer into our 
	 * structure.
	 * 
	 * @param f file to load
	 * @return
	 */
	public wowfile load(File f);
	
	/**
	 * Renders and saves the data back to
	 * the given file
	 * 
	 * @param f openedFile to store into
	 * @return
	 */
	public int save(openedFile f);
	
	/**
	 * Create a new file
	 * 
	 * @param f
	 * @return
	 */
	public wowfile create();
	
	public FileFilter getFiter();
}
