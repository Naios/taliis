package de.taliis.editor;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import de.taliis.editor.plugin.Plugin;

class MenuRenderer extends DefaultTreeCellRenderer {
	public Component getTreeCellRendererComponent(
                        JTree tree,
                        Object value,
                        boolean sel,
                        boolean expanded,
                        boolean leaf,
                        int row,
                        boolean hasFocus) {

        super.getTreeCellRendererComponent(
                        tree, value, sel,
                        expanded, leaf, row,
                        hasFocus);

        if(value instanceof menuNode) {
		    menuNode tmp = (menuNode)value;
		    if(tmp.view instanceof Plugin) {
		        setIcon(tmp.view.getIcon());
		    }
        }
	    return this;
    }
}