package de.taliis.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileFilter;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import de.taliis.editor.plugin.*;

/**
 * New designed Taliis Editor Main class
 * @author Tharo Herberg
 */

public class main 
	implements eventServer, TreeSelectionListener, ActionListener
{
	// management classes
	fileMananger fm;		// our file mananger
	configMananger cm;		// our config mananger
	Vector<PluginPack> pp;	// all loaded plugin packs (jar files)
	Vector<Plugin> ppool;	// unsorted list of plugins
	
	// visual elements
	JFrame mainFrame;
	JMenuBar mainMenu;
	JTree tree;
	JSplitPane sp1;
	
	
	// menue elements
	JMenu menFile, menHelp, menEdit;
	JMenuItem meniNew, meniExit,
				meniHelp, meniAbout;
	
	
	
	public static void main(String[] args) {
		new main();
	}
	
	
	
	/**
	 * Initialization. Trys to:
	 * 1. Init Config Loader
	 * 		Needed to catch the absolute needed basic values
	 * 2. load Plugins
	 * 		Load all plugins from our plugin dictionary
	 * 3. Init File Loader
	 * 		be ready to load / hold files
	 * 4. Init Main Frame
	 * 		setup application
	 * 5. Init Plugins
	 * 		init our loaded plugins
	 */
	public main() {
		if(initConfigMananger() < 0
		   || loadPlugins() < 0
		   || initFileMananger() < 0
		   || initMainFrame() < 0
		   || initPlugins() < 0
		) {
			unload();
			return;
		}
	}
	/**
	 * Setup the configMananger that handles
	 * to read requested config files or global
	 * config settings.
	 * 
	 * @return <0 if something went wrong.
	 */
	private int initConfigMananger() {
		cm = new configMananger();
		return 1;
	}
	private int initFileMananger() {
		fm = new fileMananger(ppool);
		return 1;
	}
	private int initMainFrame() {
		// the frame itself
		mainFrame = new JFrame();
		mainFrame.setTitle("Taliis Editor");
		mainFrame.setSize(700, 400);
		mainFrame.setLocation(200, 200);
		mainFrame.addWindowListener(new WindowAdapter(){
		      public void windowClosing(WindowEvent we){
		          unload();
		        }
		      });
		mainFrame.setLayout(new BorderLayout(100, 0));

		// menu
		mainMenu = new JMenuBar();
		initMenu();		
		
		// tree
		tree = new JTree();
		fileTreeModel tm = new fileTreeModel(fm, ppool);
		tree.setRootVisible(false);
		tree.putClientProperty("JTree.lineStyle", "Horizontal");
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setShowsRootHandles(false);
		tree.setModel(tm);
		tree.setCellRenderer(new MenuRenderer());
		tree.addTreeSelectionListener(this);
		

		// main area
		JScrollPane treeView = new JScrollPane(tree);
		treeView.setMinimumSize(new Dimension(200, 200));
		sp1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				treeView, new JPanel() );
		sp1.setOneTouchExpandable(true);
		sp1.setDividerLocation(200);
		sp1.setAutoscrolls(false);
		
		mainFrame.add(sp1, BorderLayout.CENTER);	
		mainFrame.add(mainMenu, BorderLayout.NORTH);
	
		mainFrame.show();
		return 1;
	}
	private void initMenu() {
		menFile = new JMenu("File");
		  menFile.setName("menu_file");
		  menFile.setMnemonic(KeyEvent.VK_F);
		meniExit = new JMenuItem("Exit");
		  meniExit.setName("meni_exit");
		  meniExit.setMnemonic(KeyEvent.VK_X);
		  meniExit.setAccelerator(KeyStroke.getKeyStroke(
			        KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		  meniExit.addActionListener(this);
		  menFile.add(meniExit);
		  
		mainMenu.add(menFile);
		
	// changeable edit menues
		for(Plugin p : ppool) {
			if((p.getPluginType() & p.PLUGIN_TYPE_STORAGE)!=0) {
				menEdit = new JMenu("Edit");
				menEdit.setMnemonic(KeyEvent.VK_E);
				menEdit.setName("edit_" + p.getSupportedDataTypes()[0]);
				menEdit.setVisible(false);
				menEdit.setEnabled(false);
				mainMenu.add(menEdit);
			}
		}
		// grey std menu entry
		menEdit = new JMenu("Edit");
		menEdit.setMnemonic(KeyEvent.VK_E);
		menEdit.setName("edit_none");
		menEdit.setEnabled(false);
		mainMenu.add(menEdit);

	// help menues
		menHelp = new JMenu("Help");
		  menHelp.setName("menu_help");
		  menHelp.setMnemonic(KeyEvent.VK_H);
		meniAbout = new JMenuItem("About");
		  meniAbout.setName("meni_about");
		  meniAbout.setMnemonic(KeyEvent.VK_A);
		  menHelp.add(meniAbout);
		
		mainMenu.add(menHelp);
	}
	
	private int loadPlugins() {
		pp = new Vector<PluginPack>();
		ppool = new Vector<Plugin>();
		
		// parse plugins directory
		String src = cm.getGlobalConfig().getProperty("taliis_plugin_dir");
		if(src==null) src = "plugins/";
		File dir = new File(src);
		
		// filter all not jar files out
		File files[] = dir.listFiles(new FileFilter() {
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".jar");
            }
        });
		if(files.length==0) return 2;

		// load in plugins		
		for(File f : files) {
			if(f.isFile()) {
				System.out.println("Load Plugin File: " + f.getName());
				PluginPack tmp = new PluginPack(f);
				if(tmp.Plugins!=null && tmp.Plugins.size()>0) {
					pp.add( tmp );
					for(Plugin p : tmp.Plugins)
						ppool.add(p);
				}
			}
		}
		
		return 1;
	}
	private int initPlugins() {	
		// give plugins all references
		System.out.println("Init Plugin: ");
		for(PluginPack tmppp : pp) {
			for(Plugin tmpp : tmppp.Plugins ) {
				System.out.println(" " + tmpp.getClass().toString());
				if(tmpp.checkDependencies()==true) {
					tmpp.setPluginPool(ppool);
					tmpp.setClassLoaderRef(tmppp.getLoader());
					tmpp.setConfigManangerRef(cm);
					tmpp.setFileManangerRef(fm);
					tmpp.setMenuRef(mainMenu);
					tmpp.setEventServer(this);
				}
				else {
					System.out.println(" Error! Plugin need some other classes");
					if(tmpp.neededDependencies()!=null) {
						for(String s : tmpp.neededDependencies()) {
							System.out.println("\t" + s);
						}
					}
					// remove from plugin pool
					ppool.remove(tmpp);
				}
			}
		}
		
		return 1;
	}
	private void unload() {
		//TODO: save open files?
		//save config
		cm.unload();
		cm = null;
		
		fm = null;
		pp.clear();
		pp = null;
		System.exit(0);
	}



// Event Handlders	
/////////////////////////////////////////////////////////////////

	
	public void updateTable() {
		javax.swing.SwingUtilities.invokeLater( 
				new Runnable() {
					public void run() {
						tree.updateUI();
					
						// make it look nice
						tree.expandPath(new TreePath(tree.getModel().getRoot()));
					} 
				}
			);
	}


	
	public void valueChanged(TreeSelectionEvent arg0) {
		Object node = tree.getLastSelectedPathComponent();
		if (node == null) return;

		if(node instanceof menuNode){
			// 1. catch file and set active
			menuNode tmp = (menuNode) node;
			fm.setActiveFile(tmp.file);
			
			// 2a start plugin?
			if(tmp.view instanceof PluginView) {
				PluginView tmpp = (PluginView)tmp.view;
				sp1.setRightComponent(tmpp.createView());
			}
			// 2b change menus structures
			else if(tmp.view instanceof PluginStorage) {
				openedFile o = ((menuNode)node).file;
				fm.setActiveFile(o);
				
				// switch changeable edit menus
				for(Component c : mainMenu.getComponents() ) {
					String f = c.getName();
					if(f.contains("edit_")) {
						if(f.contains("_" + o.getExtension()) ) {
							c.setVisible(true);
						}
						else c.setVisible(false);
					}
				}
			}
		}
	}

	
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==meniExit) {
			unload();
		}
		else if(e.getSource()==meniAbout) {
			//TODO: About window
		}
	}
}