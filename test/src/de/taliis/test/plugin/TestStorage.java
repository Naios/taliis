package de.taliis.test.plugin;
//so, also ich bin da :P
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.fileLoader;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.eventServer;

public class TestStorage implements Plugin, PluginStorage {
	fileMananger fm;
	ImageIcon icon;
	
	
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		return true;
	}

	
	public int getPluginType() {
		return PLUGIN_TYPE_STORAGE;
	}
	
	
	public String[] getSupportedDataTypes() {
		return new String[]{"adt"};
	}

	
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/package.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}
	}

	
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}

	
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}

	
	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}

	
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		
	}

	
	public wowfile load(File f) {
		return new adt( fileLoader.openBuffer(f.getAbsolutePath()) );
	}

	
	public wowfile create() {
		// TODO Auto-generated method stub
		//fm.registerObject(o)
		return null;
	}

	
	public int save(openedFile f) {
		//TODO: catch failures and so on
		if(f.obj instanceof wowfile) {
			wowfile o = (wowfile)f.obj;
			
			o.render();
			fileLoader.saveBuffer(o.buff , f.f.getAbsolutePath());
			
			return 1;
		}
		return -1;
	}

	
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}

	
	public ImageIcon getIcon() {
		return icon;
	}

	
	public FileFilter getFiter() {
		return new FileFilter() {
			public boolean accept(File f) {
				if (f.isDirectory()) {
		            return true;
		        }
				
				String name = f.getName().toLowerCase();
				
				if(name.endsWith(".adt")) return true;
				else return false;
            }

			
			public String getDescription() {
				return "WoW ADT Map Files";
			}
        };
		
	
	}

}
