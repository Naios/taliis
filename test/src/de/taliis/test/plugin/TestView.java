package de.taliis.test.plugin;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.dbc;
import starlight.taliis.core.files.wowfile;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;

public class TestView implements Plugin, PluginView {
	fileMananger fm;
	ImageIcon viewIcon = null;

	public JPanel createView() {
		JPanel tmp = new JPanel();
		wowfile f = (wowfile)fm.getActiveFile().obj;
		
		String text = "";
		if(f instanceof adt) text = "ADT File"; 
		else if(f instanceof dbc) text = "DBC File";
		else text = f.getClass().toString(); 
			
		tmp.add(new JLabel(text), "Center");
		return tmp;
	}

	public String toString() {
		return "Test View";
	}
	
	
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		return true;
	}

	
	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}

	
	public String[] getSupportedDataTypes() {
		return new String[] {"adt"};
	}

	
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setClassLoaderRef(URLClassLoader ref) {
		URL u = ref.getResource("icons/heart.png");
		viewIcon = new ImageIcon( u );
	}

	
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}

	
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}

	
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;
	}

	
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
	}

	
	public ImageIcon getIcon() {
		return viewIcon;
	}

	
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}
	

}
