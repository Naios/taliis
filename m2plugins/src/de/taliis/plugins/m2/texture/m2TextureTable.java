package de.taliis.plugins.m2.texture;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import starlight.alien.*;
import starlight.taliis.apps.editors.AdvJTableCell;
import starlight.taliis.apps.editors.TableColorRenderer;
import starlight.taliis.core.files.m2;
import starlight.taliis.helpers.fileLoader;

public class m2TextureTable extends JPanel 
implements ActionListener {
	
	m2 obj;
	JTable table;
	JToolBar toolBar;
	JTextField newFile;
	
	final String ADD = "add";
	final String DEL = "del";

	public m2TextureTable(m2 reference) {
		obj = reference;
		this.setLayout(new BorderLayout());
	
		newFile = new JTextField();
		newFile.setText("Files are not checked for existance in mpq! (alpha)");
		
		toolBar = new JToolBar();
		JButton  button = makeNavigationButton("delete", DEL,
                "Delete given texture File",
                "Remove Texture");
		toolBar.add(button);
		button = makeNavigationButton("add", ADD,
                "Add a new texture File",
                "Add Texture");
		toolBar.addSeparator();
		toolBar.add(button);
		toolBar.add(newFile);
		add(toolBar, BorderLayout.PAGE_START);
		
        table = new JTable(new m2texTableModel(obj));
        //table.setPreferredScrollableViewportSize(new Dimension(70, 70));

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 

        table.getColumnModel().getColumn(0).setMaxWidth(20);
        table.getColumnModel().getColumn(1).setMaxWidth(30);
        table.getColumnModel().getColumn(2).setMaxWidth(60);
        table.getColumnModel().getColumn(3).setMaxWidth(60);
        //Add the scroll pane to this panel.
        add(scrollPane, "Center");
	}
	
	protected JButton makeNavigationButton(String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {
		//Look for the image.
		String imgLocation = "images/icons/"
		+ imageName
		+ ".png";

		//Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);
		button.addActionListener(this);
		
		if (imageName != "" && imageName != null) {    //image found
			button.setIcon(fileLoader.createImageIcon(imgLocation));
		}
		else {                                     //no image found
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (ADD.equals(cmd)) {
        	JOptionPane.showMessageDialog(this.getParent(),
        			"This function got deactivated.\nRename existing files instead.", 
        		    "He dude ..",
        		    JOptionPane.INFORMATION_MESSAGE);
        	
        }
        else if (DEL.equals(cmd)) {
        	JOptionPane.showMessageDialog(this.getParent(),
        			"This function got deactivated.\nRename existing files instead.", 
        		    "He dude ..",
        		    JOptionPane.INFORMATION_MESSAGE);
        }
	}
	
}

class m2texTableModel extends AbstractTableModel {
	static ImageIcon page = fileLoader.createImageIcon("images/icons/page.png");

	m2 obj;
	
	// Bezeichnungen
    Vector columnNames = new Vector();
    
    m2texTableModel(m2 aspect) {
    	obj	=	aspect;
    }
    
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 5;
	}
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return obj.header.getnTextures();
	}
	
    public String getColumnName(int col) {
        if(col==1) return "ID";
        else if(col==2) return "Offset";
        else if(col==3) return "Length";
        else if(col==4) return "Filename";
        else return "";
    }
	
	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
    	if(col==0) return page;
    	else if(col==1) return row;
    	else if(col==2) return obj.texdefs[row].getofsFilename();
    	else if(col==3) return obj.texdefs[row].getlenFilename();
    	else if(col==4)return obj.textures[row].toString();
    	else return null;
		
	}
	
    public Class getColumnClass(int c) {
        if(c==0) return Icon.class;
        else return String.class;
    }
    
    public boolean isCellEditable(int row, int col) {
    	return false;
    	/*if(col==2) return true;
    	else return false;*/
    }
	
}
	