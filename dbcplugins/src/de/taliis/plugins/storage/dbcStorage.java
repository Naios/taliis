package de.taliis.plugins.storage;

//so, also ich bin da :P
import java.io.File;
import java.io.InvalidClassException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.files.dbc;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.fileLoader;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.eventServer;

public class dbcStorage implements Plugin, PluginStorage {
	fileMananger fm;
	ImageIcon icon;
		
	public boolean checkDependencies() {
		return true;
	}

	public int getPluginType() {
		return PLUGIN_TYPE_STORAGE;
	}
	
	
	public String[] getSupportedDataTypes() {
		return new String[]{"dbc"};
	}

	
	public String[] neededDependencies() {
		return null;
	}

	
	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/table_multiple.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}
	}

	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}
	
	public wowfile load(File f) throws InvalidClassException {
		return new dbc( fileLoader.openBuffer(f.getAbsolutePath()) );
	}

	public wowfile create() {
		JOptionPane.showMessageDialog(null,
			    "There is no need in creating new dbc files.",
			    "Major Failure",
			    JOptionPane.ERROR_MESSAGE);
		return null;
	}

	public int save(openedFile f) {
		//TODO: catch failures and so on
		if(f.obj instanceof wowfile) {
			wowfile o = (wowfile)f.obj;
			
			o.render();
			fileLoader.saveBuffer(o.buff , f.f.getAbsolutePath());
			
			return 1;
		}
		return -1;
	}
	
	public ImageIcon getIcon() {
		return icon;
	}

	
	public FileFilter getFiter() {
		return new FileFilter() {
			public boolean accept(File f) {
				if (f.isDirectory()) {
		            return true;
		        }
				
				String name = f.getName().toLowerCase();
				
				if(name.endsWith(".dbc")) return true;
				else return false;
          }

			
			public String getDescription() {
				return "WoW DBC Data Files";
			}
      };
	}

	public void setConfigManangerRef(configMananger ref) { }
	public void setEventServer(eventServer ref) { }
	public void setMenuRef(JMenuBar ref) {	}
	public void setPluginPool(Vector<Plugin> ref) { }
	public void unload() { }
}
