package de.taliis.plugins.dbc.mixeditor;

import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;

import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.files.dbc;

/**
 * A smal container class that holds correspodending
 * datas that we need.
 * 
 * @author ganku
 *
 */
public class MixDataSet {
	/**
	 * The dbc file we are working with
	 */
	public dbc dbcFile;
	
	/**
	 * the active config for this editor window
	 */
	public Properties config;
	
	/**
	 * the correct config sub-version for our file
	 */
	public int configRevIndex = -1;
	public String configBaseString = "";

	/**
	 * whats the datatype of col x?
	 */
	public int types[];
	
	/**
	 * is column a crossed column?
	 */
	public boolean crossed[];
	
	/**
	 * Translates from table-col -> mix hash-array index
	 */
	public int mixArrIndexies[];
	
	public HashMap<Integer, String> CrossLists[];
	
	/**
	 * How many cross fields do we have?
	 */
	public int crossers = 0;
	
	/**
	 * New "column" types 
	 */
	
	/**
	 * A 'mask' means we have several indexies from other
	 * files | bunched. A mask is always used in cross fields
	 */
	public final static int COL_TYPE_MASK = 16;
	
	/**
	 * Same like a mask: A bitfield. But this one is not working
	 * as indexies. its more for options so NEVER used in crossfields
	 */
	public final static int COL_TYPE_FLAGS = 32;
	
	
	/**
	 * Initialization of all arrays we use to handle our
	 * column types
	 * @param lenght
	 */
	public void initDataTypeStuff() {
		int lenght = this.dbcFile.getNFields();
		
		this.types = new int[lenght];
		this.mixArrIndexies = new int[lenght];
		this.crossed = new boolean[lenght];
	}
	
	public void initHashArray() {
		CrossLists = new HashMap[this.crossers];
	}
}
