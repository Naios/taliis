package de.taliis.plugins.dbc.mixeditor;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import starlight.taliis.core.ZeroTerminatedString;

public class MixCellEditor  
	extends AbstractCellEditor 
	implements TableCellEditor 
{
    private JTextField component = new JTextField();
    
    public Component getTableCellEditorComponent(
            JTable table, Object value,
            boolean isSelected, int rowIndex, int colIndex )
    {
    	if(value instanceof MixEntry) {
    		MixEntry v = (MixEntry) value;
    		component.setText("" + v.getRealValue());
    	}
    	else component.setText(value.toString());
    	return component;
    }

    public Object getCellEditorValue()
    {
        return component.getText();
    }
}
