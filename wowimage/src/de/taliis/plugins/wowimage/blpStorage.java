package de.taliis.plugins.wowimage;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.files.wowfile;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.eventServer;

public class blpStorage implements Plugin, PluginStorage {
	ImageIcon icon=null;
	
	public boolean checkDependencies() { return true; }
	public ImageIcon getIcon() { return icon; }

	public int getPluginType() {
		return this.PLUGIN_TYPE_STORAGE;
	}

	public String[] getSupportedDataTypes() {
		return new String[]{ "blp" }; 
	}

	public String[] neededDependencies() {
		return null;
	}

	public void setClassLoaderRef(URLClassLoader ref) { 
		try {
			URL u = ref.getResource("icons/photo.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}		
	}

	public void setConfigManangerRef(configMananger arg0) {	}

	public void setEventServer(eventServer arg0) { }

	public void setFileManangerRef(fileMananger arg0) {	}

	public void setMenuRef(JMenuBar arg0) {	}

	public void setPluginPool(Vector<Plugin> arg0) { }
	
	
	
	public wowfile create() {
		System.err.println("Theres no need for a create function.");
		return null;
	}
	
	public FileFilter getFiter() {
		return new FileFilter() {

			public boolean accept(File arg0) {
				if(arg0.isDirectory())return true;
				if(arg0.toString().toLowerCase().endsWith(".blp")) return true;
				else return false;
			}

			public String getDescription() {
				return "WOW BLP Files";
			}
		};
	}
	
	public wowfile load(File arg0) {
		return new blp(arg0);
	}
	
	public int save(openedFile arg0) {
		// no savings
		return -1;
	}
	public void unload() {
		// TODO Auto-generated method stub
		
	}
}
