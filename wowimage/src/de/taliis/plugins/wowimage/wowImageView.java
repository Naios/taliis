package de.taliis.plugins.wowimage;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;

public class wowImageView implements Plugin, PluginView {
	fileMananger fm;
	ImageIcon icon = null;
	
	public boolean checkDependencies() {
		return true;
	}
	public ImageIcon getIcon() {
		return icon;
	}
	public int getPluginType() {
		// TODO Auto-generated method stub
		return this.PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		return new String[] { "blp" };
	}

	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/photo.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}	
	}

	public void setConfigManangerRef(configMananger ref) {
		// we dont have a std editor? Cool! Not its me ^^
		String key = "taliis_storage_defaultView_blp";
		if(!ref.getGlobalConfig().containsKey(key)) {
			ref.getGlobalConfig().setProperty(
					key, "de.taliis.plugins.wowimage.wowImageView"
				);
		}
	}

	public void setEventServer(eventServer arg0) {
		// TODO Auto-generated method stub
	}

	public void setFileManangerRef(fileMananger arg0) {
		// TODO Auto-generated method stub
		fm = arg0;
	}

	public void setMenuRef(JMenuBar arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setPluginPool(Vector<Plugin> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public JPanel createView() {
		openedFile of = fm.getActiveFile();
		if(of==null) return null;
		
		if(of.obj instanceof blp) {
			blp img = (blp)of.obj;
			Icon icon = new ImageIcon(img.getImg());
			JLabel label = new JLabel(icon);
			
			JPanel f = new JPanel();
			f.add(label);

			return f;
		}
		return null;
	}
	
	public String toString() {
		return "View Image";
	}
	public void unload() {
		// TODO Auto-generated method stub
		
	}
}
