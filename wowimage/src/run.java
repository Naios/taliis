import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintStream;
import java.util.StringTokenizer;
import javax.swing.*;
import wowimage.BLPDecoder;
import wowimage.BLPEncoder;
import wowimage.ConversionException;
import wowimage.DropButton;

public class run
{

    public run()
    {
    }

    public static void main(String args[])
        throws IOException
    {
        if(args.length == 1)
        {
            if(args[0].endsWith("blp"))
            {
                String s = args[0];
                BLPDecoder blpdecoder;
				try {
					blpdecoder = new BLPDecoder(s);
	                StringTokenizer stringtokenizer = new StringTokenizer(s, ".");
	                String s2 = stringtokenizer.nextToken() + ".png";
	                blpdecoder.writePNG(s2);
	                System.out.println("Converted " + s + " to " + s2);
	            } catch (Exception e) {
					e.printStackTrace();
				}
            } else
            if(args[0].endsWith("png"))
            {
                String s1 = args[0];
                try {
	                BLPEncoder blpencoder = new BLPEncoder("png", s1);
	                StringTokenizer stringtokenizer1 = new StringTokenizer(s1, ".");
	                String s3 = stringtokenizer1.nextToken() + ".blp";
	                blpencoder.writeBLP(s3);
	                System.out.println("Converted " + s1 + " to " + s3);
	            } catch (Exception e) {
					e.printStackTrace();
				}
            } else
            {
                System.err.println("Usage: util <filename>");
                System.err.println("filename must be either a .blp or a .png");
                System.err.println("");
                System.err.println("-or-");
                System.err.println("");
                System.err.println("Usage: util -encode/-decode <directory>");
                System.err.println("will convert all blp or png files in that directory (will overwrite target files without warning!)");
                return;
            }
            //break MISSING_BLOCK_LABEL_681;
        }
        try
        {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            JFrame jframe = new JFrame();
            jframe.setTitle("WoW Image Converter v0.991");
            Container container = jframe.getContentPane();
            container.setLayout(new BoxLayout(container, 1));
            JPanel jpanel = new JPanel();
            jpanel.setLayout(new GridLayout(3, 2));
            jpanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
            jpanel.add(new Label("Mipmaps"));
            JSlider jslider = new JSlider(0, 1, 8, 8);
            jslider.setPaintLabels(true);
            jslider.setPaintTicks(true);
            jslider.setSnapToTicks(true);
            jslider.setMinorTickSpacing(1);
            jpanel.add(jslider);
            jpanel.add(new Label("x"));
            jpanel.add(new Label("y"));
            jpanel.add(new Label("x"));
            jpanel.add(new Label("y"));
            Container container1 = new Container();
            container1.setLayout(new GridLayout(0, 2));
            DropButton dropbutton = new DropButton(DropButton.DECODER);
            DropButton dropbutton1 = new DropButton(DropButton.ENCODER);
            container1.add(dropbutton);
            container1.add(dropbutton1);
            container.add(container1);
            JTextArea jtextarea = new JTextArea(3, 3);
            JScrollPane jscrollpane = new JScrollPane(jtextarea);
            jtextarea.setEditable(false);
            jtextarea.append("Drop the file(s) you want to process on one of the buttons above\n");
            jtextarea.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
            container.add(jscrollpane);
            JButton jbutton = new JButton("Help");
            jbutton.setAlignmentX(0.5F);
            jbutton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent actionevent)
                {
                    JOptionPane.showMessageDialog((Component)actionevent.getSource(), "Written by Taktaal\n\nSend bug reports to taktaal@gmx.net\n\nIf you think you found a bug please\nremember to attach the error.txt\nthat the program creates on a crash\nas well as any image file that leads to an error");
                }

            });
            container.add(jbutton);
            dropbutton.setStatusControl(jtextarea);
            dropbutton1.setStatusControl(jtextarea);
            jframe.addWindowListener(new WindowAdapter() {

                public void windowClosing(WindowEvent windowevent)
                {
                    System.exit(0);
                }

            });
            jframe.setSize(400, 300);
            jframe.setResizable(false);
            jframe.setVisible(true);
        }
        catch(Exception exception)
        {
            System.out.println("Critical error, terrible bug");
            exception.printStackTrace();
        }
    }
}
