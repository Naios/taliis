package de.taliis.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.InvalidClassException;
import java.net.URL;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.JWindow;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import starlight.taliis.core.chunks.ChunkNotFoundException;

import de.taliis.editor.plugin.*;

/**
 * New designed Taliis Editor Main class
 * @author Tharo Herberg
 */

public class main 
	implements eventServer, TreeSelectionListener, ActionListener
{
	// management classes
	fileMananger fm;		// our file mananger
	configMananger cm;		// our config mananger
	PluginPack pp;			// all loaded plugin packs (jar files)
	Vector<Plugin> ppool;	// unsorted list of plugins
	
	// visual elements
	JFrame mainFrame;
	JMenuBar mainMenu;
	JTree tree;
	JSplitPane sp1;
	
	JFrame Aboutwindow;
	
	
	// menue elements
	JMenu menFile, menHelp, menSettings, menEdit;
	JMenuItem meniNew, meniExit,
				meniHelp, meniAbout;
	


	
	public static void main(String[] args) throws InvalidClassException, ChunkNotFoundException {
		main obj = new main();
		
		for(String f : args)
			obj.load(f);
	}
	
	
	
	/**
	 * Tells the file Mananger to load a file
	 * @param FileName
	 * @throws ChunkNotFoundException 
	 * @throws InvalidClassException 
	 */
	public void load(String FileName) throws InvalidClassException, ChunkNotFoundException {
		fm.openFileLocation(FileName);
	}
	
	
	/**
	 * Initialization. Trys to:
	 * 1. Init Config Loader
	 * 		Needed to catch the absolute needed basic values
	 * 2. load Plugins
	 * 		Load all plugins from our plugin dictionary
	 * 3. Init File Loader
	 * 		be ready to load / hold files
	 * 4. Init Main Frame
	 * 		setup application
	 * 5. Init Plugins
	 * 		init our loaded plugins
	 */
	public main() {
		if(initConfigMananger() < 0
		   || loadPlugins() < 0
		   || initFileMananger() < 0
		   || initMainFrame() < 0
		   || initPlugins() < 0
		) {
			unload();
			return;
		}
	}
	/**
	 * Setup the configMananger that handles
	 * to read requested config files or global
	 * config settings.
	 * 
	 * @return <0 if something went wrong.
	 */
	private int initConfigMananger() {
		cm = new configMananger();
		
		// default variablen
		String s = cm.getGlobalConfig().getProperty("taliis_temp_dir");
		if(s==null) {
			cm.getGlobalConfig().setProperty("taliis_temp_dir", "./temp");
		}
		
		return 1;
	}
	private int initFileMananger() {
		fm = new fileMananger(ppool);
		return 1;
	}
	private int initMainFrame() {
		// change the look and feel
		/*try { 
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); 
		} catch (Exception e) {}/**/
		
	// the frame itself
		mainFrame = new JFrame();
		mainFrame.setTitle("Taliis Editor");
		mainFrame.setSize(700, 400);
		mainFrame.setLocation(200, 200);
		mainFrame.addWindowListener(new WindowAdapter(){
		      public void windowClosing(WindowEvent we){
		          unload();
		        }
		      });
		mainFrame.setLayout(new BorderLayout(100, 0));
		
	// icon
		ImageIcon icon;
		try {
			URL u = this.getClass()
						.getClassLoader()
						.getResource("icons/taliis.png");
			icon = new ImageIcon( u );
			mainFrame.setIconImage(icon.getImage());
		} catch(NullPointerException e) {}
		
	// menu
		mainMenu = new JMenuBar();
		initMenu();		
		
		// tree
		tree = new JTree();
		fileTreeModel tm = new fileTreeModel(fm, cm, ppool);
		tree.setRootVisible(false);
		tree.putClientProperty("JTree.lineStyle", "Horizontal");
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setShowsRootHandles(false);
		tree.setModel(tm);
		tree.setCellRenderer(new MenuRenderer());
		tree.addTreeSelectionListener(this);
		

		// main area
		JScrollPane treeView = new JScrollPane(tree);
		treeView.setMinimumSize(new Dimension(200, 200));
		sp1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				treeView, new JPanel() );
		sp1.setOneTouchExpandable(true);
		sp1.setDividerLocation(200);
		sp1.setAutoscrolls(false);
		
		Aboutwindow = new JFrame();
		ImageIcon abouticon = null;
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		System.out.println("Adding Icon");
		try{
			URL u = this.getClass()
			.getClassLoader()
			.getResource("icons/about.png");
			abouticon = new ImageIcon( u );
			contentPane.add(new JLabel(abouticon, JLabel.CENTER));
			Aboutwindow.setIconImage(abouticon.getImage());
		}catch(NullPointerException e) {System.out.println("Failed loading image");}
		contentPane.add(new JLabel("Taliis", JLabel.CENTER), BorderLayout.NORTH);
		contentPane.add(new JLabel("(c) 2007-2009 Taliis-Team"), BorderLayout.SOUTH);
		Aboutwindow.setContentPane(contentPane);
		Aboutwindow.setLocation(200, 200);
		Aboutwindow.setSize(260, 500);
		Aboutwindow.setVisible(false);
		Aboutwindow.show(false);
		
		mainFrame.add(sp1, BorderLayout.CENTER);	
		mainFrame.add(mainMenu, BorderLayout.NORTH);
		mainFrame.show();
		return 1;
	}
	private void initMenu() {
		menFile = new JMenu("File");
		  menFile.setName("menu_file");
		  menFile.setMnemonic(KeyEvent.VK_F);
		meniExit = new JMenuItem("Exit");
		  meniExit.setName("meni_exit");
		  meniExit.setMnemonic(KeyEvent.VK_X);
		  meniExit.setAccelerator(KeyStroke.getKeyStroke(
			        KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		  meniExit.addActionListener(this);
		  menFile.add(meniExit);
		  
		mainMenu.add(menFile);
		
	// changeable edit menues
		for(Plugin p : ppool) {
			if(p instanceof PluginStorage) {
				menEdit = new JMenu("Edit");
				menEdit.setMnemonic(KeyEvent.VK_E);
				menEdit.setName("edit_" + p.getSupportedDataTypes()[0]);
				menEdit.setVisible(false);
				menEdit.setEnabled(false);
				mainMenu.add(menEdit);
			}
		}
		// grey std menu entry
		menEdit = new JMenu("Edit");
		 menEdit.setMnemonic(KeyEvent.VK_E);
		 menEdit.setName("edit_none");
		 menEdit.setEnabled(false);
		mainMenu.add(menEdit);

	// grey settings menue
		menSettings = new JMenu("Settings");
		 menSettings.setName("menu_settings");
		 menSettings.setEnabled(false);
		mainMenu.add(menSettings);
		
	// help menues
		menHelp = new JMenu("Help");
		  menHelp.setName("menu_help");
		  menHelp.setMnemonic(KeyEvent.VK_H);
		meniAbout = new JMenuItem("About");
		  meniAbout.setName("meni_about");
		  meniAbout.setMnemonic(KeyEvent.VK_A);
		  meniAbout.addActionListener(this);
		  menHelp.add(meniAbout);
		
		mainMenu.add(menHelp);
	}
	
	private int loadPlugins() {
		ppool = new Vector<Plugin>();
		
		// parse plugins directory
		String src = cm.getGlobalConfig().getProperty("taliis_plugin_dir");
		if(src==null) 
			src = "./plugins";
		File dir = new File(src);
		
		// filter all not jar files out
		File files[] = dir.listFiles(new FileFilter() {
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".jar");
            }
        });
		if(files==null) return 2;
		if(files.length==0) return 3;

		// load in plugins		
		pp = new PluginPack(files, cm);
		if(pp.Plugins!=null && pp.Plugins.size()>0) {
			for(Plugin p : pp.Plugins)
				ppool.add(p);
		}
		
		return 1;
	}
	private int initPlugins() {	
		// give plugins all references
		if(pp==null) return 0;
		System.out.println("Init Plugin: ");
		for(Plugin tmpp : pp.Plugins ) {
				System.out.println(" " + tmpp.getClass().toString());
				if(tmpp.checkDependencies()==true) {
					tmpp.setPluginPool(ppool);
					tmpp.setClassLoaderRef(pp.getLoader());
					tmpp.setConfigManangerRef(cm);
					tmpp.setFileManangerRef(fm);
					tmpp.setMenuRef(mainMenu);
					tmpp.setEventServer(this);
				}
				else {
					System.out.println(" Error! Plugin need some other classes");
					if(tmpp.neededDependencies()!=null) {
						for(String s : tmpp.neededDependencies()) {
							System.out.println("\t" + s);
						}
					}
					// remove from plugin pool
					ppool.remove(tmpp);
				}
		}
		
		return 1;
	}
	private void unload() {
		/**
		 * So. I realy tryed any clean way to wait until
		 * this list can iterated .. but i failed. Therefore
		 * i started this bussy waiting now -.-
		 */
		boolean aw = true;
		do {
			try {
				// all plugins have to unload
				for(Plugin p : ppool)
					p.unload();
				aw = false;
			} catch(java.util.ConcurrentModificationException e) {}
		}while(aw==true);

			
		//save config
		cm.unload();
		
		
		// unload manangers
		cm = null;	
		fm = null;
		pp = null;
		System.exit(0);
	}



// Event Handlders	
/////////////////////////////////////////////////////////////////

	
	public void updateTable() {
		javax.swing.SwingUtilities.invokeLater( 
				new Runnable() {
					public void run() {
						tree.updateUI();
					
						// make it look nice
						tree.expandPath(new TreePath(tree.getModel().getRoot()));
					} 
				}
			);
		
		// is there no active file?
		if(fm.getActiveFile()==null)
			sp1.setRightComponent(new JPanel());
	}


	
	public void valueChanged(TreeSelectionEvent arg0) {
		Object node = tree.getLastSelectedPathComponent();
		if (node == null) return;

		if(node instanceof menuNode){
			// 1. catch file and set active
			menuNode tmp = (menuNode) node;
			fm.setActiveFile(tmp.file);
			
			// 2a start plugin?
			if(tmp.view instanceof PluginView) {
				PluginView tmpp = (PluginView)tmp.view;
				try {
					sp1.setRightComponent(tmpp.createView());
				} catch (InvalidClassException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ChunkNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			// 2b change menus structures
			// look for default editor
			else if(tmp.view instanceof PluginStorage) {
				// switch changeable edit menus
				for(Component c : mainMenu.getComponents() ) {
					if(c instanceof JMenu) {
						JMenu m = (JMenu)c;
						String f = m.getName();
						if(f.contains("edit_")) {
							if(f.contains("_" + tmp.file.getExtension()) ) {
								m.setVisible(true);
							}
							else {
								m.setVisible(false);
							}
						}
					}
				}
				mainMenu.updateUI();
				
				
				// show default view?
				String entryName = "taliis_storage_defaultView_" + tmp.file.getExtension();
				String dv = cm.getGlobalConfig().getProperty(entryName);			
				
				// load and show default view
				PluginView tmpp = null;
				if(dv!=null) {
					for(Plugin p : ppool) {
						if(p instanceof PluginView) {
							if(p.getClass().toString().compareToIgnoreCase("class "+dv)==0) {
								tmpp = (PluginView)p;
								break;
							}
						}
					}
				}
				
				// no valid view found? delete entry.
				if(tmpp==null) {
					if(cm.getGlobalConfig().containsKey(entryName)) {
						System.out.println("Default View + '" + entryName + "' not found.\nEntry deleted.");
						cm.getGlobalConfig().remove(entryName);
					}
				} else
					try {
						sp1.setRightComponent(tmpp.createView());
					} catch (InvalidClassException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ChunkNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==meniAbout) {
			about();
		}
		
		else if(e.getSource()==meniExit) {
			unload();
		}
	}
	
	public void about(){
		
		Aboutwindow.setVisible(true);
		Aboutwindow.show(true);

	}
}
