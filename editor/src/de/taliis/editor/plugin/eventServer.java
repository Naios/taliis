package de.taliis.editor.plugin;
/**
 * The event Server Interface is implementated
 * by the main application itself. Its designed
 * to give plugins the ability to force some
 * updates.
 * 
 * 
 * @author ganku
 *
 */

/**
 * Forces rebuild of the main table. atm there is
 * only this plugin <.<
 */
public interface eventServer {
	public abstract void updateTable();
}
