package de.taliis.editor.plugin;

import java.io.InvalidClassException;

import javax.swing.JPanel;

import starlight.taliis.core.chunks.ChunkNotFoundException;

/**
 * Interface for all editors / views
 * 
 * @author ganku
 */


public interface PluginView {
	/**
	 * Creates the "editor/view" and gives the reference back
	 * @return JPanel reference to the view
	 * @throws ChunkNotFoundException 
	 * @throws InvalidClassException 
	 */
	public abstract JPanel createView() throws InvalidClassException, ChunkNotFoundException;
}
