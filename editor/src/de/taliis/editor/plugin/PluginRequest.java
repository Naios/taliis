package de.taliis.editor.plugin;

import java.io.File;
import java.io.InvalidClassException;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import de.taliis.editor.openedFile;

/**
 * Needed standart for plugins that offer to
 * 'get' data from somewhere such as the mpq
 * plugin.
 * 
 * @author ganku
 *
 */
public interface PluginRequest {
	/**
	 * Load the given ressource into taliis
	 * @return
	 * @throws ChunkNotFoundException 
	 * @throws InvalidClassException 
	 */
	public openedFile requestRessource(String URI) throws InvalidClassException, ChunkNotFoundException;
	
	/**
	 * Just returns a file handle of the file.
	 * dont load it into taliis
	 * @return
	 */
	public File requestFile(String URI);
}
