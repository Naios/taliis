package de.taliis.editor;

import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;

public class menuNode {
	public Plugin view = null;
	public openedFile file = null;
	
	public menuNode(Plugin v, openedFile f) {
		view = v;
		file = f;
	}
	
	public String toString() {
		if(view instanceof PluginView)
			return view.toString();
		else return file.toString();
	}
}
