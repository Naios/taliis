#	Taliis (c) 2007-2008 
#		by Melanie de Neygevisage	(Mae)
#			Tharo Herberg 		  	(ganku)
#		used libs are (c) by the contributors.

# Plugin Pack config File

pack_name = wdl storage and editor
pack_info = Browses and manipulate wdl files.
pack_autor = Tigurius
pack_version = 0.1

plugin_count = 3

# the full classpath of the plugin
plugin_1 = de.taliis.plugins.storage.wdlStorage
plugin_2 = de.taliis.plugins.wdl.heightmapView
plugin_3 = de.taliis.plugins.wdl.heightmapInjector