package de.taliis.plugins.wdl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.MenuElement;

import starlight.taliis.core.chunks.wdl.MARE;
import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wdl;


import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;



public class wdlScanner implements Plugin, ActionListener {
	fileMananger fm;
	JMenuItem mScanner;
	JMenu menu;
	@Override
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPluginType() {
		// TODO Auto-generated method stub
		return Plugin.PLUGIN_TYPE_FUNCTION;
	}

	@Override
	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "wdl" };
	}

	@Override
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;
	}

	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		// cast out edit menue
		for(MenuElement men : ref.getSubElements()) {
			if(men instanceof JMenu) {
				if(((JMenu)men).getName().contains("edit_wdl")) {
					menu = (JMenu)men;
					menu.setEnabled(true);
					break;
				}
			}
		}
		
		mScanner=new JMenuItem("Scan Folder");
		mScanner.addActionListener(this);
		menu.add(mScanner);
	}

	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		openedFile os = fm.getActiveFile();
		wdl obj = null;
		if(os.obj instanceof wdl){
			obj=(wdl) os.obj;
		}
		else return;
		for(int x=0;x<64;x++){
			for(int y=0;y<64;y++){
				String name=os.f.getAbsolutePath().substring(0, os.f.getAbsolutePath().length()-4)+"_"+x+"_"+y+".adt";
				openedFile of = null;
				try {
					of = fm.openFileLocation(name);
				}
				catch(Exception e){
					
				}
				if(of!=null){
					if(of.obj instanceof adt) {
						adt injObj = (adt) of.obj;
						short heightmap[]=new short[16*16];
						for(int c=0; c<256; c++) {
							float temp = 0;
							for(int i=0;i<145;i++){
								temp+=injObj.mcnk[c].mcvt.getVal(i)+injObj.mcnk[c].getPosZ();
							}
							temp=temp/145f;
							heightmap[c]=(short)(temp);
						}	
						int posx,posy;
						posy=y;
						posx=x;
						obj.mare[posy*64+posx]=new MARE();
						for(int i=0;i<16*16;i++){
							obj.mare[posy*64+posx].setInnerHeight(i,heightmap[i]);
						}
						
						//this sucks but I'm too lazy atm to find a better way
						for(int i=0;i<17*17;i++){
							if(i<17){
								if(i!=0)
								obj.mare[posy*64+posx].setOuterHeight(i, (short) ((heightmap[i]+heightmap[i])/2));
								else
									obj.mare[posy*64+posx].setOuterHeight(i, heightmap[i]);
	
							}
							else if(i<(17*17)-17){
								obj.mare[posy*64+posx].setOuterHeight(i, (short) ((heightmap[i-17]+heightmap[i-16])/2));
							}
							else
								obj.mare[posy*64+posx].setOuterHeight(i, (short) ((heightmap[(16*16)-1]+heightmap[(16*16)-2])/2));
						}
						
					}
					System.out.println("Loaded file: "+x+" "+y);
					fm.closeFile(of);
				}
			}
		}
		
	}
	
}