package de.taliis.plugins.storage;

import java.io.File;
import java.io.InvalidClassException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wdl;
import starlight.taliis.core.files.wdt;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.adtChecker;
import starlight.taliis.helpers.adtObjHelper;
import starlight.taliis.helpers.fileLoader;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.eventServer;
import de.taliis.plugins.dialogs.newWDLDialog;




public class wdlStorage implements Plugin, PluginStorage{

	fileMananger fm;
	ImageIcon icon;
	eventServer es;
	
	@Override
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}

	@Override
	public int getPluginType() {
		// TODO Auto-generated method stub
		return PLUGIN_TYPE_STORAGE;
	}

	@Override
	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[]{"wdl"};
	}

	@Override
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		try {
			URL u = ref.getResource("icons/world_go.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}		
	}

	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		es = ref;
	}

	@Override
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;
	}

	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public wowfile create() {
		// TODO Auto-generated method stub
		newWDLDialog n = new newWDLDialog( null );
		if(n.ok==true) {
			wdl tmp = null;
			try {
				tmp = new wdl();
			} catch (ChunkNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(n.maho==true)
				tmp.has_maho=true;
			// register by ourselfes
			fm.registerObject(
				tmp, 
				new File(n.name + ".wdl")
			);
			es.updateTable();
		}
		
		return null;
	}

	@Override
	public FileFilter getFiter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public wowfile load(File f) throws InvalidClassException,
			ChunkNotFoundException {
		// TODO Auto-generated method stub
		wdl tmp;
		tmp = new wdl( fileLoader.openBuffer(f.getAbsolutePath()));
		
		return tmp;
	}

	@Override
	public int save(openedFile f) {
		// TODO Auto-generated method stub
		if(f.obj instanceof wowfile) {
			wowfile o = (wowfile)f.obj;
			o.render();
				fileLoader.saveBuffer(o.buff , f.f.getAbsolutePath());
				return 1;
		}
		return -1;
	}
	
}
