package de.taliis.plugins.tigusplugins;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import starlight.taliis.core.chunks.adt.MCAL_Entry;
import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.core.files.adt;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;
import de.taliis.plugins.tigusplugins.mixeditor.dbcLoader;

/**
 * Displays adt's alphamap infomation as picture
 * 
 * @author tharo
 * 
 */
public class adtMCNKEditor implements Plugin, PluginView, ActionListener, MouseListener {
	fileMananger fm;
	configMananger cm;
	Vector<Plugin> pp;
	ImageIcon icon = null;
	JPopupMenu popup;
	JMenuItem mSetID,mCurrID,mHoleEditor,mFlagEditor;
	JMenuItem mAddTex,mTextures[];
	JMenu mArea,mTex,mHoles,mFlags;
	int xpos=0,ypos=0;
	dbcLoader dLoader;
	String dep[] = { 
			"starlight.taliis.core.files.wowfile", 
			"starlight.taliis.core.files.adt",
		};

	public boolean checkDependencies() {
		String now = "";
		try {
			for (String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch (Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}

	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}

	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "adt" };
	}

	public String[] neededDependencies() {
		return dep;
	}

	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/world_edit.png");
			icon = new ImageIcon(u);
		} catch (NullPointerException e) {
		}
	}

	public void setConfigManangerRef(configMananger ref) {
		cm = ref;
	}

	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub

	}

	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}

	public void setMenuRef(JMenuBar ref) {
		
	}

	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		pp = ref;
	}

	public void unload() {
		// TODO Auto-generated method stub
	}

	public JPanel createView() {
		// get file
		openedFile of = fm.getActiveFile();
		
		if (of == null || !(of.obj instanceof adt))
			return null;
		adt obj = (adt) of.obj;
		for(Plugin p : pp) {
			if(p.getClass().toString().endsWith("mpqFileRequest"))
				dLoader=new dbcLoader(fm, p);	
		}
		// create buffered image
		initPopup(0,0);

		BufferedImage img = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
		for(int x=0; x<16; x++) {
			for(int y=0; y<16; y++) {
				int layers = obj.mcnk[x*16+y].mcal.getLenght();
				//if(layers==0) continue;
					
				// preload data
				int data[] = new int[4096];
				
	
				for(int layer=0; layer<layers; layer++){
					MCAL_Entry mce = obj.mcnk[x*16+y].mcal.getLayerNo(layer);
					
					for(int c=0; c<4096; c++) {
						
						// color = old color | new color>>layer*16bit
						int newcolor = (mce.getValue(c)&0xff)<<(layer*8);
						
						// combined
						data[c] = data[c] | newcolor;
										
				}
					
					
					for(int c=0; c<64; c++)
						
						img.setRGB(y*64,				//int startX,
					                  x*64+c,		//int startY,
					                  64,			//int w,
					                  1,			//int h,
					                  data,			//int[] rgbArray,
					                  c*64,			//int offset,
					                  64);			//int scansize
					
					}
				}}
		// create panel, display
		Icon icon = new ImageIcon(img);
		JLabel label = new JLabel(icon);
		label.addMouseListener(this);
		label.setMinimumSize(new Dimension(1024, 1024));

		JPanel f = new JPanel();
		f.add(label);

		JScrollPane scr = new JScrollPane(f, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scr.setPreferredSize(new Dimension(1024, 1024));

		JPanel r = new JPanel();
		r.setLayout(new BorderLayout());
		r.add(scr, BorderLayout.CENTER);

		return r;

	}
	
	/**
	 * We use popup windows for basic operations like
	 * - copy
	 * - delete
	 */
	private void initPopup(int x,int y) {
		openedFile of = fm.getActiveFile();
		adt obj=(adt)of.obj;
		MCNK f=obj.mcnk[x*16+y];
		int ID=0;
		
		ID=f.getAreaID();
		int tex=f.getNLayers();
		popup = new JPopupMenu();
		
		mTex=new JMenu("Textures");
		mHoles=new JMenu("Holes");
		mArea = new JMenu("AreaID");
		mFlags=new JMenu("Flags");
		mTextures=new JMenuItem[tex];
		for(int i=0;i<tex;i++){
			String texname=obj.mtex.getValueNo(f.mcly.getLayer(i).getTextureID());
			mTextures[i]=new JMenuItem(i+" "+texname);
			mTextures[i].addActionListener(this);
			mTex.add(mTextures[i]);
		}
		if(tex<4){
			mAddTex=new JMenuItem("Add Texture");
			mAddTex.addActionListener(this);
			mTex.add(mAddTex);
		}
		
		mHoleEditor=new JMenuItem("Edit Holes");
		mHoleEditor.addActionListener(this);
		mHoles.add(mHoleEditor);
		
		mFlagEditor=new JMenuItem("Edit Flags");
		mFlagEditor.addActionListener(this);
		mFlags.add(mFlagEditor);

		
		mSetID = new JMenuItem("Set AreaID");
		
		mSetID.addActionListener(this);
		mCurrID = new JMenuItem(ID+" "+dLoader.getDBCValue_s("AreaTable.dbc", 0, 14,ID));
		
		mArea.add(mCurrID);
		mArea.add(mSetID);
		popup.add(mArea);
		popup.add(mTex);
		popup.add(mHoles);
		popup.add(mFlags);

	}


	public String toString() {
		return "MCNK-Editor";
	}

	public void actionPerformed(ActionEvent e) {
			if(e.getSource()==mSetID){
				// get file
				openedFile of = fm.getActiveFile();

				AreaIDPopUp a= new AreaIDPopUp(null);
				adt obj = (adt) of.obj;
				obj.mcnk[xpos*16+ypos].setAreaID(a.areaid);
				
			}
		else 
			if(e.getSource()==mHoleEditor){
				// get file
				openedFile of = fm.getActiveFile();
				adt obj=(adt)of.obj;
				HoleDialog a= new HoleDialog(null,obj.mcnk[xpos*16+ypos].getHoles());
				obj.mcnk[xpos*16+ypos].setHoles(a.holeval);
			}
		else 
			if(e.getSource()==mFlagEditor){
				// get file
				openedFile of = fm.getActiveFile();
				adt obj=(adt)of.obj;
				FlagsDialog a= new FlagsDialog(null,obj.mcnk[xpos*16+ypos].getFlags());
				obj.mcnk[xpos*16+ypos].setFlags(a.flagval);
		}
		else 
			if(e.getSource()==mAddTex){
				// get file
				openedFile of = fm.getActiveFile();
				adt obj=(adt)of.obj;
				String[] texs=new String[obj.mtex.entrys.length];
				for(int k=0;k<obj.mtex.entrys.length;k++){
					texs[k]=obj.mtex.entrys[k].toString();
				}
				TextureDialog a=new TextureDialog(null,texs,0,0);
				obj.mcnk[xpos*16+ypos].setNLayers(obj.mcnk[xpos*16+ypos].getNLayers()+1);
				obj.mcnk[xpos*16+ypos].mcly.add(a.textureid,a.groundid);
				obj.mcnk[xpos*16+ypos].mcal.createLayer();
			}
		else
				for(int i=0;i<mTextures.length;i++){
					if(e.getSource()==mTextures[i]){
						openedFile of = fm.getActiveFile();
						adt obj=(adt)of.obj;
						String[] texs=new String[obj.mtex.entrys.length];
						for(int k=0;k<obj.mtex.entrys.length;k++){
							texs[k]=obj.mtex.entrys[k].toString();
						}
						TextureDialog a=new TextureDialog(null,texs,obj.mcnk[xpos*16+ypos].mcly.GetTexID(i),obj.mcnk[xpos*16+ypos].mcly.GetGroundEffect(i));
						obj.mcnk[xpos*16+ypos].mcly.SetTexId(a.textureid, i);
						obj.mcnk[xpos*16+ypos].mcly.SetGroundEffect(a.groundid,i);
					}
				}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getButton()==MouseEvent.BUTTON3) {
			xpos=e.getX()/64;ypos=e.getY()/64;
			initPopup(xpos,ypos);
			popup.show(e.getComponent(), e.getX(), e.getY());
			
		}
	}
}
