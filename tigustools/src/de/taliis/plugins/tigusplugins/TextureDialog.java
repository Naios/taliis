package de.taliis.plugins.tigusplugins;


import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;






public class TextureDialog implements ActionListener{


	public boolean ok = false;
	public int textureid=0;
	public int groundid=0;
	JDialog frame;
	JComboBox TexBox;
	JTextField GroundID;


	JButton okButton = new JButton("OK");
	
	
	public void addComponentsToPane(Container pane){
    	pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		pane.add(TexBox);
		
		c.gridx=7;
		c.gridy=0;
		c.gridwidth = 5;
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.add(GroundID,c);
		

		c.gridx = 6;
		c.gridy = 2;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.NONE;
		okButton.addActionListener(this);
		pane.add(okButton, c);
	
	}
	
	public TextureDialog(Component rel,String[] texnames,int id,int geid){
		//Create and set up the window.
        frame = new JDialog();
        frame.setLocationRelativeTo(rel);
        frame.setTitle("Set Texture");
        frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        // using this line make the Java VM crash at java 5 03 MAC .. o.O
        // static or nonstatic icon doesnt madder
        //frame.setIconImage(IconNew.getImage());
        frame.setModal(true);
        
        textureid=id;
        TexBox=new JComboBox(texnames);
        TexBox.setSelectedIndex(textureid);
                
        groundid=geid;
        GroundID=new JTextField();
        GroundID.setText(String.valueOf(geid));

        JPanel bla = new JPanel();
        
        //Set up the content pane.
        addComponentsToPane(bla/*frame.getContentPane()/**/);
        frame.setContentPane(bla);
        //Display the window.
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource()==okButton) {
			textureid=TexBox.getSelectedIndex();
			groundid=Integer.parseInt(GroundID.getText());
			frame.dispose();
			ok = true;
		}
	}
	
}