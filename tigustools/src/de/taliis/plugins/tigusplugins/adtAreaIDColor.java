package de.taliis.plugins.tigusplugins;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import starlight.taliis.core.files.adt;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;

/**
 * Editor for editing AreaIDs graphical
 * 
 * @author tigurius
 * 
 */

public class adtAreaIDColor implements Plugin, PluginView, MouseListener, ActionListener {
	fileMananger fm;
	eventServer serv;
	ImageIcon icon = null;
	JPopupMenu popup;
	JMenuItem mSetID;
	int xpos=0,ypos=0;
	JPanel panel;
	JLabel label;
	String dep[] = { "starlight.taliis.core.files.wowfile",
			"starlight.taliis.core.files.adt", };

	public boolean checkDependencies() {
		String now = "";
		try {
			for (String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch (Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}

	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}

	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "adt" };
	}

	public String[] neededDependencies() {
		return dep;
	}

	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/contrast.png");
			icon = new ImageIcon(u);
		} catch (NullPointerException e) {
		}
	}

	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub

	}

	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		serv=ref;
	}

	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}

	public void setMenuRef(JMenuBar ref) {
	}

	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub

	}

	public void unload() {
		// TODO Auto-generated method stub
	}
	
	/**
	 * We use popup windows for basic operations like
	 * - copy
	 * - delete
	 */
	private void initPopup() {
		popup = new JPopupMenu();
		mSetID = new JMenuItem("Set AreaID");
		mSetID.addActionListener(this);
		popup.add(mSetID);

	}

	public JPanel createView() {
		openedFile of = fm.getActiveFile();
		if (of == null || !(of.obj instanceof adt))
			return null;
		paintPicture();
		initPopup();
		JPanel f = new JPanel();
		f.add(label);


		JScrollPane scr = new JScrollPane(f, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scr.setPreferredSize(new Dimension(16 * 16, 16 * 16));

		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(scr, BorderLayout.CENTER);

		return panel;
	}

	@Override
	public String toString() {
		return "AreaIDs";
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		// right click
		if(e.getButton()==MouseEvent.BUTTON3) {
			popup.show(e.getComponent(), e.getX(), e.getY());
			xpos=e.getX()/16;ypos=e.getY()/16;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==mSetID){
			// get file
			openedFile of = fm.getActiveFile();

			AreaIDPopUp a= new AreaIDPopUp(null);
			adt obj = (adt) of.obj;
			obj.mcnk[xpos*16+ypos].setAreaID(a.areaid);
			paintPicture();
		}
	}
	
	private void paintPicture(){
		// get file
		openedFile of = fm.getActiveFile();



		// get data
		adt obj = (adt) of.obj;
		
		//create colorpalette
		int tempid[]={0};
		for (int x = 0; x < 16; x++)
			for (int y = 0; y < 16; y++) {
				boolean hasid=false;
				for(int i=0;i<tempid.length;i++){
					if(obj.mcnk[x*16+y].getAreaID()==tempid[i]){
						hasid=true;
						break;
					}					
				}
				if(!hasid){
					int t[]=new int[tempid.length+1];
					for(int k=0;k<tempid.length;k++){
						t[k]=tempid[k];
					}
					t[tempid.length]=obj.mcnk[x*16+y].getAreaID();
					tempid=t;
				}
			}
		int idcolor[]=new int[tempid.length];
		int color=0;
		int t=0;
		for(int i=0;i<idcolor.length;i++){
			color+=(64<<(t*8));
			idcolor[i]=color;
			if(t<3)
				t++;
			else t=0;
			
		}
		// create buffered image
		BufferedImage img = new BufferedImage(16 * 16, 16 * 16, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < 16; x++)
			for (int y = 0; y < 16; y++) {
				// preload data
				int data[] = new int[16*16];
				
				for(int i=0;i<tempid.length;i++){
					if(obj.mcnk[x*16+y].getAreaID()==tempid[i]){
						for(int k=0;k<16*16;k++){
							data[k]=idcolor[i];
						}
					}
					
				}
				


				// set pixels
				for (int c = 0; c < 16; c++)
					img.setRGB(x * 16, // int startX,
					y * 16 + c,// int startY,
					16,// int w,
					1,// int h,
					data,// int[] rgbArray,
					c * 16,// int offset,
					16);// int scansize)

			}

		// create panel, display
		Icon icon = new ImageIcon(img);
		label= new JLabel(icon);
		label.addMouseListener(this);
		label.setMinimumSize(new Dimension(16 * 16, 16 * 16));

	}
}
