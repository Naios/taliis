#	Taliis (c) 2007-2008 
#		by Melanie de Neygevisage	(Mae)
#			Tharo Herberg 		  	(ganku)
#		used libs are (c) by the contributors.

# Plugin Pack config File

pack_name = Tigus Plugins
pack_info = Some Plugins I created/will create^^
pack_autor = Tigurius Scriptor
pack_version = 1.0

plugin_count = 2

# the full classpath of the plugin
plugin_1 = de.taliis.plugins.tigusplugins.adtAreaIDColor
plugin_2 = de.taliis.plugins.tigusplugins.adtMCNKEditor


