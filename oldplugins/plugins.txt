#	Taliis (c) 2007-2008 
#		by Melanie de Neygevisage	(Mae)
#			Tharo Herberg 		  	(ganku)
#		used libs are (c) by the contributors.

# Plugin Pack config File

pack_name = Old features Plugin
pack_info = All old Taliis Features repacked as plugin.
pack_autor = Tharo Herberg
pack_version = 1.0

plugin_count = 8

# the full classpath of the plugin
plugin_1 = de.taliis.plugins.oldPlugins.adtTextureFiles
plugin_2 = de.taliis.plugins.oldPlugins.adtDDFiles
plugin_3 = de.taliis.plugins.oldPlugins.adtWMOFiles
plugin_4 = de.taliis.plugins.oldPlugins.adtDDAppears
plugin_5 = de.taliis.plugins.oldPlugins.adtWMOAppears
plugin_6 = de.taliis.plugins.oldPlugins.dbcRichTablePlugin
plugin_7 = de.taliis.plugins.oldPlugins.dbcRawTablePlugin
plugin_8 = de.taliis.plugins.oldPlugins.dbcStringTablePlugin
