package starlight.taliis.helpers;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * Hacky overwrote of the original fileLoader in order
 * to make seperate classloaders possible without changing
 * the using classes - code
 * 
 * A simple collection of methods that allows to load
 * files. Most of them are for Images but there also some
 * for data inside of MPQ archives using the wowmpq lib
 * 
 * @author tharo
 *
 */
public class fileLoader {
	// tweak 
	public static URLClassLoader cl = null;
	
	/**	
	 * Loads the given filename as BufferedImage
	 * @param FileName
	 */
	public static BufferedImage loadImage(String FileName) {  
		BufferedImage img = null;  
		try {
			img = ImageIO.read(new File(FileName));  
		} catch (Exception e) {  
			e.printStackTrace();
			System.err.println("Cannot open File " + FileName + ".");
			System.exit(-1);
		}
		
		return img;
	}
	
	/**
	 * Opens the given file as ByteBuffer. The whole
	 * file get copyed into RAM.
	 * 
	 * @param FileName
	 * @return the ByteBuffer with our data
	 */
	public static ByteBuffer openBuffer(String FileName) {
		try {
			// open the file an isolate the file channel
				File f = new File(FileName);
				if(f.exists()==false) {
					System.err.println("File not Found: " + FileName);
					return null;
				}
				//FileInputStream fis = new FileInputStream(f);
				RandomAccessFile fis = new RandomAccessFile(f, "rw");
				FileChannel fc = fis.getChannel();
				
			// create buffer (lill endian) with our files data
				int sz = (int)fc.size();
				ByteBuffer bb = fc.map(FileChannel.MapMode.READ_WRITE, 0, sz);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				
			// re alocate data to RAM
				ByteBuffer tmp = ByteBuffer.allocate(bb.capacity());
				tmp.order(ByteOrder.LITTLE_ENDIAN);
				bb.position(0);
				tmp.put(bb);
				tmp.position(0);

			// clean up
				fc.close();
				fis.close();			
			
				return tmp;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return null;
	}
	
	/**
	 * Saves a given byteBuffer to a given file.
	 * Same get overwritten.
	 * 
	 * @param buff
	 * @param fileName
	 */
	public static void saveBuffer(ByteBuffer buff, String fileName) {
		// write buffer
		File file = new File(fileName);
		
		boolean append = false;
		try {
			// Create a writable file channel
			FileChannel wChannel = new FileOutputStream(file, append).getChannel();

			// write out
			wChannel.write(buff);
		    
			// Close the file
			wChannel.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads given image as image icon.
	 * Uses getRessource()
	 * @param path
	 * @return
	 */
    public static ImageIcon createImageIcon(String path) {
    	return new ImageIcon( getRessource(path) );
    }
    
    /**
     * Returns the URL of the project ressource and auto
     * detects its in a absolute path or inside of a jar
     * archive. JAR is tryed first.
     * @param path
     * @return
     */
    public static URL getRessource(String path) {
    	URL url = null;
    	if(cl==null) url = ClassLoader.getSystemResource(path);
    	else url = cl.getResource(path);
    	
    	// not in the ressources? try load from outside
    	if (url == null) {
    		File f = new File(path);
    		try {
				url = new URL("file://" + f.getAbsolutePath());
    		} catch (MalformedURLException e) {
    			//e.printStackTrace();
    			return null;
			}
    	}
        
    	return url;
    }
    
    /**
     * Returns InputStream from the requested file.
     * First its looking in the file System. Then its looking
     * for a config file inside of the jar file.
     * 
     * @param path
     * @return
     */
    public static InputStream getRessourceAsStream(String path) {
    	InputStream s;
    	
    	try {
    		File f = new File(path);
			s = new FileInputStream(f);
			return s;
    	} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	
    	s = ClassLoader.getSystemResourceAsStream(path);
    	return s;
    }
}
