package de.taliis.plugins.oldPlugins;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import starlight.taliis.apps.editors.adtTextureFileTable;
import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.fileLoader;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;

public class adtTextureFiles implements Plugin, PluginView {
	ImageIcon viewIcon = null;
	fileMananger fm;
	
	String dep[] = {
			"starlight.taliis.core.files.wowfile",
			"starlight.taliis.core.files.adt",
			"starlight.alien.CorrectStrangeBehaviourListener"
		};
	
	public boolean checkDependencies() {
		String now = "";
		try {
			for(String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch(Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}
	
	public ImageIcon getIcon() {
		return viewIcon;
	}
	
	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}
	
	public String[] getSupportedDataTypes() {
		return new String[] {"adt"};
	}
	
	public String[] neededDependencies() {
		return dep;
	}
	
	public void setClassLoaderRef(URLClassLoader ref) {
		fileLoader.cl = ref;
		try {
			URL u = ref.getResource("images/icons/page_copy.png");
			viewIcon = new ImageIcon( u );
		} catch (Exception e) {};/**/
	}
	
	public void setConfigManangerRef(configMananger ref) {
		
	}
	
	public void setEventServer(eventServer ref) {
		
	}

	
	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}

	
	public void setMenuRef(JMenuBar ref) {
		
	}

	
	public void setPluginPool(Vector<Plugin> ref) {
		
	}

	public JPanel createView() {
		wowfile f = (wowfile)fm.getActiveFile().obj;
		if(f instanceof adt) {
			return new adtTextureFileTable( (adt)f );
		}
		return null;
	}

	public String toString() {
		return "Texture Files";
	}
	
	public void unload() {};
}
