package de.taliis.plugins.dialogs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import starlight.alien.*;
import starlight.taliis.apps.editors.AdvJTableCell;
import starlight.taliis.apps.editors.TableColorRenderer;
import starlight.taliis.apps.editors.tallisStdEditor;
import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.MDDF;
import starlight.taliis.core.chunks.adt.MDDF_Entry;
import starlight.taliis.core.chunks.adt.MMDX;
import starlight.taliis.helpers.adtObjHelper;
import starlight.taliis.helpers.fileLoader;

/**
 * JTable interface for edit MTEX chunks in ADT files
 * @author tharo
 *
 */

public class adtDoodadAppTable extends JPanel 
implements ActionListener, MouseListener, KeyListener {
	adt obj;
	JTextField newFile;

	final String ADD = "add";
	final String DEL = "del";
	JTable table;
	JToolBar toolBar;
	JPopupMenu popup;
	JMenuItem miCopy, miDelete;
	
	
	
	
	public adtDoodadAppTable(adt reference) {
		obj = reference;
		this.setLayout(new BorderLayout());
			
		toolBar = new JToolBar();
		JButton  button = makeNavigationButton("delete", DEL,
                "Delete given texture File",
                "Remove Texture");
		toolBar.add(button);
		button = makeNavigationButton("add", ADD,
                "Add a new texture File",
                "Add Texture");
		toolBar.add(button);
		toolBar.addSeparator();
		add(toolBar, BorderLayout.PAGE_START);
		
			
        table = new JTable(new mddfTableModel(obj.mddf, obj.mmdx));
        //table.setPreferredScrollableViewportSize(new Dimension(70, 70));
        table.addMouseListener(this);
		table.addKeyListener(this);
        initPopup();
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 
        
        // set some table look and feel
        initTableFace();
        
        //Add the scroll pane to this panel.
        add(scrollPane);
	}
	
	
	
	protected JButton makeNavigationButton(String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {
		//Look for the image.
		String imgLocation = "images/icons/"
		+ imageName
		+ ".png";

		//Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);
		button.addActionListener(this);
		
		if (imageName != "" && imageName != null) {    //image found
			button.setIcon(fileLoader.createImageIcon(imgLocation));
		}
		else {                                     //no image found
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}
	
	void initTableFace() {
		TableColumn column;
		
		// file id
		column = table.getColumnModel().getColumn(0);
        column.setPreferredWidth(50);
        column.setCellRenderer(new TableColorRenderer(Color.WHITE));
        
        table.getColumnModel().getColumn(1).setMaxWidth(90);

		// first floats
		for(int c=2; c<5; c++) {
			column = table.getColumnModel().getColumn(c);
			column.setMaxWidth(150);
			column.setCellRenderer(new TableColorRenderer(new Color(224,255,224)));
		}
		
		// orientation floats
		for(int c=7; c<10; c++) {
			column = table.getColumnModel().getColumn(c);
			column.setMaxWidth(150);
			column.setCellRenderer(new TableColorRenderer(new Color(224,255,224)));
		}
		
		

	}
	
	/**
	 * We use popup windows for basic operations like
	 * - copy
	 * - delete
	 */
	private void initPopup() {
		popup = new JPopupMenu();
		miCopy = new JMenuItem("Clone selected Row");
		miCopy.addActionListener(this);
		miDelete = new JMenuItem("Delete selected Row(s)");
		miDelete.addActionListener(this);
		popup.add(miCopy);
		popup.add(miDelete);
	}
	
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (ADD.equals(cmd)) {
        	if(obj.mmdx.getLenght()==0) {
        		JOptionPane.showMessageDialog(this.getParent(),
        				"No Doodads registered!",
        				"Error",
        				JOptionPane.ERROR_MESSAGE);
        		return;
        	}
        	// add dirty empty new doodad
        	adtObjHelper objh = new adtObjHelper(obj);
        	Random r = new Random();
        	objh.addDoodad(0, r.nextInt(100000)+1);
        	
        	table.updateUI();
        }
        else if (ADD.equals(cmd)) {
        	if(table.getSelectedRow()==-1) return;
        	obj.mddf.remove( table.getSelectedRow()  );
        	table.updateUI();
        }
     // delete rows
        else if(e.getSource()==miDelete) {
        	if(table.getSelectedRow()==-1) return;
        	obj.mddf.remove( table.getSelectedRow()  );
        	table.updateUI();
		}
        
        else if(e.getSource()==miCopy) {
			int row = table.getSelectedRow(); 
			if(row==-1) return;
			
			if(obj.mmdx.getLenght()==0) {
        		JOptionPane.showMessageDialog(this.getParent(),
        				"No Doodads registered!",
        				"Error",
        				JOptionPane.ERROR_MESSAGE);
        		return;
        	}
        	// add dirty empty new doodad
        	adtObjHelper objh = new adtObjHelper(obj);
        	Random r = new Random();
        	objh.addDoodad(obj.mddf.entrys[row].getNameID(), r.nextInt(100000)+1,MDDF_Entry.translate(obj.mddf.entrys[row].getX()),MDDF_Entry.translate(obj.mddf.entrys[row].getY()), obj.mddf.entrys[row].getZ());
        	table.updateUI();

		}
    }
    
    /**
	 * Event stuff
	 */
	public void mouseClicked(MouseEvent e) { }
	public void mouseEntered(MouseEvent e) { }
	public void mouseExited(MouseEvent e) {	}
	public void mousePressed(MouseEvent e) { }
    
    public void mouseReleased(MouseEvent e) {
		// right click
		if(e.getButton()==MouseEvent.BUTTON3) {
			// menues need selected rows in order to perform
			
			if(table.getSelectedRowCount()>0)
				popup.show(e.getComponent(), e.getX(), e.getY());
		}
	}
    
    public void keyPressed(KeyEvent e) {
    }
	public void keyTyped(KeyEvent e) { }
	public void keyReleased(KeyEvent e) {
	}

}

class mddfTableModel extends AbstractTableModel {
	MDDF archive;
	MMDX files;
	
	// Bezeichnungen
    Vector columnNames = new Vector();
    
    mddfTableModel(MDDF reference, MMDX files_ref) {
    	archive = reference;
    	files = files_ref;
    }
    
    public int getColumnCount() {
        return 10;
    }

    public int getRowCount() {
    	return (int)archive.getLenght();
    }

    public String getColumnName(int col) {
        switch(col) {
        	case 0: return "File ID";
        	case 1: return "Unique ID";
        	case 2: return "x";
        	case 3: return "y";
        	case 4: return "z";
        	case 5: return "Scale";
        	case 6: return "Flags";
        	case 7: return "rot a";
        	case 8: return "rot b";
        	case 9: return "rot c";
        }
        return "";
    }

    public Object getValueAt(int row, int col) {
    	switch(col) {
	    	case 0: {
	    		int id = archive.entrys[row].getNameID();
	    		String filename = files.getValueNo(id);
	    		return new AdvJTableCell(id, filename);
	    	}
	    	case 1: return archive.entrys[row].getUniqID();
	    	case 2: return MDDF_Entry.translate(archive.entrys[row].getX());
	    	case 3: return MDDF_Entry.translate(archive.entrys[row].getY());
	    	case 4: return archive.entrys[row].getZ();
	    	case 5: return archive.entrys[row].getScale();
	    	case 6: return archive.entrys[row].getFlags();
	    	case 7: return archive.entrys[row].getA();
	    	case 8: return archive.entrys[row].getB();
	    	case 9: return archive.entrys[row].getC();
    	}
	    return null;
    }

/*    public Class getColumnClass(int c) {
        if(c==0) return Icon.class;
        else return String.class;
    }/**/

    public boolean isCellEditable(int row, int col) {
    	//if(col==1) return true;
    	return true;
    }
    
    

    public void setValueAt(Object value, int row, int col) {
    	// translate value
    	int vInt = 0;
    	float vFloat = 0;
    	
	    try {
	    	switch(col) {
	    		case 0: case 1: case 5: case 6:
	    			// integer value
	    			vInt = Integer.valueOf(value.toString());
	    			break;
	    		case 2: case 3: case 4: case 7: case 8: case 9:
	    			// float value
	    			vFloat = Float.valueOf(value.toString());
	    			break;
	    		default:
	    			return;
	    	}
	    } catch(Exception exp) {
	    	return;
	    }
	// unique id	TODO: check for aviability
	    switch(col) {
	    	case 0:
	    		if(vInt>=0 || vInt<archive.getLenght())
	    	    		archive.entrys[row].setNameID(vInt);
	    		break;
	    	case 1: archive.entrys[row].setUniqID(vInt); break;
	    	case 2: archive.entrys[row].setX( MDDF_Entry.translate( vFloat ) ); break;
	    	case 3: archive.entrys[row].setY( MDDF_Entry.translate( vFloat ) ); break;
	    	case 4: archive.entrys[row].setZ( vFloat ); break;
	    	case 5: archive.entrys[row].setScale((short)vInt); break;
		    //case 6: archive.entrys[row]
	    	case 7: archive.entrys[row].setA(vFloat); break;
	    	case 8: archive.entrys[row].setB(vFloat); break;
	    	case 9: archive.entrys[row].setC(vFloat); break;
	    }
    }
    

    public void clear() {
    }
}